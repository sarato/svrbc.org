const port = 8889
module.exports = process.env.SVRBC_TEST === 'true'
  ? {
      server: {
        command: 'npm run start:test',
        host: 'localhost',
        launchTimeout: 45000,
        port,
        // We don't just use the default jest-puppeteer options because it
        // would do a quick tcp request, and the netlify server would respond
        // immediately.
        waitOnScheme: {
          resources: [`http://localhost:${port}/`]
        }
      }
    }
  : {}
