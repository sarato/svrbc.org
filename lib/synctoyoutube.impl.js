import chalk from 'chalk'

import cli from './cli'
import sermons from './sermons'
import youtube from './youtube-server'

async function main () {
  console.log(chalk.cyan('Fetching all sermons from Gridsome…'))
  const gSermons = await sermons.getSermons()
  const youtubeSermonMap =
    Object.fromEntries(gSermons.map(s => [s.youtube, s]))
  console.log(chalk.cyan('Fetching all videos from YouTube…'))
  const ySermons = await youtube.getAllVideos()

  for (const ys of ySermons) {
    const gs = youtubeSermonMap[ys.id]
    const title = sermons.jazzTitle(gs)
    const description = sermons.metaDesc(gs)
    if (title === ys.snippet.title &&
      description === ys.snippet.description) {
      continue
    }

    console.log('updating…', ys.url)
    await youtube.updateVideo(ys.id, {
      title,
      description
    })
  }
}

cli.run('synctoyoutube', main)
