import path from 'path'

import _createApp from 'gridsome/lib/app'
import { BOOTSTRAP_PAGES } from 'gridsome/lib/utils/constants'

const APP_CONTEXT = path.resolve(__dirname, '..')

async function createApp () {
  return _createApp(APP_CONTEXT, BOOTSTRAP_PAGES)
}

export default {
  createApp
}
