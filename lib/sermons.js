import crypto from 'crypto'
import { promises as fs } from 'fs'
import glob from 'glob-promise'
import matter from 'gray-matter'
import stringify from 'json-stable-stringify'
import fetch from 'node-fetch'
import path from 'path'
import smartquotes from 'smartquotes'

import dataformat from './dataformat'
import { normalizeKey, slugify } from '../src/lib/datautils'

import {
  dateFullFmt,
  dateIdFmt,
  formatLADate,
  readLADate
} from '../src/lib/datetime'
import bible from '../src/lib/bible'

const srcDir = path.join(__dirname, '..', 'src')
const sermonsDir = path.join(srcDir, 'sermons')

async function populateAudioBytes (data) {
  if (!data.audio) return data
  const resp = await fetch(data.audio, { method: 'HEAD' })
  if (resp.status !== 200) {
    data.audioBytes = 0
    return data
  }
  data.audioBytes = parseInt(resp.headers.get('content-length'))
  return data
}

function jazzTitle (sermon) {
  const tagline = sermon.tagline ? `${sermon.tagline} | ` : ''
  return `${tagline}${sermon.title}`
}

function idFromDate (date, meridiem) {
  const dateId = formatLADate(date, dateIdFmt)
  return `${dateId}-${meridiem}`
}

function normalizeTitle (t) {
  return smartquotes(t)
}

function normalizePreacher (p) {
  return p.replace(/^(Pastor|Preacher|Deacon|Brother) */, '')
}

function normalizeText (text) {
  return bible.reformat(text, bible.LONG)
}

function formatMd (data, fdata, content) {
  // Make a date if we don't have it.
  const date = data.date.toISOString ? data.date : readLADate(data.date)
  const fm = Object.assign({}, data, {
    date: formatLADate(date, dateIdFmt),
    text: normalizeText(data.text),
    preacher: data.preacher ? data.preacher.id : undefined,
    series: data.series ? data.series.id : undefined
  })
  return dataformat.formatMd(Object.assign({ mdContent: content }, fm), [
    'id',
    'title',
    'date',
    'text',
    'preacher',
    'unregisteredPreacher',
    'series',
    'unregisteredSeries',
    'audio',
    'audioBytes',
    'youtube'
  ], {
    noquote: ['date', 'text', 'audio']
  })
}

function formatSeriesMd (data, fdata, content) {
  // Make a date if we don't have it.
  const meta = [
    `id: ${data.id}`,
    `title: ${normalizeTitle(data.title)}`,
    data.text ? `text: ${normalizeText(data.text)}` : null,
    data.preacher ? `preacher: ${data.preacher}` : null,
    `image: ${fdata.image}`,
    data.bannerstyle ? `bannerstyle: ${data.bannerstyle}` : null
  ]
    .filter(d => !!d)
    .join('\n')
  return `---
${meta}
---

${content.trim()}
`.trim() + '\n'
}

function formatYoutubeDesc (data) {
  // Make a date if we don't have it.
  const date = data.date.toISOString ? data.date : readLADate(data.date)
  let meridiem
  switch (data.id.match(/(?<=-)(am|pm|ev)$/)[0]) {
    case 'am':
      meridiem = 'Morning'
      break
    case 'pm':
      meridiem = 'Afternoon'
      break
    case 'ev':
      meridiem = 'Evening'
      break
    default:
      throw new Error('undefined meridiem')
  }
  const content = (data.txtContent || data.mdContent || '').trim()
  const meta = [
    `Sermon: ${normalizeTitle(data.title)}`,
    `Date: ${formatLADate(date, dateFullFmt)}, ${meridiem}`,
    data.text ? `Text: ${normalizeText(data.text)}` : null,
    data.unregisteredSeries
      ? `Series: ${data.unregisteredSeries}`
      : !data.series
          ? null
          : data.series.title
            ? `Series: ${data.series.title}`
            : null,
    data.unregisteredPreacher
      ? `Preacher: ${data.unregisteredPreacher}`
      : !data.preacher
          ? null
          : data.preacher.title
            ? `Preacher: ${data.preacher.title}`
            : null,
    data.audio ? `Audio: ${data.audio}` : null
  ]
    .filter(d => !!d)
    .join('\n')
  return `${meta}

${content}
`.trim()
  // NOTE: If we were to add a newline here, YouTube would chop it off.
}

function formatFilename (data) {
  return `${data.id}-${slugify(data.title)}.md`
}

function formatSeriesFilename (data) {
  return `${data.id}.md`
}

async function readSrc (dir) {
  if (!dir) dir = srcDir
  const ret = {
    sermons: {},
    series: {},
    preachers: {},
    youtube: {},
    content: {}
  }

  const sermonPaths = await glob(path.join(srcDir, 'sermons', '*.md'))
  for (const p of sermonPaths) {
    const name = path.basename(p)
    const text = await fs.readFile(p, 'utf8')
    const { data } = matter(text)
    if (data.id) ret.sermons[data.id] = name
    if (data.youtube) ret.youtube[data.youtube] = name
    ret.content[name] = md5(text)
  }

  const seriesPaths = await glob(path.join(srcDir, 'series', '*.md'))
  for (const p of seriesPaths) {
    const text = await fs.readFile(p, 'utf8')
    const { data } = matter(text)
    ret.series[normalizeKey(data.title)] = data.id
  }

  const preachersPaths = await glob(path.join(srcDir, 'preachers', '*.md'))
  for (const p of preachersPaths) {
    const text = await fs.readFile(p, 'utf8')
    const { data } = matter(text)
    ret.preachers[normalizeKey(data.title)] = data.id
  }

  return ret
}

async function readManifest (file) {
  if (!file) file = path.join(sermonsDir, 'manifest.json')
  let text
  try {
    text = await fs.readFile(file, 'utf8')
  } catch (e) {
    console.error(`${file} was missing`)
    return {}
  }
  return JSON.parse(text)
}

async function rmf (p) {
  try {
    await fs.unlink(p)
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
  }
}

async function write (ss, sermonsDir, manifest) {
  for (const s of ss) {
    if (s.id in manifest.sermons) rmf(manifest.sermons[s.id])
    if (s.youtube in manifest.youtube) rmf(manifest.youtube[s.youtube])

    const filepath = path.join(sermonsDir, formatFilename(s))
    await fs.writeFile(filepath, formatMd(s))
  }
}

function fileMods (ss, manifest) {
  // The `mods` of this function are the action objects described here.
  // https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
  const mods = []
  const newManifest = JSON.parse(JSON.stringify(manifest))
  const sermonsPath = path.join('src', 'sermons')
  const deletes = {}
  for (const s of ss) {
    const filename = formatFilename(s)
    const idFilename = manifest.sermons[s.id]
    const ytFilename = manifest.youtube[s.youtube]
    if (idFilename && idFilename !== filename) deletes[idFilename] = true
    if (ytFilename && ytFilename !== filename) deletes[ytFilename] = true
    newManifest.sermons[s.id] = filename
    newManifest.youtube[s.youtube] = filename

    const content = formatMd(s)
    const newChecksum = md5(content)
    const oldChecksum = manifest.content[filename]
    if (oldChecksum !== newChecksum) {
      mods.push({
        action: oldChecksum ? 'update' : 'create',
        file_path: path.join(sermonsPath, filename),
        content
      })
      newManifest.content[filename] = newChecksum
    }
  }

  if (mods.length) {
    mods.push({
      action: 'update',
      file_path: path.join(sermonsPath, 'manifest.json'),
      content: stringify(newManifest, { space: 2 }) + '\n'
    })
  }

  Object.keys(deletes).forEach(k => mods.push({
    action: 'delete',
    file_path: path.join(sermonsPath, k)
  }))
  return mods
}

function md5 (content) {
  return crypto
    .createHash('md5')
    .update(content)
    .digest('base64')
}

export default {
  fileMods,
  formatFilename,
  formatMd,
  formatSeriesFilename,
  formatSeriesMd,
  formatYoutubeDesc,
  idFromDate,
  jazzTitle,
  normalizePreacher,
  normalizeText,
  populateAudioBytes,
  readManifest,
  readSrc,
  slugify,
  write
}
