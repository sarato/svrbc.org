import { promises as fs } from 'fs'
import path from 'path'
import { promisify } from 'util'

import chalk from 'chalk'
import fileType from 'file-type'
import glob from 'glob-promise'
import _imageSize from 'image-size'
import readChunk from 'read-chunk'

const imageSize = promisify(_imageSize)

// BEGIN Singletons ////////////////////////////////////////////////////////////

const SRC_PATH = path.join(__dirname, '..', 'src')

// TODO: This file should just get merged into individual tests.
const CHECKS = [
  // unexpected extensions extensions
  data => [data]
    .filter(d => !d.relpath.endsWith(`.${d.type}`))
    .map(d => `${d} images should use the .${d.type} extension`)
]

async function parseImg (filepath) {
  const stat = await fs.stat(filepath)
  if (!stat.isFile()) return null

  const buffer = await readChunk(filepath, 0, fileType.minimumBytes)
  const type = fileType(buffer)
  if (!type || !type.mime.startsWith('image/')) return null

  const data = {
    path: filepath,
    relpath: filepath.replace(SRC_PATH, ''),
    type: type.ext,
    bytes: stat.size
  }

  await Promise.all([
    (async () => {
      data.dims = await imageSize(filepath)
    })(),
    (async () => {
    })()
  ])

  return data
}

async function scan (callback) {
  const paths = await glob(path.join(SRC_PATH, '**', '*'))
  return Promise.all(paths.map(async p => {
    const data = await parseImg(p)
    // TODO: Remove these exceptions.
    if ([
      '/assets/homepage-banner.jpg'
    ].includes(p.replace(SRC_PATH, ''))) { return true }
    return data ? callback(data) : true
  }))
}

async function main () {
  const start = Date.now()

  const results = await scan(async data => {
    const messages = (await Promise.all(CHECKS.map(c => c(data)))).flat()
    if (messages.length === 0) return true

    console.log(chalk.cyan(data.path))

    for (let message of messages) {
      message = message.replace(/(^|\n)/g, '$1  ')
      console.log(chalk.red(`${message}`))
    }

    return false
  })
  const ret = results.every(v => v) ? 0 : 1

  // Finish.
  const seconds = (Date.now() - start) / 1000
  console.log(`imglint took ${seconds}s`)
  return ret
}

main().then(process.exit, console.error)
