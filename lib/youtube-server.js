import chalk from 'chalk'
import { google } from 'googleapis'
import fetch from 'node-fetch'
// This module provides an arrayFormat option, unlike the default node
// querystring module.
import querystring from 'query-string'

import {
  fromISO,
  hoursSince,
  readLADate
} from '../src/lib/datetime'
import { colorDiff } from './diff'
import { normalizeKey } from '../src/lib/datautils'
import firestore from './firestore-server'
import oauth from './oauth'
import sermons from './sermons'

const API_ROOT = 'https://www.googleapis.com/youtube/v3/'
const SVRBC_CHANNEL_ID = 'UC_C4Ho75qx256qiGnrEuIDg'
const SVRBC_UPLOADS_ID = 'UU_C4Ho75qx256qiGnrEuIDg'
const SVRBC_TOPIC_URL = `https://www.youtube.com/xml/feeds/videos.xml?channel_id=${SVRBC_CHANNEL_ID}`
const SVRBC_CALLBACK_URL = 'https://svrbc.org/.netlify/functions/onyoutubeupdate'
// https://developers.google.com/youtube/v3/docs/search/list#maxResults
const MAX_RESULTS = 50

const DESC_VALUES = {
  title: 'Sermon',
  date: 'Date',
  text: 'Text',
  series: 'Series',
  preacher: 'Preacher',
  audio: 'Audio'
}
const DESC_VALUES_REVERSE =
  Object.fromEntries(Object.entries(DESC_VALUES).map(([k, v]) => [v, k]))
const DESC_VALUES_REGEX =
  new RegExp(`(${Object.values(DESC_VALUES).join('|')}): (.*)(\n|$)`, 'g')

const TIME_MAP = {
  Morning: 'am',
  Afternoon: 'pm',
  Evening: 'ev'
}

function getKey () {
  if (process.env.TEST === 'true') return 'fakekey'
  if (!process.env.YOUTUBE_API_KEY) throw new Error('YOUTUBE_API_KEY not set')
  return process.env.YOUTUBE_API_KEY
}

function apiUrl (path, params) {
  const newParams = querystring.stringify(Object.assign({
    key: getKey()
  }, params), {
    arrayFormat: 'comma'
  })
  return `${API_ROOT}${path}?${newParams}`
}

async function apiFetch (path, params, body, method) {
  const fetchOpts = {}
  if (body) {
    fetchOpts.method = 'PUSH'
    fetchOpts.body = JSON.stringify(body)
    fetchOpts.headers = {
      'Content-Type': 'application/json'
    }
  }
  if (method) fetchOpts.method = method
  const url = apiUrl(path, params)
  const resp = await fetch(url, fetchOpts)
  const data = await resp.json()
  if (resp.status !== 200) {
    console.error(`Error fetching ${url}`)
    console.error(JSON.stringify(data, null, 2))
    throw new Error(`Error ${resp.status} from YouTube`)
  }
  return data
}

async function apiFetchList (path, params, cont) {
  params = Object.assign({
    maxResults: MAX_RESULTS
  }, params)
  cont = cont || (i => true)

  const items = []
  while (true) {
    const result = await apiFetch(path, params)
    for (const item of result.items) {
      if (!cont(item)) return items
      items.push(item)
    }
    if (!result.nextPageToken) break
    params.pageToken = result.nextPageToken
  }
  return items
}

async function subscribe () {
  console.log(SVRBC_TOPIC_URL)
  console.log(SVRBC_CALLBACK_URL)
  console.log(process.env.YOUTUBE_CALLBACK_SECRET)
  await fetch('https://pubsubhubbub.appspot.com/?' + querystring.stringify({
    'hub.mode': 'subscribe',
    'hub.topic': SVRBC_TOPIC_URL,
    'hub.callback': SVRBC_CALLBACK_URL,
    'hub.secret': process.env.YOUTUBE_CALLBACK_SECRET
  }), {
    method: 'POST'
  })
}

async function getChannel () {
  const channels = await apiFetchList('channels', {
    id: SVRBC_CHANNEL_ID,
    part: 'contentDetails'
  })
  return channels[0]
}

async function getVideos (ids) {
  return apiFetchList('videos', {
    id: ids,
    part: 'snippet'
  })
}

async function getSermons (ids, opts) {
  const videos = await getVideos(ids)
  return videos
    .map(v => parseSermonDataFromVideo(v, opts))
    .filter(s => !!s)
}

async function getAllVideos () {
  const videos = await apiFetchList('playlistItems', {
    playlistId: SVRBC_UPLOADS_ID,
    part: 'snippet'
  })
  videos.forEach(v => {
    v.id = videoIdFromVideo(v)
    v.url = urlFromVideo(v)
  })
  return videos
}

// after: All sermons returned will be published after this date.
//   This should be a javascript Date.
// Right now this function relies on the unspecified behavior that results will
// be returned sorted in descending order of the publishing date.
async function getNewVideos (after) {
  return apiFetchList('playlistItems', {
    playlistId: SVRBC_UPLOADS_ID,
    part: 'snippet'
  }, v => fromISO(v.snippet.publishedAt) > after)
}

async function getAllSermons (opts) {
  const videos = await getAllVideos()
  return videos
    .map(v => parseSermonDataFromVideo(v, opts))
    .filter(s => !!s)
}

// https://developers.google.com/youtube/v3/docs/videos/update
async function updateVideo (v) {
  await apiFetch('videos', {
    part: 'snippet'
  }, {
    snippet: v.snippet
  }, 'PUT', true)
}

function videoIdFromVideo (v) {
  let id = v.id
  if (typeof id !== 'string') id = v.id.videoId
  if (!id) id = v.snippet.resourceId.videoId
  return id
}

function urlFromVideo (data) {
  const id = videoIdFromVideo(data)
  return `https://www.youtube.com/watch?v=${id}`
}

function parseSermonDataFromVideo (data, opts) {
  opts = Object.assign({
    series: {}
  }, opts)

  const url = urlFromVideo(data)
  const desc = data.snippet.description

  const values = {}
  for (const match of desc.matchAll(DESC_VALUES_REGEX)) {
    values[DESC_VALUES_REVERSE[match[1]]] = match[2].trim()
  }
  if (!values.title) return null
  if (!values.date) return null

  // Normalize fields
  const ampmMatch = values.date.match(/Morning|Afternoon/)
  if (!ampmMatch) {
    throw new Error(`Date "${values.date}" does not include ` +
      `"Morning" or "Afternoon": ${url}`)
  }
  const ampm = TIME_MAP[ampmMatch[0]]

  const date = readLADate(values.date)
  if (!date) {
    throw new Error(`Date "${values.date}" could not be parsed: ${url}`)
  }

  values.txtContent = desc.replace(DESC_VALUES_REGEX, '').trim()

  const seriesId = opts.series[normalizeKey(values.series || '')]
  if (seriesId) {
    values.series = {
      id: seriesId,
      title: values.series
    }
  } else {
    if (values.series) values.unregisteredSeries = values.series
    delete values.series
  }

  const preacherName = sermons.normalizePreacher(values.preacher || '')
  const preacherId = opts.preachers[normalizeKey(preacherName)]
  if (preacherId) {
    values.preacher = {
      id: preacherId,
      title: preacherName
    }
  } else {
    if (preacherName) values.unregisteredPreacher = preacherName
    delete values.preacher
  }

  return Object.assign(values, {
    date,
    id: sermons.idFromDate(date, ampm),
    youtube: videoIdFromVideo(data),
    // This is for easy debugging.
    youtubeUrl: url
  })
}

function normalizeVideo (v, opts) {
  const n = JSON.parse(JSON.stringify(v))
  const s = parseSermonDataFromVideo(v, opts)
  if (s === null) return n
  n.snippet.description = sermons.formatYoutubeDesc(s)
  return n
}

function liveOrSoon (broadcasts) {
  const notEnded = broadcasts
    .filter(b => b.snippet)
    .filter(b => !b.snippet.actualEndTime)
  if (!notEnded.length) return null
  for (const b of notEnded) {
    if (b.snippet.actualStartTime) return b
  }

  const t = x => fromISO(x.snippet.scheduledStartTime)
  const now = new Date()
  const notStarted = notEnded
    .filter(b => !!b.snippet.scheduledStartTime)
    // If it's been more than two hours since the scheduled start, something
    // is probably wrong and we shouldn't consider this video as close to
    // starting.
    .filter(b => hoursSince(t(b), { now }) < 2)
    .sort((a, b) => t(a).getTime() - t(b).getTime())
  if (!notStarted.length) return null
  if (notStarted.length === 1) return null
  return notStarted[0]
}

class YoutubeClient {
  constructor (origin, auth) {
    this.origin = origin
    this.auth = auth
    this.client = google.youtube({
      version: 'v3',
      auth
    })
  }

  async verifyUser () {
    const channel = await this.getMyChannel()
    return channel.id === SVRBC_CHANNEL_ID
  }

  async getMyChannel () {
    const resp = await this.client.channels.list({
      part: 'id',
      mine: true
    })
    return resp.data.items[0]
  }

  async storeToken () {
    const db = firestore.getClient(this.origin)
    await db.setToken('youtube', this.auth.credentials)
  }

  async list (kind, params, limit) {
    params = Object.assign({
      maxResults: MAX_RESULTS
    }, params)

    const ret = []
    while (true) {
      const { data } = await kind.list(params)
      ret.push(...data.items)
      if (limit && ret.length >= limit) break
      if (!data.nextPageToken) break
      params.nextPageToken = data.nextPageToken
    }
    return ret
  }

  async getVideos (...ids) {
    return this.list(this.client.videos, {
      id: ids.join(','),
      part: 'snippet'
    })
  }

  async getLiveOrSoon () {
    const broadcasts = await this.getBroadcasts()
    return liveOrSoon(broadcasts)
  }

  async getBroadcasts (opts) {
    opts = Object.assign({
      limit: 10
    }, opts)
    return this.list(this.client.liveBroadcasts, {
      part: 'snippet',
      mine: true
    }, opts.limit)
  }

  async updateVideo (v) {
    return this.client.videos.update({
      part: 'snippet',
      requestBody: v
    })
  }

  async normalizeVideos (videos, opts) {
    opts = Object.assign({
      verbose: false
    }, opts)
    const normalized = []
    for (const v of videos) {
      const n = normalizeVideo(v, opts)
      let ok = true
      if (v.title !== n.title) {
        if (opts.verbose) {
          console.log('`title` diff:')
          console.log(colorDiff(v.title, n.title))
        }
        ok = false
      }
      if (n.snippet.description !== v.snippet.description) {
        if (opts.verbose) {
          console.log('`snippet.description` diff:')
          console.log(colorDiff(v.snippet.description, n.snippet.description))
        }
        ok = false
      }
      if (ok) continue

      console.log(chalk.yellow(`Updating ${v.id}…`))
      await this.updateVideo(n)
      normalized.push(v.id)
    }

    return normalized
  }

  parseSermon (v, opts) {
    return parseSermonDataFromVideo(v, opts)
  }
}

async function getClient (opts = {}) {
  const origin = process.env.OVERRIDE_URL ||
    process.env.URL ||
    'http://localhost:8888'
  let oauthOpts
  if (opts.code) {
    oauthOpts = {
      code: opts.code
    }
  } else {
    const db = firestore.getClient(origin)
    oauthOpts = await db.getToken('youtube')
  }
  let auth
  if (oauthOpts) {
    auth = await oauth.getClient(origin, oauthOpts)
  } else {
    console.error('No auth token available.  Defaulting to API key.')
    auth = process.env.YOUTUBE_API_KEY
  }
  return new YoutubeClient(origin, auth)
}

export default {
  SVRBC_TOPIC_URL,

  getAllVideos,
  getAllSermons,
  getChannel,
  getClient,
  getNewVideos,
  getVideos,
  getSermons,
  liveOrSoon,
  normalizeVideo,
  parseSermonDataFromVideo,
  subscribe,
  updateVideo,
  urlFromVideo,
  videoIdFromVideo,

  // This is only exported for testing.
  apiUrl
}

export {
  YoutubeClient
}
