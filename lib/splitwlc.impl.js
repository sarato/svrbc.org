import { promises as fs } from 'fs'
import yaml from 'js-yaml'

import cli from './cli'
import sermons from './sermons'

function getLong (t, indent, additional) {
  if (t.length + indent + additional < 80) return t
  let ret = '>\n' + ' '.repeat(indent - 1)
  let cur = indent - 1
  for (const w of t.split(/\s+/)) {
    cur += 1 + w.length
    if (cur < 80) {
      ret += ' ' + w
      continue
    }
    ret += '\n' + ' '.repeat(indent) + w
    cur = indent + w.length
  }
  return ret
}

async function handle (q) {
  const segments = q.answer
    .split(/\[[0-9]+\]/)
    .filter(a => a)
    .map((a, i) => ({
      a: a.trim(),
      v: q.verses[i + 1]
    }))
    .map(s => {
      if (s.a.endsWith(':') && s.a.length < 70) {
        s.a = `"${s.a}"`
      }
      let v = ''
      if (s.v) {
        v = sermons.normalizeText(s.v.join(' '))
      }
      const fullv = v ? `\n  - verses: ${getLong(v, 6, 6)}` : ''
      return `  - text: ${getLong(s.a, 6, 4)}${fullv}`
    })
    .join('\n')
  const data = `id: ${q.number}
question: ${getLong(q.question, 2, 0)}
segments:
${segments}
`
  const padded = ('' + q.number).padStart(3, '0')
  await fs.writeFile(`./src/catechism/wlc/q${padded}.yml`, data)
}

async function main () {
  const contents = await fs.readFile('wlc.yml', 'utf8')
  const wlc = yaml.safeLoad(contents)
  for (const q of wlc.questions) {
    await handle(q)
  }
  return 0
}

cli.run('updateallsermons', main, {
  summary: false
})
