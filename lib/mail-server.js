// If we don't import the dist file, we get this error:
// require.extensions is not supported by webpack. Use a loader instead.
// But configuring webpack for netlify-lambda and jest is too hard at the
// moment.
import FormData from 'form-data'
import fetch from 'node-fetch'

import templates from './mail-templates'

const MAILGUN_DOMAIN = 'https://api.mailgun.net/v3/mail.svrbc.org'

const EMAILS = {
  conley: {
    name: 'Conley Owens',
    address: 'conley@svrbc.org'
  },
  'daisy-guardado': {
    name: 'Daisy Guardado',
    address: 'daisy.garcia.dg@gmail.com'
  },
  'dale-hardin': {
    name: 'Dale Hardin',
    address: 'dkhardin10@gmail.com'
  },
  'james-torrefranca': {
    name: 'James Torrefranca',
    address: 'jamesat@gmail.com'
  },
  john: {
    name: 'John Burchett',
    address: 'john@svrbc.org'
  },
  'john-davis': {
    name: 'John Davis',
    address: 'jdavis@pivotinteriors.com'
  },
  josh: {
    name: 'Josh Sheldon',
    address: 'josh@svrbc.org'
  },
  info: {
    name: 'SVRBC Officers',
    address: 'info@svrbc.org'
  },
  'ken-tompkins': {
    name: 'Ken Tompkins',
    address: 'ken.tompkins@usa.net'
  },
  sarah: {
    name: 'Sarah Owens',
    address: 'sarah@svrbc.org'
  },
  test: {
    name: 'Test',
    address: 'test@svrbc.org'
  }
}

function transformEmail ({ name, note, address }) {
  const displayNameParts = []
  if (name) displayNameParts.push(name)
  if (note) displayNameParts.push(`{${note}}`)
  const displayName = displayNameParts.join(' ')

  const parts = []
  if (displayName) parts.push(displayName)
  parts.push(`<${address}>`)
  return parts.join(' ')
}

// https://documentation.mailgun.com/en/latest/api-sending.html#sending
function createSendRequest (opts) {
  if (!opts.from) throw new Error('sender is missing in email config')
  if (!opts.to) throw new Error('recipient is missing in email config')
  if (!opts.data) throw new Error('data is missing in email config')
  if (!opts.template) throw new Error('template is missing in email config')

  const template = templates[opts.template]
  if (!template) throw new Error(`template "${opts.template}" is unknown`)
  const subject = template.subject(opts.data).trim()
  const body = template.body(opts.data).trim()

  const dev = process.env.DEV_EMAIL && process.env.SVRBC_TEST !== 'true'
  const to = opts.to
    .map(t => {
      const record = EMAILS[t]
      if (!record) throw new Error(`email for "${t}" unknown`)
      return transformEmail({
        name: record.name,
        address: dev ? process.env.DEV_EMAIL : record.address,
        note: dev ? record.address : undefined
      })
    })

  // We can't send email on behalf of Yahoo.
  // Might as well not send on behalf of any.
  // https://sendgrid.com/blog/aol-dmarc-update/
  const data = new FormData()
  data.append('from', transformEmail({
    name: `${opts.from.name} (${opts.from.address}) via SVRBC.org`,
    address: 'webmail@svrbc.org'
  }))
  data.append('h:Reply-To', transformEmail(opts.from))
  to.forEach(t => data.append('to', t))
  data.append('subject', subject)
  data.append('html', body)
  data.test = opts.test
  return data
}

function mockSend (req) {
  const boundaryRe = new RegExp(req.getBoundary(), 'g')
  const data = req.getBuffer().toString().replace(boundaryRe, '')
  console.log('=== Mail to be sent ===')
  console.log(data)
  console.log('=======================')
  return {
    message: 'Mocked',
    data
  }
}

async function send (req) {
  if (!process.env.MAILGUN_API_KEY) {
    console.log('Mailgun API key not set')
    return mockSend(req)
  }
  if (req.test) return mockSend(req)
  if (process.env.SVRBC_TEST === 'true') return mockSend(req)

  const headers = req.getHeaders()
  const auth = Buffer.from(`api:${process.env.MAILGUN_API_KEY}`)
    .toString('base64')
  headers.Authorization = `Basic ${auth}`
  const resp = await fetch(`${MAILGUN_DOMAIN}/messages`, {
    method: 'POST',
    body: req,
    headers
  })
  console.log('Sent request')
  console.log(JSON.stringify(req, null, 2))

  const data = await resp.json()
  if (resp.status !== 200) {
    console.error(data)
    throw new Error(`Error ${resp.status} from Mailgun server`)
  }
  return data
}

export default {
  createSendRequest,
  send
}
