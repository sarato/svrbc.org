import chalk from 'chalk'
import cli from './cli'
import youtube from './youtube-server'

async function main () {
  console.log(chalk.cyan('Subscribing to push notifications…'))
  await youtube.subscribe()
  return 0
}

cli.run('subscribetoyoutube', main, {
  env: [
    'YOUTUBE_CALLBACK_SECRET'
  ]
})
