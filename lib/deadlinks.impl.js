import AwaitLock from 'await-lock'
import chalk from 'chalk'
import delay from 'delay'
import fetch from 'node-fetch'

import Timer from '../src/lib/timer'
import dist from './dist'

const SECOND = 1000

const delays = {
  'www.youtube.com': 10 * SECOND
}

async function main () {
  const timer = new Timer()

  let ret = 0
  const hosts = Object.keys(delays)
  const locks = Object.fromEntries(hosts.map(k => [k, new AwaitLock()]))
  await dist.scan(async data => {
    if (data.type !== 'html') return
    const as = data.a
      .filter(a => a.attribs.href.match(/^[a-z]+:/))
      .filter(a => !a.attribs.href.match(/^mailto:/))
    for (const a of as) {
      const { href: url, 'data-allowstatus': allow } = a.attribs
      const statusOk = s => [
        200,
        ...(allow ? allow.split(',') : []).map(parseInt)
      ].includes(s)

      const host = new URL(url).host
      const lock = locks[host]
      if (lock) {
        await lock.acquireAsync()
        delay(delays[host]).then(() => lock.release())
      }

      let resp = await fetch(url, { method: 'HEAD' })
      if (statusOk(resp.status)) continue

      // It's possible the server does not have HEAD implemented, so fire a
      // GET request as a second pass.
      resp = await fetch(url, { method: 'GET' })
      if (statusOk(resp.status)) continue

      console.log(chalk.red(`${data.relpath}: ${resp.status}: ${url}`))
      ret = 1
    }
  })

  timer.pretty('deadlinks')
  return ret
}

main().then(process.exit, console.error)
