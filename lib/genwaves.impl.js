import chalk from 'chalk'
import { exec as _exec } from 'child_process'
import { promises as fs } from 'fs'
import mp3Duration from 'mp3-duration'
import path from 'path'
import util from 'util'

import cli from './cli'
import cloudstorage from './cloudstorage'
import tmp from 'tmp-promise'

const exec = util.promisify(_exec)

async function processFiles (storage, files, tmpDir) {
  console.log(chalk.cyan(`Processing ${files.length} files…`))
  for (const f of files) {
    console.log(chalk.cyan(`Downloading ${f.metadata.name}…`))
    const destination = path.join(tmpDir, f.metadata.name)
    await storage.download(f, destination)

    console.log(chalk.cyan(`Getting duration of ${f.metadata.name}…`))
    const duration = await mp3Duration(destination)
    const mins = parseInt(duration / 60)

    console.log(chalk.cyan(`Getting waveform of a ${mins}m file…`))
    // This zoom level will give us roughly a 4k file.
    const zoom = parseInt(100 * duration)
    const { stdout } = await exec([
      'audiowaveform',
      '--bits=8',
      '--output-format=json',
      `--input-filename=${destination}`,
      `--zoom=${zoom}`
    ].join(' '))
    const data = JSON.parse(stdout)
    // We could normalize the values here, but using floats in a JSON file
    // increases the file size.

    const jsonPath = destination.replace(/.mp3$/, '.json')
    console.log(chalk.cyan(`Writing file ${jsonPath}…`))
    await fs.writeFile(jsonPath, JSON.stringify(data), 'utf8')

    const bucketPath = path.join(
      'waveforms',
      f.metadata.bucket,
      f.metadata.name.replace(/.mp3$/, '.waveform.json'))
    console.log(chalk.cyan(`Uploading JSON file to ${bucketPath}…`))
    await storage.upload(jsonPath, bucketPath)
  }
}

async function main ({ _: [prefix] }) {
  console.log(chalk.cyan('Listing files…'))
  const storage = cloudstorage.getClient('svrbc-web')
  const files = await storage.list(prefix)

  await tmp.withDir(async o => {
    try {
      await processFiles(storage, files, o.path)
    } catch (e) {
      console.error(chalk.red(e.message))
      console.error(chalk.red(e.stack))
    }
  }, {
    prefix: 'genwaves',
    unsafeCleanup: true
  })

  return 0
}

cli.run('genwaves', main, {
  env: [
    'GCP_SERVICE_ACCOUNT_PRIVATE_KEY'
  ]
})
