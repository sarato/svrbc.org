/* eslint-env jest */
import { fromISO } from '../src/lib/datetime'
import youtube from './youtube-server'

const parseMaps = {
  preachers: {
    'charles spurgeon': 'chs'
  },
  series: {
    'opinions and eisegesis': 'opinions-and-eisegesis'
  }
}

describe('apiUrl', () => {
  it('array works', () => {
    expect(youtube.apiUrl('search', {
      id: ['1', '2', '3']
    }).replace(/&?key=.*?(?=(&|$))/, ''))
      .toEqual('https://www.googleapis.com/youtube/v3/search?id=1,2,3')
  })
})

describe('youtube.parseSermonDataFromVideo', () => {
  it('empty generates no sermon', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: ''
      }
    })).toBeNull()
  })

  it('missing date raises error', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    })).toBeNull()
  })

  it('complete data generates sermon', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('missing series ignored', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Date: March 6, 2019 Afternoon
Sermon: A Biblical Argument Against Smores
Text: Job 6:6-7
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {}
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('unregistered preacher handled', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Date: March 6, 2019 Afternoon
Sermon: A Biblical Argument Against Smores
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {},
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      unregisteredPreacher: 'Charles Spurgeon',
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('missing preacher handled', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Date: March 6, 2019 Afternoon
Sermon: A Biblical Argument Against Smores
Series: Opinions and Eisegesis
Text: Job 6:6-7
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {},
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('preacher with title handle', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Pastor Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('missing content (and newline) succeeds', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: ''
    })
  })

  it('parse Morning message', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Morning
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {
        'opinions and eisegesis': 'opinions-and-eisegesis'
      }
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-am',
      date: fromISO('2019-03-06T19:00:00.000Z'),
      text: 'Job 6:6-7',
      series: {
        id: 'opinions-and-eisegesis',
        title: 'Opinions and Eisegesis'
      },
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('error if no Morning or Afternoon', () => {
    expect(() => youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    })).toThrow()
  })

  it('error if date is not parseable', () => {
    expect(() => youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 66, 2019 evening
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    })).toThrow()
  })

  it('tolerates incorrect series', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {}
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      unregisteredSeries: 'Opinions and Eisegesis',
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })

  it('tolerates no series', () => {
    expect(youtube.parseSermonDataFromVideo({
      id: { videoId: 'notreal' },
      snippet: {
        description: `
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3

A surprising message!
`
      }
    }, {
      preachers: {
        'charles spurgeon': 'chs'
      },
      series: {}
    })).toEqual({
      title: 'A Biblical Argument Against Smores',
      id: '2019-03-06-pm',
      date: fromISO('2019-03-06T22:00:00.000Z'),
      text: 'Job 6:6-7',
      preacher: {
        id: 'chs',
        title: 'Charles Spurgeon'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'notreal',
      youtubeUrl: 'https://www.youtube.com/watch?v=notreal',
      txtContent: 'A surprising message!'
    })
  })
})

describe('liveOrSoon', () => {
  it('ignores missing snippet', () => {
    expect(youtube.liveOrSoon([{}])).toBeNull()
  })

  it('ignores missing end time', () => {
    expect(youtube.liveOrSoon([{
      snippet: {
        actualStartTime: new Date().toISOString(),
        actualEndTime: new Date().toISOString()
      }
    }])).toBeNull()
  })

  it('returns started entry', () => {
    const b = {
      snippet: {
        actualStartTime: new Date().toISOString()
      }
    }
    expect(youtube.liveOrSoon([b])).toEqual(b)
  })

  it('ignores missing scheduled time', () => {
    expect(youtube.liveOrSoon([{
      snippet: {}
    }])).toBeNull()
  })

  it('ignores old scheduled time', () => {
    expect(youtube.liveOrSoon([{
      snippet: {
        scheduledStartTime: new Date(0).toISOString()
      }
    }])).toBeNull()
  })

  it('returns soonest entry', () => {
    const now = new Date()
    const b1 = {
      snippet: {
        scheduledStartTime: now.toISOString()
      }
    }
    const b2 = {
      snippet: {
        scheduledStartTime: new Date(now.getTime() + 1000).toISOString()
      }
    }
    expect(youtube.liveOrSoon([b2, b1])).toEqual(b1)
  })
})

/* eslint-disable no-irregular-whitespace */
describe('normalizeVideo', () => {
  it('description normalized', () => {
    const run = (description) => {
      return youtube.normalizeVideo({
        id: { videoId: 'notreal' },
        snippet: { description }
      }, parseMaps).snippet.description
    }
    expect(run('this is not a sermon')).toEqual('this is not a sermon')
    expect(run(`
Sermon: A Biblical Argument Against Smores
Date: March 6, 2019 Afternoon
Text: Job 6:6-7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3
`)).toEqual(`Sermon: A Biblical Argument Against Smores
Date: March 6, 2019, Afternoon
Text: Job 6:6–7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3`)
  })
})
/* eslint-enable no-irregular-whitespace */
