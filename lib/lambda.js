import { parseStringPromise as parseXML } from 'xml2js'
import crypto from 'crypto'
import dotenv from 'dotenv'
import querystring from 'query-string'

import './lambda-env'
import recaptcha from './recaptcha-server'

dotenv.config()

const HEADERS = {
  recaptchaToken: 'recaptcha-token',
  hubSignature: 'x-hub-signature',
  host: 'host'
}

async function getParams (e) {
  const headers = Object.fromEntries(
    Object.entries(HEADERS)
      .map(([k, v]) => [k, e.headers[v]])
      .filter(([_, v]) => v))
  let body = {}
  switch (e.headers['content-type']) {
    case 'application/json':
    case 'text/json':
      body = JSON.parse(e.body)
      break
    case 'text/xml':
    case 'application/xml':
    case 'application/atom+xml':
      body = await parseXML(e.body)
      break
    case 'application/x-www-form-urlencoded':
      body = querystring.parse(e.body)
      break
  }
  return Object.assign({
    httpMethod: e.httpMethod
  }, headers, body, e.queryStringParameters)
}

// Match Secret
function verifyHmac (headers, secret, body) {
  const hubSignature = headers['x-hub-signature']
  if (!hubSignature) throw new Error('no signature provided')
  const hubSplit = hubSignature.split('=')
  if (hubSplit.length !== 2) throw new Error('signature malformed')
  const [algo, sig] = hubSplit

  let hmac
  try {
    hmac = crypto.createHmac(algo, secret)
  } catch (e) {
    console.log(`Unable to hash with algorithm ${algo}`)
    return false
  }

  const digest = hmac
    .update(body)
    .digest('hex')
    .toLowerCase()
  if (digest !== sig) throw new Error('signature does not match')
}

export default (consume, produce, opts = {}) => {
  return async (event, context, callback) => {
    // Parse the request.
    let req
    try {
      req = await getParams(event)
    } catch (e) {
      console.error('error parsing request:', e)
      console.error('body', event.body)
      callback(null, {
        statusCode: 400,
        body: JSON.stringify({
          message: 'Could not parse request as JSON or XML'
        })
      })
      return
    }

    // Force an error.
    if (req.forceError) {
      console.log('forcing an error')
      callback(null, {
        statusCode: parseInt(req.forceError) || 500,
        body: JSON.stringify({
          message: 'Forced error'
        })
      })
      return
    }

    // Verify HMAC.
    if (opts.hmacSecret && event.httpMethod === 'POST' &&
      opts.hmacSecret !== req.hmac) {
      console.log('req', req)
      try {
        verifyHmac(event.headers, opts.hmacSecret, event.body)
      } catch (e) {
        console.error('could not verify hmac secret:', e.message)
        callback(null, {
          statusCode: 403,
          body: JSON.stringify({
            error: e.message
          })
        })
        return
      }
      console.log('hmac verified')
    }

    let digest
    try {
      digest = await consume(req)
    } catch (e) {
      console.error('error consuming request:', e.message)
      callback(null, {
        statusCode: 422,
        body: JSON.stringify({
          error: e.message
        })
      })
      return
    }

    if (opts.recaptchaActions) {
      if (!req.recaptchaToken) {
        console.error('no recaptcha token provided')
        callback(null, {
          statusCode: 429,
          body: JSON.stringify({
            error: 'no recaptcha token provided'
          })
        })
        return
      }
      try {
        await recaptcha.verify(req.recaptchaToken, opts.recaptchaActions)
      } catch (e) {
        console.error('error verifying recaptcha')
        console.error(e.stack)
        callback(null, {
          statusCode: 429,
          body: JSON.stringify({
            error: e.message
          })
        })
        return
      }
    }

    let data
    try {
      data = await produce(digest)
    } catch (e) {
      console.error('error producing response')
      console.error(e.stack)
      callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          error: e.message
        })
      })
      return
    }

    if (opts.closeOnSuccess) {
      callback(null, {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/plain'
        },
        body: '<script type="text/javascript">window.close();</script>' +
          'Success!'
      })
      return
    }

    let contentType, body
    switch (opts.responseContentType) {
      case 'text':
        contentType = 'text/plain'
        body = data
        break
      default:
        contentType = 'application/json'
        body = JSON.stringify(data)
    }
    callback(null, {
      statusCode: 200,
      headers: {
        'Content-Type': contentType
      },
      body
    })
  }
}
