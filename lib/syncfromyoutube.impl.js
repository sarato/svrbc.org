import chalk from 'chalk'
import { promises as fs } from 'fs'
import path from 'path'
import rimraf from 'rimraf-promise'

import cli from './cli'
import sermons from './sermons'
import youtube from './youtube-server'

const NEW_SERMONS_DIR = '/tmp/newsermons'

async function main () {
  console.log(chalk.cyan('Fetching all series…'))
  const seriesMap = await sermons.getSeriesTitleMap()

  console.log(chalk.cyan('Listing all sermons…'))
  const ss = await youtube.getAllSermons({ seriesMap })

  await rimraf(NEW_SERMONS_DIR)
  fs.mkdir(NEW_SERMONS_DIR)

  console.log(chalk.green(`Writing ${ss.length} files…`))
  for (const s of ss) {
    const filepath = path.join(NEW_SERMONS_DIR, sermons.formatFilename(s))
    await fs.writeFile(filepath, sermons.formatMd(s))
  }

  return 0
}

cli.run('syncfromyoutube', main)
