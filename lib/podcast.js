import gql from 'graphql-tag'
import { Parser } from 'htmlparser2'
import Podcast from 'podcast'

import { Sermon } from '../src/lib/data'
import graphql from './graphql'

import templates from './templates'

const ITUNES_LIMIT = 300
const CHURCH = 'Silicon Valley Reformed Baptist Church'

function htmlToText (html) {
  const segments = []
  const parser = new Parser({
    onopentag (name, attribs) {
      if (name === 'p') segments.push('\n\n')
    },
    ontext (text) {
      segments.push(text)
    }
  })
  parser.write(html)
  parser.end()
  return segments.join(' ').trim()
}

async function generate () {
  // Query the sermons
  const res = await graphql.query(gql`{
    metadata {
      siteUrl
    }
    allSermon(sortBy: "date", order: DESC, limit: ${ITUNES_LIMIT}) {
      edges {
        node {
          audio
          audioBytes
          content
          date
          path
          preacher {
            title
          }
          title
          text
          series {
            title
            path
          }
          unregisteredPreacher
          youtube
        }
      }
    }
  }`)

  const siteUrl = res.data.metadata.siteUrl

  const htmlTemplate = await templates.load('podcast.description.html')
  const txtTemplate = await templates.load('podcast.description.txt')

  // For fields see: https://github.com/maxnowack/node-podcast
  const feed = new Podcast({
    title: 'SVRBC Sermons',
    description: `Sermons from ${CHURCH}`,
    feedUrl: `${siteUrl}/sermons.rss`,
    siteUrl: `${siteUrl}/sermons/`,
    imageUrl: `${siteUrl}/podcast/sermon.jpg`,
    author: CHURCH,
    language: 'en',
    itunesAuthor: CHURCH,
    itunesSummary: `Sermons from ${CHURCH}`,
    itunesOwner: {
      name: CHURCH,
      email: 'info@svrbc.org'
    },
    itunesExplicit: false,
    itunesCategory: [{
      text: 'Religion & Spirituality',
      subcats: [{
        text: 'Christianity'
      }]
    }]
  })

  res.data.allSermon.edges
    .map(e => new Sermon(Object.assign(e.node, {
      txtContent: htmlToText(e.node.content),
      hasAudio: e.node.audio && e.node.audioBytes,
      siteUrl
    })))
    .forEach(s => feed.addItem(Object.assign({
      title: s.title,
      description: htmlTemplate(s),
      url: `${siteUrl}${s.path}`,
      // We could use s.preacherName here, but some podcatchers look at the
      // individual episodes to get the overall author.
      author: CHURCH,
      date: s.realDate,
      itunesAuthor: CHURCH,
      itunesTitle: s.title,
      itunesSummary: txtTemplate(s),
      itunesImage: `${siteUrl}/podcast/sermon.jpg`
    }, (s.audio && s.audioBytes)
      ? {
          enclosure: {
            url: s.audio,
            size: s.audioBytes
          }
        }
      : null)))

  return feed.buildXml('  ')
}

export default {
  ITUNES_LIMIT,
  generate
}

export {
  ITUNES_LIMIT,
  generate
}
