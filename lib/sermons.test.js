/* eslint-env jest */
import sermons from './sermons'

const VIDEO = {
  id: '2020-01-01-pm',
  date: '2020-01-01',
  content: '',
  text: 'Job 6:6-7',
  title: 'A Biblical Argument Against Smores',
  series: {
    id: 'opinions-and-eisegesis',
    title: 'Opinions and Eisegesis'
  },
  preacher: {
    id: 'chs',
    title: 'Charles Spurgeon'
  },
  audio: 'http://example.com/audio.mp3',
  youtube: 'vidId'
}

describe('formatFilename', () => {
  it('works for basic data', () => {
    expect(sermons.formatFilename({
      id: '2020-01-01-pm',
      title: 'A Biblical Argument Against Smores'
    })).toEqual('2020-01-01-pm-a-biblical-argument-against-smores.md')
  })

  it('slugify possessives correctly', () => {
    expect(sermons.formatFilename({
      id: '2020-01-01-pm',
      title: 'God’s Biblical Argument Against Smores'
    })).toEqual('2020-01-01-pm-gods-biblical-argument-against-smores.md')
  })

  it('slugify non-curly possessives correctly', () => {
    expect(sermons.formatFilename({
      id: '2020-01-01-pm',
      title: "God's Biblical Argument Against Smores"
    })).toEqual('2020-01-01-pm-gods-biblical-argument-against-smores.md')
  })
})

describe('formatMd', () => {
  it('works for basic data', () => {
    expect(sermons.formatMd({
      id: '2020-01-01-pm',
      date: '2020-01-01',
      content: '',
      text: 'Job 6:6-7',
      title: 'A Biblical Argument Against Smores',
      series: {
        id: 'opinions-and-eisegesis'
      },
      preacher: {
        id: 'chs'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'vidId'
    })).toEqual(`---
id: 2020-01-01-pm
title: A Biblical Argument Against Smores
date: 2020-01-01
text: Job 6:6–7
preacher: chs
series: opinions-and-eisegesis
audio: http://example.com/audio.mp3
youtube: vidId
---
`)
  })

  it('text is normalized', () => {
    expect(sermons.formatMd({
      id: '2020-01-01-pm',
      date: '2020-01-01',
      content: '',
      text: 'Jb 6:6-7',
      title: 'A Biblical Argument Against Smores',
      series: {
        id: 'opinions-and-eisegesis'
      },
      preacher: {
        id: 'chs'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'vidId'
    })).toEqual(`---
id: 2020-01-01-pm
title: A Biblical Argument Against Smores
date: 2020-01-01
text: Job 6:6–7
preacher: chs
series: opinions-and-eisegesis
audio: http://example.com/audio.mp3
youtube: vidId
---
`)
  })

  it('tolerate missing series', () => {
    expect(sermons.formatMd({
      id: '2020-01-01-pm',
      date: '2020-01-01',
      content: '',
      text: 'Job 6:6-7',
      title: 'A Biblical Argument Against Smores',
      preacher: {
        id: 'chs'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'vidId'
    })).toEqual(`---
id: 2020-01-01-pm
title: A Biblical Argument Against Smores
date: 2020-01-01
text: Job 6:6–7
preacher: chs
audio: http://example.com/audio.mp3
youtube: vidId
---
`)
  })

  it('tolerate unregistered series', () => {
    expect(sermons.formatMd({
      id: '2020-01-01-pm',
      date: '2020-01-01',
      content: '',
      text: 'Job 6:6-7',
      title: 'A Biblical Argument Against Smores',
      unregisteredSeries: 'Opinions and Eisegesis',
      preacher: {
        id: 'chs'
      },
      audio: 'http://example.com/audio.mp3',
      youtube: 'vidId'
    })).toEqual(`---
id: 2020-01-01-pm
title: A Biblical Argument Against Smores
date: 2020-01-01
text: Job 6:6–7
preacher: chs
unregisteredSeries: Opinions and Eisegesis
audio: http://example.com/audio.mp3
youtube: vidId
---
`)
  })
})

describe('formatYoutubeDesc', () => {
  const DESC = `Sermon: A Biblical Argument Against Smores
Date: January\u00A01, 2020, Afternoon
Text: Job 6:6–7
Series: Opinions and Eisegesis
Preacher: Charles Spurgeon
Audio: http://example.com/audio.mp3`

  it('works for basic data', () => {
    expect(sermons.formatYoutubeDesc(VIDEO)).toEqual(DESC)
  })
})

describe('normalizeText', () => {
  it('works for normalized verse', () => {
    expect(sermons.normalizeText('Genesis 1:1')).toEqual('Genesis 1:1')
  })

  it('works for all three letter codes', () => {
    expect(sermons.normalizeText('Gen 1:1')).toEqual('Genesis 1:1')
    expect(sermons.normalizeText('Exo 1:1')).toEqual('Exodus 1:1')
    expect(sermons.normalizeText('Lev 1:1')).toEqual('Leviticus 1:1')
    expect(sermons.normalizeText('Num 1:1')).toEqual('Numbers 1:1')
    expect(sermons.normalizeText('Deu 1:1')).toEqual('Deuteronomy 1:1')
    expect(sermons.normalizeText('Jos 1:1')).toEqual('Joshua 1:1')
    expect(sermons.normalizeText('Jdg 1:1')).toEqual('Judges 1:1')
    expect(sermons.normalizeText('Rut 1:1')).toEqual('Ruth 1:1')
    expect(sermons.normalizeText('1Sa 1:1')).toEqual('1 Samuel 1:1')
    expect(sermons.normalizeText('2Sa 1:1')).toEqual('2 Samuel 1:1')
    expect(sermons.normalizeText('1Ki 1:1')).toEqual('1 Kings 1:1')
    expect(sermons.normalizeText('2Ki 1:1')).toEqual('2 Kings 1:1')
    expect(sermons.normalizeText('1Ch 1:1')).toEqual('1 Chronicles 1:1')
    expect(sermons.normalizeText('2Ch 1:1')).toEqual('2 Chronicles 1:1')
    expect(sermons.normalizeText('Ezr 1:1')).toEqual('Ezra 1:1')
    expect(sermons.normalizeText('Neh 1:1')).toEqual('Nehemiah 1:1')
    expect(sermons.normalizeText('Est 1:1')).toEqual('Esther 1:1')
    expect(sermons.normalizeText('Job 1:1')).toEqual('Job 1:1')
    expect(sermons.normalizeText('Psa 1:1')).toEqual('Psalm 1:1')
    expect(sermons.normalizeText('Pro 1:1')).toEqual('Proverbs 1:1')
    expect(sermons.normalizeText('Ecc 1:1')).toEqual('Ecclesiastes 1:1')
    expect(sermons.normalizeText('Sos 1:1')).toEqual('Song of Solomon 1:1')
    expect(sermons.normalizeText('Isa 1:1')).toEqual('Isaiah 1:1')
    expect(sermons.normalizeText('Jer 1:1')).toEqual('Jeremiah 1:1')
    expect(sermons.normalizeText('Lam 1:1')).toEqual('Lamentations 1:1')
    expect(sermons.normalizeText('Eze 1:1')).toEqual('Ezekiel 1:1')
    expect(sermons.normalizeText('Dan 1:1')).toEqual('Daniel 1:1')
    expect(sermons.normalizeText('Hos 1:1')).toEqual('Hosea 1:1')
    expect(sermons.normalizeText('Joe 1:1')).toEqual('Joel 1:1')
    expect(sermons.normalizeText('Amo 1:1')).toEqual('Amos 1:1')
    expect(sermons.normalizeText('Oba 1:1')).toEqual('Obadiah 1')
    expect(sermons.normalizeText('Jon 1:1')).toEqual('Jonah 1:1')
    expect(sermons.normalizeText('Mic 1:1')).toEqual('Micah 1:1')
    expect(sermons.normalizeText('Nah 1:1')).toEqual('Nahum 1:1')
    expect(sermons.normalizeText('Hab 1:1')).toEqual('Habakkuk 1:1')
    expect(sermons.normalizeText('Zep 1:1')).toEqual('Zephaniah 1:1')
    expect(sermons.normalizeText('Hag 1:1')).toEqual('Haggai 1:1')
    expect(sermons.normalizeText('Zec 1:1')).toEqual('Zechariah 1:1')
    expect(sermons.normalizeText('Mal 1:1')).toEqual('Malachi 1:1')
    expect(sermons.normalizeText('Mat 1:1')).toEqual('Matthew 1:1')
    expect(sermons.normalizeText('Mar 1:1')).toEqual('Mark 1:1')
    expect(sermons.normalizeText('Luk 1:1')).toEqual('Luke 1:1')
    expect(sermons.normalizeText('Joh 1:1')).toEqual('John 1:1')
    expect(sermons.normalizeText('Act 1:1')).toEqual('Acts 1:1')
    expect(sermons.normalizeText('Rom 1:1')).toEqual('Romans 1:1')
    expect(sermons.normalizeText('1Co 1:1')).toEqual('1 Corinthians 1:1')
    expect(sermons.normalizeText('2Co 1:1')).toEqual('2 Corinthians 1:1')
    expect(sermons.normalizeText('Gal 1:1')).toEqual('Galatians 1:1')
    expect(sermons.normalizeText('Eph 1:1')).toEqual('Ephesians 1:1')
    expect(sermons.normalizeText('Php 1:1')).toEqual('Philippians 1:1')
    expect(sermons.normalizeText('Col 1:1')).toEqual('Colossians 1:1')
    expect(sermons.normalizeText('1Th 1:1')).toEqual('1 Thessalonians 1:1')
    expect(sermons.normalizeText('2Th 1:1')).toEqual('2 Thessalonians 1:1')
    expect(sermons.normalizeText('1Ti 1:1')).toEqual('1 Timothy 1:1')
    expect(sermons.normalizeText('2Ti 1:1')).toEqual('2 Timothy 1:1')
    expect(sermons.normalizeText('Tit 1:1')).toEqual('Titus 1:1')
    expect(sermons.normalizeText('Phm 1:1')).toEqual('Philemon 1')
    expect(sermons.normalizeText('Heb 1:1')).toEqual('Hebrews 1:1')
    expect(sermons.normalizeText('Jam 1:1')).toEqual('James 1:1')
    expect(sermons.normalizeText('1Pe 1:1')).toEqual('1 Peter 1:1')
    expect(sermons.normalizeText('2Pe 1:1')).toEqual('2 Peter 1:1')
    expect(sermons.normalizeText('1Jo 1:1')).toEqual('1 John 1:1')
    expect(sermons.normalizeText('2Jo 1:1')).toEqual('2 John 1')
    expect(sermons.normalizeText('3Jo 1:1')).toEqual('3 John 1')
    expect(sermons.normalizeText('Jde 1:1')).toEqual('Jude 1')
    expect(sermons.normalizeText('Rev 1:1')).toEqual('Revelation 1:1')
  })

  it('works for multiple verses', () => {
    expect(sermons.normalizeText('Gen 1:1–2; Mat 1:1'))
      .toEqual('Genesis 1:1–2; Matthew 1:1')
  })
})

describe('normalizePreacher', () => {
  it('works for basic examples', () => {
    expect(sermons.normalizePreacher('Pastor Charles Spurgeon'))
      .toEqual('Charles Spurgeon')
    expect(sermons.normalizePreacher('Preacher Charles Spurgeon'))
      .toEqual('Charles Spurgeon')
    expect(sermons.normalizePreacher('Deacon Charles Spurgeon'))
      .toEqual('Charles Spurgeon')
    expect(sermons.normalizePreacher('Brother Charles Spurgeon'))
      .toEqual('Charles Spurgeon')
  })
})
