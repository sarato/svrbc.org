---
id: "2020-08-24"
date: 2020-08-24
title: Freedom to Worship in Santa Clara County
subtitle: ""
author: ""
occassion: >-
  An open letter to the Santa Clara County Board of Supervisors regarding
  recent Covid-19 related public health orders
image: ../banners/justice.jpg
bannerstyle: dark
---

Dear Members of the Board,

Thank you for your careful consideration of matters throughout this pandemic.
No doubt, this one of the most difficult seasons of leadership you have faced.
You have the unenviable task of making decisions where all options result in
many upset citizens in Santa Clara County.  We pray for your wisdom and your
well-being [1 Timothy 2:1-2]{.pref}.

Having acknowledged this, however, we must register our own objections to
recent county decisions handed down through public health mandates.  Much of
this debate has been framed around relative risks, the importance of economic
growth, etc.  We are not medical experts, and furthermore, we do not wish to
enter the cacophony of pragmatic judgments by levying our own.  Our concerns
are a matter of principle.

We hold these truths to be self-evident, that all men are created equal, that
they are endowed by their Creator with certain unalienable Rights.  We do not
believe this because it comes from the pen of Thomas Jefferson or the writings
of the founding fathers, but because it comes from the word of God, which
confirms the same [Genesis 1:26; Proverbs 29:7]{.pref}.  Among these, the
rights of free movement, peaceable assembly, and religious worship have always
been recognized.

Perhaps you consider yourself wiser than the generation that founded this
country.  Perhaps you believe that as a society we have progressed beyond such
antiquated notions of a Creator, free to promote equality and rights on firmer
foundations.  Yet what firmer foundations are there?  Apart from humanity being
created in a shared image of God, what grounds do we have for promoting
equality?  Apart from rights being given by God, what grounds do we have for
preserving rights?  These things would be a communal fiction, malleable, formed
by the whims of each successive generation.  And yet, we live in a time when
people recognize that the evils of the past were not justified by the presence
of societal consensus.  Rights must be grounded in the unchanging Almighty, or
they are no rights at all.  The founding fathers knew that built on anything
other than the bedrock of our Creator, liberty will only erode.

Quicker than any other region in the nation, you were willing to suspend these
rights that were not yours to suspend.  Quicker than any other region in the
nation, you were willing to engage in the partiality of deciding “essential”
activities and “non-essential” activities, which freedoms would remain and
which would be taken away.  Rather than merely estimating health risks, you
have made value judgments, permitting that which seems sufficiently valuable
and forbidding that which seems insufficiently valuable.  You permit abortion
clinics to run, not because they are relatively safe, but because you cherish
the murder of the unborn.  You forbid gathered singing—even outdoors, with
masks, for as few as two people—not because it is more dangerous than all other
permitted activities, but because you depreciate the worship of God.  On what
basis do you make these value judgments?  Do you lay claim to a perspective
higher than those who would disagree?

We call you to repent from these acts of injustice.  Continue to make public
health guidelines; we value them and have followed them as much as we are able.
Shut down activities that inherently promote death; this should have been done
even before the pandemic.  But do not employ the force of law to restrict
unalienable rights.  Some have praised you for these acts of petty tyranny, but
the God who made you does not, and to him you will one day give an account
[Matthew 12:36]{.pref}.  Steeped in our own erring sensibilities, we can only
stand condemned, but there is great forgiveness in Christ
[Colossians 1:13-14]{.pref}, who holds all the treasures of wisdom
[Colossians 2:3]{.pref}.

On behalf of Silicon Valley Reformed Baptist Church,

_Conley Owens_<br>
_Josh Sheldon_
