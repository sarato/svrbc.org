---
id: "2020-12-21"
date: 2020-12-21
title: Is Church Membership Biblical?
author: Conley Owens
image: ../banners/keys.jpg
bannerstyle: dark
---

The short answer is, “Yes!”

However, if you are in the mood to read a longer answer, here are some biblical
reasons to affirm the concept of local church membership.

## Membership Tallies in Acts {#tallies}

Where better to turn for information on what the church is than the book that
describes its founding?  Even from the day of Pentecost, the church in
Jerusalem was keeping track of how many were added to their number.

> So those who received his word were baptized, and there were added that day
> about three thousand souls. … And the Lord added to their number day by day
> those who were being saved.
>
> [Acts 2:41,47b (ESV)]{.citation}

One who rejects church membership might be inclined to regard this merely as
the number of people baptized or the number of people identifying as Christian.
However, later in the same book, we see that these are people who belong to
particular churches, not just the church at large.

> So the churches were strengthened in the faith, and they increased in numbers
> daily.
>
> [Acts 16:5 (ESV)]{.citation}

Some consider it too much to suggest that the early church would actually keep
formal records of such things.  Of course, a formal record is not necessary for
membership to exist, but it’s worth noting that they were in the habit of
maintaining rolls for other purposes, such as keeping a list of needy widows [1
Timothy 5:9]{.pref}

## The Analogy of the Body {#body}

The Bible describes the church as the body of Christ, and Christians as members of that body.

> For as in one body we have many members, and the members do not all have the
> same function, so we, though many, are one body in Christ, and individually
> members one of another.
>
> [Romans 12:4–5 (ESV)]{.citation}

<!-- -->

> Now you are the body of Christ and individually members of it.
>
> [1 Corinthians 12:27 (ESV)]{.citation}

One could point out that this is just talking about the universal church,
composed of all Christians regardless of which assembly they attend.  However,
both of these passages in their larger contexts address how local churches
ought to operate.  In other words, participation in Christ’s body happens
through membership in a local church.

One day, all the members of Christ’s universal body will be gathered together,
but at the moment we find our togetherness primarily in local assemblies.

## The Responsibility of Pastors to Laymen {#pastors}

The duty God assigns to pastors makes no practical sense  apart from church membership.

> Pay careful attention to yourselves and to all the flock, in which the Holy
> Spirit has made you overseers, to care for the church of God, which he
> obtained with his own blood.
>
> [Acts 20:28 (ESV)]{.citation}

<!-- -->

> shepherd the flock of God that is among you, exercising oversight, not under
> compulsion, but willingly, as God would have you; not for shameful gain, but
> eagerly;
>
> [1 Peter 5:2 (ESV)]{.citation}

Who, precisely, is a pastor supposed to pastor?  He cannot pastor every
Christian on the planet since he does not have the capacity to.  He cannot
pastor all who show up in a church on a given Sunday because the work of truly
caring for someone requires a level of relationship that must extend beyond a
mere day.  If he is to “exercise oversight,” he must have some means and
authority to do so.  For anyone who is not committed to a particular church,
such an oversight would not only be difficult to conduct, but intrusive.

Even the notion of a “flock of God that is among you” and “the flock, in which
the Holy Spirit has made you overseers” refers to a particular congregation
with particular borders.  Whatever those borders are, they define membership.

## The Responsibility of Laymen to Pastors {#laymen}

God instructs Christians to submit to elders.  Once again, one cannot
meaningfully fulfill this duty apart from membership in a local church.

> Obey your leaders and submit to them, for they are keeping watch over your
> souls, as those who will have to give an account.  Let them do this with joy
> and not with groaning, for that would be of no advantage to you.
>
> [Hebrews 13:17 (ESV)]{.citation}

A Christian cannot, nor is required to, heed every pastor.  No one has the time
for such an endeavor, and the instruction of different pastors will vary,
stripping  the command of any real  meaning.  Additionally, if a Christian has
the ability to immediately, at will, change which pastor is his pastor, then no
real obligation can be found in this verse.  For example, if I don’t like what
my pastor says, I could just decide that today he will not be my pastor.

The author of Hebrews clearly assumes that he addresses people who have entered
into a committed relationship to each other by and through the local church.

## The Responsibility of Christians to Each Other {#others}

The “one another”s in the New Testament have little shape outside of the
commitment of church membership.

> Bear one another’s burdens, and so fulfill the law of Christ.
>
> [Galatians 6:2 (ESV)]{.citation}

<!-- -->

> Let the word of Christ dwell in you richly, teaching and admonishing one
> another in all wisdom, singing psalms and hymns and spiritual songs, with
> thankfulness in your hearts to God.
>
> [Colossians 3:16 (ESV)]{.citation}

<!-- -->

> Therefore encourage one another and build one another up, just as you are
> doing.
>
> [1 Thessalonians 5:11 (ESV)]{.citation}

Once again, a Christian cannot bear the burdens of all Christians, encourage
all Christians, or sing to all Christians, etc.  He could theoretically do
these things with all Christians he encounters, but this would not match the
kind of intimate fellowship we see in Scripture.  Instead, there has to be some
particular group one regularly engages with in these activities, and with some
level of commitment.  While people may disagree about that level of commitment,
there is no reason not to call it what it is: membership.

## The Lord’s Supper {#lordssupper}

The Lord’s Supper is, in part, a statement about our unity with each other.

> Because there is one bread, we who are many are one body, for we all partake
> of the one bread.
>
> [1 Corinthians 10:17 (ESV)]{.citation}

If this communion is a formal recognition of others as believers, then there
must be some formal qualification.  Of course, it’s commonplace for many
churches who do not practice membership (and even many who do) to invite all
who claim to have faith to partake of the Lord’s Supper.  But what is to keep
anyone from making that claim if no one is responsible for judging that their
life does not accord with the gospel?

The Apostle Paul states plainly that there is a distinction between those
inside the church and those outside the church, and that this
“insideness”—i.e., membership—determines whether one should be admitted to the
table.

> But now I am writing to you not to associate with anyone who bears the name
> of brother if he is guilty of sexual immorality or greed, or is an idolater,
> reviler, drunkard, or swindler—not even to eat with such a one.  For what
> have I to do with judging outsiders? Is it not those inside the church whom
> you are to judge?
>
> [1 Corinthians 5:11–12 (ESV)]{.citation}

## Church Discipline {#discipline}

On a similar note, one of the marks of a true church is discipline, the
practice of handling a fellow disciple who is unrepentant (cf. [Matthew
18:15–22]{.ref}).  Many recognize this as a duty of the church, but are unclear
on what discipline actually is.  Regardless of what additional practices a
church has, excommunication is simply a formal statement that one does not
belong to a particular church.

> I wrote to you in my letter not to associate with sexually immoral people—
> not at all meaning the sexually immoral of this world, or the greedy and
> swindlers, or idolaters, since then you would need to go out of the world.
> But now I am writing to you not to associate with anyone who bears the name
> of brother if he is guilty of sexual immorality or greed, or is an idolater,
> reviler, drunkard, or swindler—not even to eat with such a one.  For what
> have I to do with judging outsiders? Is it not those inside the church whom
> you are to judge? God  judges those outside. “Purge the evil person from
> among you.”
>
> [1 Corinthians 15:9–13 (ESV)]{.citation}

If some is removed from the church, it means that formerly they belonged.
Regardless of what level of formality associated this belonging, it is
essentially the concept of membership.  If a church has no members—i.e., no one
“inside the church”—whom may it excommunicate?

## Disciplinary Judgments {#judgments}

In fact, the decision-making process of discipline itself requires church membership because it requires there be a known set of people who belong who have the authority to decide the matter.  Some imagine that church discipline may be sufficiently executed by the pastors/elders deciding whether to excommunicate someone.  On the contrary, the Bible requires that the whole church participate.

> If your brother sins against you, go and tell him his fault, between you and
> him alone. If he listens to you, you have gained your brother. But if he does
> not listen, take one or two others along with you, that every charge may be
> established by the evidence of two or three witnesses. If he refuses to
> listen to them, tell it to the church.
>
> [Matthew 18:15–17a (ESV)]{.citation}

In Matthew 18, Jesus depicts widening circles addressing the unrepentant man.
First, one person.  Second, one or two others, totaling to two or three
witnesses (cf. [Deuteronomy 17:6; Matthew 18:20]{.ref}).  Third, the whole
church.  The whole church here is a larger group, not a different group of one
pastor or two or three elders.

Paul also speaks of the whole church gathering to make such judgments.

> When you are assembled in the name of the Lord Jesus and my spirit is
> present, with the power of our Lord Jesus, you are to deliver this man to
> Satan for the destruction of the flesh, so that his spirit may be saved in
> the day of the Lord.
>
> [1 Corinthians 5:4–5 (ESV)]{.citation}

Note that 1 Corinthians along with most of Paul’s epistles were addressed to
whole churches rather than just to the pastors or elders.  This includes
Galatians, where the church is instructed to pronounce a judgment against a
false gospel [Galatians 1:8–9]{.pref}.  How could the church do such a thing if
it is uncertain who particularly belongs to the church?

To make any of these judgments, there must be a vote, and it must be clear who is permitted to vote.  In fact Paul speaks of such a vote when he encourages the church in Corinth to restore the man excommunicated in 1 Corinthians 5.

> For such a one, this punishment by the majority is enough,
>
> [2 Corinthians 6:2 (ESV)]{.citation}

What is the majority here?  It is not a majority that complied with the
decision against a minority who didn’t; Paul would not tolerate such disunity.
Rather, it is a majority that decided—i.e., voted—that the man would be
excommunicated.

Such decisions to add or remove a person from the church are weighty matters.
It would not be orderly for the decision to be made simply by whoever decided
to show up that day regardless of who they are.  These things need to be
weighed by those who are taught by the Spirit and have the mind of Christ.
They must be decided by those who have been formally recognized by the assembly
to have such status.

## Officer Selection {#officers}

Finally, the New Testament design of church officer selection can’t exist
without membership.  Specifically, the Bible depicts the church as a whole
deciding matters of church office, and it would be impossible to make such
judgments if it were unknown who specifically belongs to the church and has
authority to contribute their own vote.

We see one example of this in Acts 6, where the apostles have the church—i.e.,
the members of the church—pick for them men who will administer the resources
owned by the church.  These are essentially the first deacons of the church.

> And the twelve summoned the full number of the disciples and said, “It is not
> right that we should give up preaching the word of God to serve tables.
> Therefore, brothers, pick out from among you seven men of good repute, full
> of the Spirit and of wisdom, whom we will appoint to this duty.
>
> [Acts 6:2–3 (ESV)]{.citation}

Identifying leaders is the collective responsibility of the individual persons
of a local congregation.  Apart from church membership, it would not be clear
who should have a say in this.

## Common objections {#objections}

Beyond the matter of whether membership is biblical, there are several common
objections to membership.

**I can serve God sufficiently without membership**:  As stated above, you
cannot submit to elders or care for one another as Christ has called without
having some particular commitment to a local fellowship.  Furthermore, if a
matter of discipline came up, would you be able to vote as the New Testament
prescribes?  If the answer is no, then by your own rejection of membership, you
are not able to serve God as he has called.

**Church membership enables abuse**: It’s true that the commitments of church
membership have been used to emotionally manipulate members and foster
cult-like environments.  However, the misuse of a thing does not make it wrong.
If Christ has commanded membership, then people should join a church regardless
of whatever fears they have.  Good churches should have mechanisms in place to
protect members from such abuses, and you can always ask about such things
before joining.

**I’m not sure I’ve found the right church**: There is no perfect church, and
the Bible nowhere commands you to join the best church you can.  In fact,
depending on how God has gifted you, if you really want to look out for the
needs of others,  perhaps you should even consider joining a church that has
even more imperfections than the church down the street.  If you are attending
a church and not sure whether to join it, speak with a pastor and tell him
about your situation.  He will most likely be able to offer other church
recommendations nearby and guide you through the process.  Spending time in
indecision is spending time in disobedience.

**I’m not sure how long I’ll be living in this area**: Church membership is
still a command regardless of how transient you are.  Once again, if you are
attending a church, speak with a pastor and see what recommendations he has.
Many churches provide a transfer process that allows someone to remain a member
while in the process of moving.

**I’m not sure I can live up to the commitments of church membership**: No one
is perfect.  Each church is simply a society of forgiven people on the path of
sanctification.  If you have put your faith in Christ and are ready to fight
this spiritual battle against sin and temptation, you will fit right in,
regardless of how far along you are in your Christian walk.

## Conclusion {#conclusion}

Church membership in the times of the New Testament may not have looked exactly like it often does today, yet it could not have been far off.  To follow the instruction of Christ in Scripture, we must have a clear understanding of who belongs to a given local church and who does not.  The commitment of membership is a weighty one, but it is also a rewarding one.

> For if they fall, one will lift up his fellow. But woe to him who is alone
> when he falls and has not another to lift him up!
>
> [Ecclesiastes 4:10 (ESV)]{.citation}
