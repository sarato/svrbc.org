import Timer from '~/lib/timer'

export default {
  methods: {
    async $pageAll (path, data, start = 1, startsize = 0) {
      // If we don't even have as many as we can at the start, there's no point
      // in trying to grab more.
      if (data.length < startsize) return
      const timer = new Timer()
      while (true) {
        const extra = start === 1 ? '' : `${start}/`
        let res
        try {
          res = await this.$fetch(`${path}/${extra}`)
        } catch (e) {
          console.error('pager could not finish', e)
          break
        }
        let obj = res.data
        while (obj.obj) obj = obj.obj
        const page = obj.page
        data.push(...page.edges.map(e => e.node))
        if (page.pageInfo.currentPage === page.pageInfo.totalPages) break
        start++
      }
      if (this.debug) timer.pretty(`paging ${path}`)
    }
  }
}
