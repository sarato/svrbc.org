export default {
  computed: {
    $window () {
      if (typeof window === 'undefined') {
        return {
          location: {
            href: this.$route.fullPath
          }
        }
      }
      return {
        location: {
          href: window.location.href
        }
      }
    }
  }
}
