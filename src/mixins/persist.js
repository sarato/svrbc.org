// This needs to update every time we make a backwards-incompatible change.
const storeVersion = 2

function restore (type, value) {
  switch (type) {
    case 'plain':
      break
    case Date:
      if (value === 'null') return null
      return new Date(value)
    default:
      throw new Error('unanticipated type')
  }
  return JSON.parse(value)
}

function store (type, value) {
  switch (type) {
    case 'plain':
      break
    case Date:
      if (value === null) return 'null'
      return value.toJSON()
    default:
      throw new Error('unanticipated type')
  }
  return JSON.stringify(value)
}

export default {
  methods: {
    persist (prefix, ...specs) {
      if (!window.localStorage || !prefix) return
      for (const spec of specs) {
        let name = spec
        let type = 'plain'
        let objVersion = 1
        if (Array.isArray(spec)) {
          name = spec[0]
          type = spec[1]
          if (spec.length > 2) objVersion = spec[2]
        }
        const key = `v${storeVersion}-${prefix}-${name}-v${objVersion}`

        // Restore old value.
        const value = localStorage.getItem(key)
        if (value !== null) {
          try {
            this[name] = restore(type, value)
          } catch (e) {
            console.error(`could not restore ${name} with value ${value}`)
            console.error(e)
            localStorage.setItem(key, store(type, this[name]))
          }
        }

        // Store new values.
        this.$watch(name, v => {
          localStorage.setItem(key, store(type, v))
        })
      }
    }
  }
}
