import FormInputHelp from '~/components/FormInputHelp.vue'

export default {
  components: {
    FormInputHelp
  },
  props: {
    prefix: {
      type: String,
      default () {
        return ''
      }
    },
    template: {
      type: Object,
      default () {
        return {}
      }
    },
    value: {
      type: [Object, String, Date],
      default () {
        return {}
      }
    }
  },
  computed: {
    id () {
      return `${this.prefix}-${this.template.key}`
    },
    name () {
      return this.template.key
    },
    model: {
      get () {
        return this.value
      },
      set (v) {
        this.$emit('input', v)
      }
    }
  }
}
