# Banners

Banners are images that appear at the top of pages.

Guidelines:

*   Should be 1920x1080
*   Should be <500kb
*   Should not be too noisy, alternating between light and dark (i.e., it should
    be easy to read text on them)

For managing the images:

*   Do not delete an image unless we plan to never use it again
*   Give the banners unique filenames
*   Avoid updating banners (storage-wise it is expensive to do this in git)

Helpful places to look for images:

*   pixabay.com (note, license is not fully permissive)
*   europeana.eu
*   si.edu/search
*   search.creativecommons.org
*   negativespace.co
*   publicdomainpictures.net
