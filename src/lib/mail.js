import functions from './functions'

export default async (body) => {
  return functions.post('mail', body, {
    recaptchaAction: 'mail'
  })
}
