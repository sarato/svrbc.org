import { parseDate } from 'chrono-node'
import { DateTime } from 'luxon'

const HOUR = 60 * 60 * 1000

function fromISO (s) {
  return DateTime.fromISO(s, { zone: 'utc' }).toJSDate()
}

function fromLAISO (s) {
  // Remove the timezone so we can overwrite it.
  const iso = s.replace(/[Zz]$/, '')
  return DateTime.fromISO(iso, {
    zone: 'America/Los_Angeles'
  }).toJSDate()
}

function toLADate (d) {
  return DateTime.fromJSDate(d, {
    zone: 'America/Los_Angeles'
  })
}

// https://moment.github.io/luxon/docs/manual/formatting.html#table-of-tokens
const yearFmt = 'yyyy'
const dateIdFmt = 'yyyy-LL-dd'
const dateFullFmt = 'LLLL d, yyyy' // first space is nbsp
const longMonthDayFmt = 'LLLL d' // space is nbsp
const longMonthWeekdayFmt = 'cccc, LLLL d' // second space is nbsp
const longWeekdayFmt = 'cccc'
const shortMonthWeekdayFmt = 'ccc, LLL d' // second space is nbsp

function readLADate (s, opts) {
  opts = Object.assign({
    ref: null,
    future: false
  }, opts)

  // Unfortunately, casual times disregard specified timezones, so we need
  // to set them explicitly.
  if (!s.replace) {
    throw new Error(`s should be a string, is ${typeof s}: ${s}`)
  }
  s = s.replace(/\bmorning\b/ig, '11:00AM')
  s = s.replace(/\bnoon\b/ig, '12:00PM')
  s = s.replace(/\bafternoon\b/ig, '2:00PM')
  s = s.replace(/\b(evening|night)\b/ig, '7:00PM')
  s = s.replace(/\bmidnight\b/ig, '12:00AM')

  const utcDate = parseDate(`${s} UTC`, opts.ref, {
    forwardDate: opts.future
  })
  // Remove the timezone so we can overwrite it.
  return fromLAISO(utcDate.toISOString())
}

function readFutureLADate (s, ref = null) {
  return readLADate(`${s} midnight`, {
    ref,
    future: true
  })
}

// https://moment.github.io/luxon/docs/manual/formatting.html#table-of-tokens
function formatLADate (d, fmt) {
  return DateTime.fromJSDate(d, {
    zone: 'America/Los_Angeles'
  })
    .toFormat(fmt)
}

// NOTE: This does not handle DST correctly.
function addWeeks (d, w) {
  return DateTime.fromJSDate(d)
    .plus({ weeks: w })
    .toJSDate()
}

// NOTE: This does not handle DST correctly.
function addMonths (d, m) {
  return DateTime.fromJSDate(d)
    .plus({ months: m })
    .toJSDate()
}

// NOTE: This does not handle DST correctly.
function daysFromNow (d, testopts) {
  testopts = testopts || {}
  const then = DateTime.fromJSDate(d)
  const now = DateTime.fromJSDate(testopts.now || new Date())
  return parseInt(then.diff(now, 'days').values.days)
}

// NOTE: This does not handle DST correctly.
function minutesFromNow (d, testopts) {
  testopts = testopts || {}
  const then = DateTime.fromJSDate(d)
  const now = DateTime.fromJSDate(testopts.now || new Date())
  return parseInt(then.diff(now, 'minutes').values.minutes)
}

// NOTE: This does not handle DST correctly.
function hoursFromNow (d, testopts) {
  // The diff function is very unreliable for hours for some reason.
  testopts = testopts || {}
  const now = testopts.now || new Date()
  const diff = d.getTime() - now.getTime()
  // We have to use Math.floor because something like 2.7777777777777776e-7
  // goes through parseInt as 2.
  const f = diff > 0 ? Math.floor : Math.ceil
  return parseInt(f(diff / HOUR))
}

function hoursSince (d, testopts) {
  const h = hoursFromNow(d, testopts)
  if (h) return -h
  return h // Avoid signed 0
}

// NOTE: This does not handle DST correctly.
function weeksFromNow (d) {
  return parseInt(DateTime.fromJSDate(d).diffNow('weeks').values.weeks)
}

// NOTE: This does not handle DST correctly.
function weeksToNow (d) {
  return -1 * weeksFromNow(d)
}

export {
  addMonths,
  addWeeks,
  daysFromNow,
  dateFullFmt,
  dateIdFmt,
  formatLADate,
  fromISO,
  fromLAISO,
  hoursFromNow,
  hoursSince,
  longMonthDayFmt,
  longMonthWeekdayFmt,
  longWeekdayFmt,
  minutesFromNow,
  readFutureLADate,
  readLADate,
  shortMonthWeekdayFmt,
  toLADate,
  weeksFromNow,
  weeksToNow,
  yearFmt
}
