import bible from './bible'
import functions from './functions'

export default {
  query: async (verses, typ, opt) => functions.get('esv', {
    q: bible.reformat(verses, bible.SHORT)
  }, {
    recaptchaAction: `verses_${typ}`
  }),
  validate: (verses) => {
    bible.reformat(verses, bible.SHORT)
  }
}
