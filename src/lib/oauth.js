import functions from './functions'

const BASE_URL = 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fyoutube&response_type=code&client_id=96942811577-4l30equ3in7bno3a7a36p4d32h77npvk.apps.googleusercontent.com'

function getAuthURL (origin) {
  const url = getCallbackURL(origin)
  return `${BASE_URL}&redirect_uri=${url}`
}

function getCallbackURL (origin) {
  const path = functions.getPath('youtubeoauth')
  return `${origin}${path}`
}

export default {
  getAuthURL,
  getCallbackURL
}
