/* global page */
import { describe as jestDescribe } from 'jest-without-globals'
import config from '../../jest-puppeteer.config'

import Timer from './timer'

const PORT = config.server ? config.server.port : '0000'
const HOST = `localhost:${PORT}`
const URL_PREFIX = `http://${HOST}`

const describe = process.env.SVRBC_TEST === 'true'
  ? jestDescribe
  : jestDescribe.skip

async function captureErrs (f) {
  const errs = []
  const pageErrs = []
  const console = []
  function pushErr (e) {
    errs.push(e)
  }
  function pushPageErr (e) {
    pageErrs.push(e)
  }
  function pushConsole (l) {
    console.push(l)
  }
  page.on('console', pushConsole)
  page.on('error', pushErr)
  page.on('pageerror', pushPageErr)
  const res = await f()
  page.removeListener('error', pushErr)
  page.removeListener('pageerror', pushPageErr)
  return {
    errs,
    pageErrs,
    res
  }
}

async function visit (path, f) {
  const timer = new Timer()
  const { errs, pageErrs, res } = await captureErrs(async () => {
    const resp = page.goto(URL_PREFIX + path, { waitUntil: 'networkidle2' })
    if (f) await f()
    return resp
  })
  res.errs = errs
  res.pageErrs = pageErrs
  timer.pretty(`Loading ${path}`)
  return res
}

export {
  URL_PREFIX,
  captureErrs,
  describe,
  visit
}
