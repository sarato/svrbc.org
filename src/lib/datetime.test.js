/* eslint-env jest */
import { Settings } from 'luxon'

import {
  formatLADate,
  fromISO,
  fromLAISO,
  hoursSince,
  readFutureLADate,
  readLADate
} from './datetime'

describe('fromLAISO', () => {
  afterEach(() => {
    // It's not clear whether mocking the defaultZoneName is sufficient for our
    // test, but using timezone-mock causes luxon to produce "invalid date"
    // erros.
    Settings.defaultZoneName = Intl.DateTimeFormat().resolvedOptions().timeZone
  })

  it('new years works', () => {
    const d = fromLAISO('2020-01-01T01:00:00.000Z')
    expect(d.toISOString()).toEqual('2020-01-01T09:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2020-01-01 01:00 PST')
  })

  it('new years works on east coast', () => {
    Settings.defaultZoneName = 'America/New_York'
    const d = fromLAISO('2020-01-01T01:00:00.000Z')
    expect(d.toISOString()).toEqual('2020-01-01T09:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2020-01-01 01:00 PST')
  })
})

describe('hoursSince', () => {
  it('two hours works', () => {
    const d = fromISO('2020-01-01T00:00:00.000Z')
    const now = fromISO('2020-01-01T02:00:00.000Z')
    expect(hoursSince(d, { now })).toEqual(2)
  })

  it('one millisecond past works', () => {
    const d = fromISO('2020-01-01T00:00:00.000Z')
    const now = fromISO('2020-01-01T00:00:00.001Z')
    expect(hoursSince(d, { now })).toEqual(0)
  })

  it('one millisecond future works', () => {
    const d = fromISO('2020-01-01T00:00:00.001Z')
    const now = fromISO('2020-01-01T00:00:00.000Z')
    expect(hoursSince(d, { now })).toEqual(0)
  })
})

describe('readLADate', () => {
  afterEach(() => {
    // It's not clear whether mocking the defaultZoneName is sufficient for our
    // test, but using timezone-mock causes luxon to produce "invalid date"
    // erros.
    Settings.defaultZoneName = Intl.DateTimeFormat().resolvedOptions().timeZone
  })

  it('new years eve works', () => {
    const d = readLADate('2019-12-31 10:00 PM')
    expect(d.toISOString()).toEqual('2020-01-01T06:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2019-12-31 22:00 PST')
  })

  it('new years eve works on east coast', () => {
    Settings.defaultZoneName = 'America/New_York'
    const d = readLADate('2019-12-31 10:00 PM')
    expect(d.toISOString()).toEqual('2020-01-01T06:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2019-12-31 22:00 PST')
  })

  it('dst works', () => {
    const d = readLADate('2020-07-02 2:00')
    expect(d.toISOString()).toEqual('2020-07-02T09:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2020-07-02 02:00 PDT')
  })

  it('morning is reasonable', () => {
    const d = readLADate('January 1, 2020, Morning')
    expect(d.toISOString()).toEqual('2020-01-01T19:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2020-01-01 11:00 PST')
  })

  it('afternoon is reasonable', () => {
    const d = readLADate('January 1, 2020, Afternoon')
    expect(d.toISOString()).toEqual('2020-01-01T22:00:00.000Z')
    expect(formatLADate(d, 'yyyy-LL-dd T ZZZZ'))
      .toEqual('2020-01-01 14:00 PST')
  })
})

describe('readFutureLADate', () => {
  it('thursday works', () => {
    const y2k = fromISO('2000-01-01T08:00:00.000Z')
    const d = readFutureLADate('thursday', y2k)
    expect(d.toISOString()).toEqual('2000-01-06T08:00:00.000Z')
  })
})
