function routeKey (route) {
  if (route.matched.length) return route.matched[0].path
  return route.fullPath
}

export {
  routeKey
}
