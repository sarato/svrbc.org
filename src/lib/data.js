import {
  dateFullFmt,
  dateIdFmt,
  formatLADate,
  longMonthDayFmt,
  readLADate
} from './datetime'

function cache (obj, name, f) {
  if (!(name in obj)) obj[name] = f()
  return obj[name]
}

class Sermon {
  constructor (data) {
    Object.assign(this, data)
  }

  get preacherName () {
    return cache(this, '_preacherName',
      () => this.preacher ? this.preacher.title : this.unregisteredPreacher)
  }

  get realDate () {
    return cache(this, '_realDate', () => readLADate(this.date))
  }

  get dateId () {
    return cache(this, '_dateId',
      () => formatLADate(this.realDate, dateIdFmt))
  }

  get monthDay () {
    return cache(this, '_monthDay',
      () => formatLADate(this.realDate, longMonthDayFmt))
  }

  get fullDate () {
    return cache(this, '_fullDate',
      () => formatLADate(this.realDate, dateFullFmt))
  }

  get seriesCount () {
    return cache(this, '_seriesCount', () => {
      if (!this.series) return 0
      if (!this.series.sermons) return 0
      return this.series.sermons.edges.length
    })
  }
}

class Preacher {
  constructor (data) {
    Object.assign(this, data, {
      sermons: data.sermons
        ? data.sermons.edges.map(e => new Sermon(e.node))
        : []
    })
  }
}

export {
  Preacher,
  Sermon
}
