class Timer {
  constructor () {
    this.creation = Date.now()
    this.running = false
    this.endHandle = null
  }

  runFor (duration) {
    this.running = true
    clearTimeout(this.endHandle)
    this.endHandle = setTimeout(() => (this.running = false), duration)
  }

  stop () {
    clearTimeout(this.endHandle)
    this.running = false
  }

  sinceCreated () {
    return Date.now() - this.creation
  }

  pretty (str) {
    console.log(`${str} took ${this.sinceCreated() / 1000}s`)
  }
}

export default Timer
