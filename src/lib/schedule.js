import {
  addWeeks,
  fromLAISO,
  formatLADate,
  longMonthDayFmt,
  weeksToNow,
  yearFmt
} from './datetime'

// This is actually a week before we started, but since we went through the
// wrong version, we ended up a week off.
const CATECHISM_START = fromLAISO('2018-12-30T13:00:00')

function readSchedule (schedule) {
  return schedule.map(s => {
    // Workday lunch ends at 1:00 pm.
    const iso = s.date.replace('T00', 'T13')
    const date = fromLAISO(iso)
    return Object.assign({}, s, {
      date,
      dateStr: formatLADate(date, longMonthDayFmt)
    })
  })
}

function getYearTables (schedule, now) {
  const next = getNext(schedule, now)
  const nextYear = next
    ? next.date.getFullYear() + 1
    : parseInt(formatLADate(now, yearFmt)) + 1
  let curYear = {}
  const map = []
  schedule.forEach(s => {
    // We shoudn't include dates from any later than next year.
    if (s.date.getFullYear() > nextYear) return
    if (s.date.getFullYear() !== curYear.year) {
      // We shoudn't include more than two years.
      if (map.length === 2) map.shift()
      curYear = {
        year: s.date.getFullYear(),
        schedule: []
      }
      map.push(curYear)
    }
    curYear.schedule.push(s)
  })
  return map
}

function getNext (schedule, now) {
  now = (now || new Date()).getTime()
  return schedule
    .filter(s => now < s.date.getTime())
    .filter(s => !s.canceled)[0] || null
}

function getNextCatechismDate () {
  const weeks = weeksToNow(CATECHISM_START) + 1
  return addWeeks(CATECHISM_START, weeks)
}

function getNextCatechismQuestion () {
  const weeks = weeksToNow(CATECHISM_START)
  return (weeks % 114) + 1
}

function filterRecentSermons (data) {
  const ss = data.edges.map(e => e.node)
  const ret = [ss[0]]
  if (ss[0].date === ss[1].date) {
    ret.push(ss[1])
  }
  return ret
}

// Sunday between 6:00am and 5:00pm.
function sundayInSession (d) {
  const s = formatLADate(d, 'cHH')
  return s >= '706' && s <= '717'
}

// Sunday between 10:45 and 11:30
// This is used to get information before we get a response from YouTube.
function sermonSoon (d) {
  const s = formatLADate(d, 'cHHmm')
  return s >= '71045' && s <= '71130'
}

export {
  filterRecentSermons,
  getNext,
  getNextCatechismDate,
  getNextCatechismQuestion,
  getYearTables,
  readSchedule,
  sermonSoon,
  sundayInSession
}
