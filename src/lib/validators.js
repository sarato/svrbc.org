function notFuture (value) {
  return value < Date.now()
}

export {
  notFuture
}

export default {
  notFuture
}
