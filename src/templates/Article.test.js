/* eslint-env jest, browser */
/* global page */

import { describe, visit } from '../lib/pageutils'

describe('/articles/* redirect', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/articles/2020-08-24')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  it('open letter exists', async () => {
    await expect(page).toMatch('open letter')
  })
})

describe('/articles/*/* redirect', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/articles/2020-08-24/other')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  it('open letter exists', async () => {
    await expect(page).toMatch('open letter')
  })
})
