# Forms

These are complete forms (not partial form components).  They may be reused,
but are frequently for a single page.

In part, this directory exist due to the way we do validation.  At most one
form may be included directly in a page without confusing invalidations, so
it’s necessary to write some forms in their own vue component.

## Mail templates

If you are creating a new MailForm, you probably want to put a new mail
template in `lib/mail-templates/` and register it in
`lib/mail-templates/index.js`.
