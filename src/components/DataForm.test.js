/* eslint-env jest, browser */
/* global page */
import { describe, visit } from '../lib/pageutils'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('Prooftext', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/test/data-form')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  describe('component', () => {
    let component

    beforeAll(async () => {
      component = await expect(page).toMatchElement('form')
    })

    it('required fields are required', async () => {
      await expect(component).not.toMatch('required')
      await expect(page).toFillForm('form', {
        name: 'No One',
        email: 'noone@example.com'
      })
      await expect(component).toClick('input[type=submit]')
      await expect(component).toMatch('required')
      await expect(page).toFillForm('form', {
        f1: 'test'
      })
      await expect(component).toClick('input[type=submit]')
      await expect(component).not.toMatch('required')
      await expect(component).toMatch('Thank you!', {
        timeout: 10000
      })
    }, 15000)
  })
})
