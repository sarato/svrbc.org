/* eslint-env jest, browser */
/* global page */
import { describe, visit } from '../lib/pageutils'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('Prooftext', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/test/single-prooftext')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  describe('component', () => {
    let component

    beforeAll(async () => {
      component = await expect(page).toMatchElement('.prooftext')
    })

    it('verses fetched', async () => {
      await expect(page).toClick('.prooftext')
      const el = await expect(component).toMatchElement('.passage', {
        timeout: 10000
      })
      await expect(el).toMatch('God helps those who help themselves')
    }, 15000)
  })
})
