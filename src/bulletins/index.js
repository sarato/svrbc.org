module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'bulletins/*.yml',
    typeName: 'Bulletin'
  }
}
