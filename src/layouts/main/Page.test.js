/* eslint-env jest, browser */
/* global page */

import { visit, describe } from '../../lib/pageutils'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('Main layout', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/test/multiple-headings?debug')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  it('height constant', async () => {
    const debug = await page.$eval('.main-debug',
      m => JSON.parse(m.dataset.obj))
    const time = debug.heightStability.time
    expect(time, `Height was adjusted at ${time}ms.  ` +
      'Page should be stable on load.').toBeUndefined()
  })
})
