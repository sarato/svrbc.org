/* eslint-env browser */
import VueAnalytics from 'vue-analytics'
import VueClipboard from 'vue-clipboard2'
import VueKonami from 'vue-konami'
import VueLoadScript from 'vue-plugin-load-script'
import TreeView from 'vue-json-tree-view'

import 'material-design-icons/iconfont/material-icons.css'
import 'tippy.js/dist/tippy.css'
import 'typeface-open-sans/index.css'
import 'typeface-palanquin/index.css'

import SpellIgnore from '~/components/SpellIgnore.vue'
import SubHeading from '~/components/SubHeading.vue'
import Default from '~/layouts/Default.vue'
import { routeKey } from '~/lib/routeutil'
import debug from '~/mixins/debug'

import '~/assets/main.scss'

export default function (Vue, { router }) {
  // Components
  Vue.component('Default', Default)
  Vue.component('Subheading', SubHeading)
  Vue.component('SpellIgnore', SpellIgnore)

  // Plugins
  Vue.use(VueAnalytics, {
    id: 'UA-144232579-1',
    router
  })
  Vue.use(VueClipboard)
  Vue.use(VueKonami)
  Vue.use(VueLoadScript)
  Vue.use(TreeView)
  Vue.mixin(debug)

  // Overwrite the router options.
  router.options.scrollBehavior = async (to, from, savedPosition) => {
    // If the route key and the hash have not not changed, don't scroll.
    // This keeps us from jumping around when we are sharing the same template
    // component between items (e.g., the catechism pages).
    if (routeKey(from) === routeKey(to) && from.hash === to.hash) return

    if (to.hash) {
      // If we are changing paths, we need to wait for the route to finish
      // changing.
      if (from.path !== to.path) {
        await new Promise(resolve => {
          // This event is emitted by App.vue.
          router.app.$el.addEventListener('routeEnter', resolve, {
            once: true
          })
        })
      }

      const elem = document.querySelector(to.hash)
      // vue-router does not incorporate scroll-margin-top on its own.
      if (elem) {
        const offset = parseFloat(getComputedStyle(elem).scrollMarginTop)
        return {
          selector: to.hash,
          offset: { y: offset }
        }
      }
    }
    if (savedPosition) return savedPosition
    return { x: 0, y: 0 }
  }
}
