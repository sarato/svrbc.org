/* eslint-env jest */
import gql from 'graphql-tag'

import graphql from '../../lib/graphql'
import { readLADate, readFutureLADate } from '../lib/datetime.js'

function isLADate (s) {
  return !!readLADate(s)
}

function isFutureLADate (s) {
  return !!readFutureLADate(s)
}

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('announcements', () => {
  let announcements, noscript

  beforeAll(async () => {
    const res = await graphql.query(gql`{
      allAnnouncement {
        edges {
          node {
            fileInfo {
              path
            }
            schedule {
              start
              end
              urgency
            }
            script
          }
        }
      }
    }`)
    announcements = res.data.allAnnouncement.edges.map(a => a.node)
    noscript = announcements.filter(a => !a.script)
  }, 30000)

  it('all have start', () => {
    for (const a of announcements) {
      for (const s of a.schedule) {
        expect(s.start).toBeDefined()
      }
    }
  })

  it('start is valid date', () => {
    for (const a of announcements) {
      for (const s of a.schedule) {
        expect(isLADate(s.start), `${s.start} is not a real date`).toBeTrue()
      }
    }
  })

  it('last has end', () => {
    for (const a of noscript) {
      const s = a.schedule[a.schedule.length - 1]
      expect(s.end, a.fileInfo.path).toBeDefined()
    }
  })

  it('end is valid date', () => {
    for (const a of announcements) {
      for (const s of a.schedule) {
        const msg = `${s.end} is not a real date`
        expect(!s.end || isLADate(s.end), msg).toBeTrue()
      }
    }
  })

  it('urgency is natural language date', () => {
    for (const a of announcements) {
      for (const s of a.schedule) {
        const msg = `${s.urgency} is not a real date`
        expect(!s.urgency || isFutureLADate(s.urgency), msg).toBeTrue()
      }
    }
  })
})
