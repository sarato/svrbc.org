module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'confession/chapters/*.yml',
    typeName: 'ConfessionChapter'
  }
}
