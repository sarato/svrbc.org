module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'confession/*.yml',
    typeName: 'ConfessionParagraph',
    refs: {
      chapter: 'ConfessionChapter',
      prev: 'ConfessionParagraph',
      next: 'ConfessionParagraph'
    }
  }
}
