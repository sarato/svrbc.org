import dataformat from '../../lib/dataformat'

// Add verse check
export default {
  type: 'ConfessionParagraph',
  query: `node {
  prev {
    id
  }
  next {
    id
  }
  chapter {
    id
  }
  fileInfo {
    path
  }
  id
  paragraph
  chapter_title
  segments {
    text
    verses
  }
}`,
  nodeChecks: [
    dataformat.checkVerses('segments.verses')
  ],
  normalizeContent: n => {
    return dataformat.formatYaml(Object.assign({}, n, {
      chapter: parseInt(n.chapter.id),
      prev: n.prev ? n.prev.id : null,
      next: n.next ? n.next.id : null
    }), [
      'id',
      'prev',
      'next',
      'chapter',
      'paragraph',
      'segments',
      'text',
      'verses'
    ], {
      noquote: ['verses']
    })
  }
}
