---
id: 2016-11-13-pm
title: Beatitudes Part 18
date: 2016-11-13
text: Matthew 6:2–18
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161113+Afternoon+Service+-+Beatitudes+Part+18.mp3
audioBytes: 34429556
youtube: yrUrbR3IMvA
---
