---
id: 2023-01-29-pm
title: The Restoration of Jacob
date: 2023-01-29
text: Isaiah 29:22–24
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230129-TheRestorationOfJacob.aac
audioBytes: 25878182
youtube: E2FJUNvw7dU
---
