---
id: 2008-09-07-am
title: The Compassion of The Lord
date: 2008-09-07
text: 2 Samuel 7:1–17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-09-07+The+Compassion+of+The+Lord.mp3
audioBytes: 42408954
---
