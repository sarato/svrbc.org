---
id: 2022-12-25-am
title: 'The Gospel of Luke: Hope for the World'
date: 2022-12-25
text: Luke 1:26–38
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221225-TheGospelOfLuke-HopeForTheWorld.aac
audioBytes: 53703516
youtube: Q9XBlBsupbM
---
