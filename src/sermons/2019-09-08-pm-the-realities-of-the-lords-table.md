---
id: 2019-09-08-pm
title: The Realities Of The Lord’s Table
date: 2019-09-08
text: 1 Corinthians 11:27–34
preacher: josh-sheldon
series: the-lords-table
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190908-PM-TheRealitiesOfTheLordsTable.mp3
audioBytes: 35570171
---
