---
id: 2023-04-30-am
title: Overcoming Evil Like Jesus
date: 2023-04-30
text: Luke 4:1–13
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230430-OvercomingEvilLikeJesus.aac
audioBytes: 47980630
youtube: WgXVkUAUcx0
---
