---
id: 2015-12-13-am
title: The Word Of God Continues
date: 2015-12-13
text: 2 Kings 2:1–18
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151213+The+Word+of+God+Continues.mp3
audioBytes: 47604402
youtube: 5yHlbPf0Tr4
---
