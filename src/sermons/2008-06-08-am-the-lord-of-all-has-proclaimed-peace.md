---
id: 2008-06-08-am
title: The Lord Of All Has Proclaimed Peace
date: 2008-06-08
text: Acts 10:34–48
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-06-08+The+Lord+Of+All+Has+Proclaimed+Peace.mp3
audioBytes: 54767583
---
