---
id: 2019-02-03-pm
title: Authority Of Pastors
date: 2019-02-03
text: Hebrews 13:17
preacher: josh-sheldon
series: authority-in-the-church
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190203-PM-AuthorityOfPastors.mp3
audioBytes: 42265442
youtube: vOylSsskoRI
---
