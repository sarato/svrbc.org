---
id: 2014-04-27-am
title: Our Sovereign God
date: 2014-04-27
text: John 13:18–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-04-27+Our+Sovereign+God.mp3
audioBytes: 45870471
---
