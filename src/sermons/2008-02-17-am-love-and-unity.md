---
id: 2008-02-17-am
title: Love and Unity
date: 2008-02-17
text: 1 Corinthians 13
preacher: mike-mccullough
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-02-17+Love+and+Unity.mp3
audioBytes: 41984031
---
