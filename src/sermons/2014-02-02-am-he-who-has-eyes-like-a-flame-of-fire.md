---
id: 2014-02-02-am
title: He Who Has Eyes Like a Flame of Fire
date: 2014-02-02
text: Revelation 2:18
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-02-02+Who+Has+Eyes+Like+a+Flame+of+Fire.mp3
audioBytes: 53748435
---
