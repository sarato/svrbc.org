---
id: 2014-03-30-am
title: How Christ Would Have Us
date: 2014-03-30
text: John 13:6–11
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-03-30+How+Christ+Would+Have+Us.mp3
audioBytes: 46727406
---
