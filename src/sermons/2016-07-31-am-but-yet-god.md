---
id: 2016-07-31-am
title: 'But Yet, God!'
date: 2016-07-31
text: Psalm 62
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160731+But+Yet+God.mp3
audioBytes: 46646427
youtube: RiW_W0ePP-w
---
