---
id: 2015-02-22-am
title: The Church’s Commission
date: 2015-02-22
text: John 20:19–23
preacher: josh-sheldon
series: the-passion-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150222+The+Churchs+Commission.mp3
audioBytes: 47413810
---
