---
id: 2013-06-23-am
title: 'Be Prepared, the Bridegroom Comes!'
date: 2013-06-23
text: Matthew 25:1–13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-06-23+Be+Prepared+The+Bridegroom+Comes.mp3
audioBytes: 48245703
---
