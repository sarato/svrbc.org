---
id: 2013-03-24-am
title: In Jesus’ Name
date: 2013-03-24
text: Colossians 3:17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-03-24+In+Jesus%27+Name.mp3
audioBytes: 52567074
---
