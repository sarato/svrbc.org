---
id: 2016-09-11-pm
title: Beatitudes Part 10
date: 2016-09-11
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160911+Beatitudes+Part+10.mp3
audioBytes: 22238034
youtube: bFm2BV8lm50
---
