---
id: 2017-03-05-pm
title: God our Righteous Judge
date: 2017-03-05
text: Psalm 7
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170305+Afternoon+Service-God+our+Righteous+Judge.mp3
audioBytes: 24844470
youtube: Blvbl6HVj8o
---
