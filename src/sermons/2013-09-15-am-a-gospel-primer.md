---
id: 2013-09-15-am
title: A Gospel Primer
date: 2013-09-15
text: 2 Corinthians 5:21
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-09-15+A+Gospel+Primer.mp3
audioBytes: 35495928
---
