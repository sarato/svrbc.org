---
id: 2017-01-08-am
title: God’s Righteousness in His Impartiality
date: 2017-01-08
text: Romans 2:12–16
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170108+Gods+Righteousness+in+His+Impartiality.mp3
audioBytes: 50376737
---
