---
id: 2021-12-05-am
title: The Power of Good Examples
date: 2021-12-05
text: 2 Thessalonians 3:6–12
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211205-ThePowerofGoodExamples.aac
audioBytes: 35278997
youtube: 9vRR7TSFVks
---
