---
id: 2021-03-14-am
title: The Power of God’s Word Confirmed
date: 2021-03-14
text: 1 Thessalonians 2:14–16
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210314-ThePowerOfGod'sWordConfirmed.mp3
audioBytes: 68477431
youtube: EAIhLqGN_Rg
---
