---
id: 2021-08-01-am
title: To Quench the Spirit
date: 2021-08-01
text: 1 Thessalonians 5:19–22
preacher: conley-owens
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210801-ToQuenchTheSpirit.aac
audioBytes: 48590873
youtube: YEvGxhPyH5Q
---
