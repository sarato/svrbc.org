---
id: 2008-03-02-am
title: Not Enough The First Time
date: 2008-03-02
text: Acts 8:14–17
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-03-02+Not+Enough+The+First+Time.mp3
audioBytes: 48615999
---
