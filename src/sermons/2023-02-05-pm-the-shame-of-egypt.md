---
id: 2023-02-05-pm
title: The Shame Of Egypt
date: 2023-02-05
text: Isaiah 30:1–5
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230129-TheShameOfEgypt.aac
audioBytes: 26448589
youtube: AY0K8cyvPVA
---
