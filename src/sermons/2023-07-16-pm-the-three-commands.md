---
id: 2023-07-16-pm
title: The Three Commands
date: 2023-07-16
text: Micah 6:1–8
preacher: josh-sheldon
unregisteredSeries: Micah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230716-TheThreeCommands.aac
audioBytes: 33895497
youtube: BAzlw3ZWXDY
---
