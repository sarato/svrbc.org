---
id: 2017-07-09-pm
title: Back To Eden
date: 2017-07-09
text: 1 Timothy 2:13–15
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170709+PM+Service+-+Back+To+Eden.mp3
audioBytes: 28646672
youtube: Jh84k8ivM1Q
---
