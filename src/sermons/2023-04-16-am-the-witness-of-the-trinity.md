---
id: 2023-04-16-am
title: The Witness of the Trinity
date: 2023-04-16
text: Luke 3:18–21
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230416-TheWitnessOfTheTrinity.aac
audioBytes: 47896305
youtube: Y5y73q7-ATk
---
