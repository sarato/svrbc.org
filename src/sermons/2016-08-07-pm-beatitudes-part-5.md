---
id: 2016-08-07-pm
title: Beatitudes Part 5
date: 2016-08-07
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160807+Beatitudes+Part+5.mp3
audioBytes: 35590157
youtube: _NHNg9dXRjw
---
