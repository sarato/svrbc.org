---
id: 2022-01-16-am
title: No Foundation But Jesus Christ
date: 2022-01-16
text: 1 Corinthians 3:10–15
preacher: josh-sheldon
series: maturity-in-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220116-NoFoundationButJesusChrist.aac
audioBytes: 59144815
youtube: j66KhU1Zp1U
---
