---
id: 2022-01-16-pm
title: 'The Oracle of Dumah: The Weight of Revelation'
date: 2022-01-16
text: Isaiah 21:11–12
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220116-TheOracleOfDumah-TheWeightOfRevelation.aac
audioBytes: 24710298
youtube: 6uF1wEFtXMc
---
