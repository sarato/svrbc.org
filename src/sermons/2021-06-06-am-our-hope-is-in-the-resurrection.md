---
id: 2021-06-06-am
title: Our Hope is in the Resurrection
date: 2021-06-06
text: 1 Thessalonians 4:13–18
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210606-Our%20HopeIsInTheResurrection.aac
audioBytes: 46573038
youtube: UYIM4d_zKc0
---
