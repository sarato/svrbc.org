---
id: 2011-09-18-am
title: Jesus is Better
date: 2011-09-18
text: John 3:22–36
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-09-18+Jesus+is+Better.mp3
audioBytes: 30868062
---
