---
id: 2020-06-28-am
title: God’s People - Rejoicing and Reasonable
date: 2020-06-28
text: Philippians 4:4–7
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200628-GodsPeople-RejoicingAndReasonable.mp3
audioBytes: 71324987
youtube: 32aITp0tnTg
---
