---
id: 2022-05-08-pm
title: Plans of Old
date: 2022-05-08
text: Isaiah 25:1
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220508-PlansOfOld.aac
audioBytes: 22579225
youtube: joe6DO9xLR8
---
