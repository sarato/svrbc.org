---
id: 2017-01-29-pm
title: God Hears Our Groaning
date: 2017-01-29
text: Psalm 5
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170129+Afternoon+Service-God+Heres+Our+Groaning.mp3
audioBytes: 23939171
youtube: uVTQ86qdacs
---
