---
id: 2017-08-27-am
title: Our Sovereign And Merciful God
date: 2017-08-27
text: Romans 9:14–18
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170827-AM-OurSovereignAndMercifulGod.mp3
audioBytes: 52798443
youtube: P2Lmu17S3SY
---
