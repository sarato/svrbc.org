---
id: 2017-02-19-pm
title: Waiting on God
date: 2017-02-19
text: Psalm 7
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170219+Afternoon+Service+-+Waiting+on+God.mp3
audioBytes: 22663140
youtube: pehyAC47U68
---
