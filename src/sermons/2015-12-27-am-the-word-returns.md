---
id: 2015-12-27-am
title: The Word Returns
date: 2015-12-27
text: 2 Kings 2:14–25
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151227+The+Word+Returns.mp3
audioBytes: 47813790
youtube: ApsCxO5ekjM
---
