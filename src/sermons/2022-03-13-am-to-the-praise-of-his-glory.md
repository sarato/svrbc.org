---
id: 2022-03-13-am
title: To the Praise of His Glory
date: 2022-03-13
text: Ephesians 1:11–14
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220313-TothePraiseofHisGlory.aac
audioBytes: 59978483
youtube: BrJs3i-rdeU
---
