---
id: 2016-12-18-am
title: Songs Of Praise
date: 2016-12-18
text: Luke 1:39–56
preacher: josh-sheldon
series: advent-2016
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/161218+Songs+Of+Praise.mp3
audioBytes: 36238046
youtube: FmHveA9N91E
---
