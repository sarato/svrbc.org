---
id: 2013-01-20-am
title: The Shrewd Steward
date: 2013-01-20
text: Luke 16:1–13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-01-20+The+Shrewd+Steward.mp3
audioBytes: 100566276
---
