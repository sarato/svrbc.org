---
id: 2021-12-19-pm
title: The Folly of Man’s Wisdom
date: 2021-12-19
text: Isaiah 19:1
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211219-TheFollyOfMansWisdom.aac
audioBytes: 22251344
youtube: ktlIjnUNLxM
---
