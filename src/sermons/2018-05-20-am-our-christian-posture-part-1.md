---
id: 2018-05-20-am
title: Our Christian Posture Part 1
date: 2018-05-20
text: Ephesians 1:15–23
preacher: larry-trummel
series: our-christian-posture
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180520-AM-OurChristianPosture.mp3
audioBytes: 47415980
youtube: 2uEBRNbxGpU
---
