---
id: 2021-05-30-am
title: Real Religion for Real People in Real Life
date: 2021-05-30
text: 1 Thessalonians 4:11–12
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210530-RealReligionForRealPeopleInRealLife.aac
audioBytes: 56343997
youtube: uIUTG1qyQOk
---
