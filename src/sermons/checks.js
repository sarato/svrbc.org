import dataformat from '../../lib/dataformat'
import sermons from '../../lib/sermons'

function uniqueYoutubeIds (nodes, warn) {
  const ids = {}
  for (const n of nodes) {
    if (n.youtube in ids) {
      warn(`duplicate youtube id ${n.youtube}`, n)
    }
  }
}

function seriesRecorded (node, warn) {
  if (node.series && node.unregisteredSeries) {
    warn('series and unregisteredSeries should not both be defined')
  }
  if (node.series && node.misc) {
    warn('series and misc should no both be defined')
  }
  if (node.unregisteredSeries && node.misc) {
    warn('unregisteredSeries and misc should no both be defined')
  }
}

async function audioBytesRecorded (node, warn, fix, offline) {
  if (!offline) return
  if (node.audio && !node.audioBytes) {
    if (fix) {
      await sermons.populateAudioBytes(node)
    }
    if (!node.audioBytes) {
      warn(node, 'audioBytes should be recorded')
    }
  }
}

async function noExtraneousAudioBytes (node, warn, fix) {
  if (!node.audio && node.audioBytes !== null) {
    if (fix) {
      node.audioBytes = null
    } else {
      warn('audioBytes should not be recorded')
    }
  }
}

async function preacherExists (node, warn, fix, offline) {
  if (!offline) return
  if (!node.preacher) warn('known preacher should be recorded')
  if (node.unregisteredPreacher && node.unregisteredPreacher !== 'dummy') {
    warn('unregistered preacher')
  }
}

async function seriesExists (node, warn, fix, offline) {
  if (!offline) return
  if (node.unregisteredSeries) warn('unregistered series')
}

export default {
  type: 'Sermon',
  subdir: 'sermons',
  query: `node {
  audio
  audioBytes
  content
  date
  fileInfo {
    extension
    name
    path
  }
  id
  misc
  preacher {
    id
    title
  }
  series {
    id
    title
  }
  title
  text
  unregisteredPreacher
  unregisteredSeries
  youtube
}`,
  normalizeFilename: sermons.formatFilename,
  normalizeContent: (node, opts) =>
    sermons.formatMd(node, opts.data, opts.content),
  collectionChecks: [
    uniqueYoutubeIds
  ],
  nodeChecks: [
    seriesRecorded,
    noExtraneousAudioBytes,
    audioBytesRecorded,
    preacherExists,
    seriesExists,
    dataformat.checkSpelling('title', { offline: true }),
    dataformat.checkSpelling('text', { offline: true }),
    dataformat.checkSpelling('content', { html: true, offline: true }),
    dataformat.checkVerses('text')
  ],
  spellFields: ['title', 'content', 'text'],
  htmlFields: ['content'],
  verseRefsFields: ['text']
}
