---
id: 2017-03-19-am
title: The Death of Sin
date: 2017-03-19
text: Romans 6:1–14
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170319+The+Death+of+Sin+by+Pastor+Josh+Sheldon.mp3
audioBytes: 48736728
youtube: G5gYNji_iXE
---
