---
id: 2018-12-30-pm
title: Outside The Camp Part 1
date: 2018-12-30
text: Hebrews 13:11–13
preacher: josh-sheldon
series: outside-the-camp
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181230-PM-OutsideTheCamp.mp3
audioBytes: 29590414
youtube: 0GqaapqGjBE
---
