---
id: 2021-09-26-pm
title: The Lord Our Shepherd
date: 2021-09-26
text: Psalm 23
preacher: josh-sheldon
unregisteredSeries: The Lord Our Shepherd
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210926-TheLordOurShepherd.aac
audioBytes: 36648985
youtube: ojguZ3HhkPM
---
