---
id: 2022-03-27-am
title: The Authority of Christ
date: 2022-03-27
text: Ephesians 1:19–22
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220327-TheAuthorityOfChrist.aac
audioBytes: 60545602
youtube: duGpxNQ5oZA
---
