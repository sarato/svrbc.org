---
id: 2021-05-24-am
title: Living in an Unstable World for an Unshakable Kingdom
date: 2021-05-24
text: Hebrews 12:18–29
unregisteredPreacher: 'Royce Ruiz, Pastor at Hillview Baptist Church'
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210523-LivingInAnUnstableWorldForAnUnshakableKingdom.aac
audioBytes: 49078337
youtube: MDEYVSJQr2c
---
