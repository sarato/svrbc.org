---
id: 2012-12-16-am
title: The Promises of God
date: 2012-12-16
text: John 11:17–44
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-12-16+The+Promises+of+God.mp3
audioBytes: 56149104
---
