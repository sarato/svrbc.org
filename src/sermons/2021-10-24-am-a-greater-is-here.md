---
id: 2021-10-24-am
title: A Greater is Here
date: 2021-10-24
text: Chronicles 30:22
preacher: josh-sheldon
unregisteredSeries: A Greater is Here
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211024-AGreaterIsHere.aac
audioBytes: 48135883
youtube: 2fDxK1KDMFk
---
