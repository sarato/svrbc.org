---
id: 2014-06-01-am
title: The Victorious Church
date: 2014-06-01
text: Revelation 3:7–13
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-06-01+Church+in+Philadelphia.mp3
audioBytes: 64890258
---
