---
id: 2012-10-07-am
title: 'Grace and Peace from God, Part 2'
date: 2012-10-07
text: Revelation 1:4
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-10-07+Grace+and+Peace+from+God+Part2.mp3
audioBytes: 47818695
---
