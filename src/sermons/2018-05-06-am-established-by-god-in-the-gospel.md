---
id: 2018-05-06-am
title: Established By God In The Gospel
date: 2018-05-06
text: Romans 16:21–27
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180506-AM-EstablishedByGodInTheGospel.mp3
audioBytes: 41531936
youtube: h4d34IGhLCM
---
