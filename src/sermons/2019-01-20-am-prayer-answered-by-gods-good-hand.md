---
id: 2019-01-20-am
title: Prayer Answered (by God’s Good Hand)
date: 2019-01-20
text: Nehemiah 2:4–5, 7–8
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190120-AM-PrayerAnsweredbyGodsGoodHand.mp3
audioBytes: 44734355
youtube: OF-3tFLcgXg
---
