---
id: 2022-03-20-am
title: 'Eyes of your Heart Enlightened - Ephesians 1:15-18'
date: 2022-03-20
text: Ephesians 1:15–18
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220320-EyesofyourHeartEnlightened.aac
audioBytes: 61618112
youtube: '-_a176Fb8Hw'
---
