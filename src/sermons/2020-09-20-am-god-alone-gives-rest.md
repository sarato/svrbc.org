---
id: 2020-09-20-am
title: God Alone Gives Rest
date: 2020-09-20
text: Ruth 3
preacher: josh-sheldon
series: ruth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200920-GodAloneGivesRest.mp3
audioBytes: 73389497
youtube: 04gZok3xq2o
---
