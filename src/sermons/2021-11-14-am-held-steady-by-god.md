---
id: 2021-11-14-am
title: Held Steady by God
date: 2021-11-14
text: 2 Thessalonians 2:16–17
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211114-HeldSteadyByGod.aac
audioBytes: 53113349
youtube: FhGNpr-zzFY
---
