---
id: 2008-09-14-am
title: The Covenant With David
date: 2008-09-14
text: 2 Samuel 7:1–17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-09-14+The+Covenant+With+David.mp3
audioBytes: 59844975
---
