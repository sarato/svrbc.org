---
id: 2011-03-13-am
title: Responding With Grace to Sin
date: 2011-03-13
text: 1 Peter 3:1–8
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-03-13+Responding+With+Grace+to+Sin.mp3
audioBytes: 37756032
---
