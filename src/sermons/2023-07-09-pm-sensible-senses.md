---
id: 2023-07-09-pm
title: Sensible Senses
date: 2023-07-09
text: Isaiah 32:3–4
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230709-SensibleSenses.aac
audioBytes: 27319925
youtube: AN9oOi_xgkQ
---
