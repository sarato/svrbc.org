---
id: 2013-07-07-am
title: 'Heavenly vs. Earthly Wisdom, Part 1'
date: 2013-07-07
text: James 3:13–18
preacher: steve-mixsell
series: james
audio: https://storage.googleapis.com/pbc-ca-sermons/2013/2013-07-07+Heavenly+vs.mp3
audioBytes: 47596851
---
