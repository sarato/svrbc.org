---
id: 2013-01-06-am
title: 'Saved by Faith Alone, in God’s Word Alone'
date: 2013-01-06
text: James 2:14–26
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-01-06+Saved+by+Faith+Alone_In+God%27s+Word+Alone.mp3
audioBytes: 60932928
---
