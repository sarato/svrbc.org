---
id: 2015-02-15-am
title: The Meaning Of Resurrection
date: 2015-02-15
text: Romans 4:4, 16–25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150215+-+The+Meaning+of+the+Resurrection.mp3
audioBytes: 38334879
---
