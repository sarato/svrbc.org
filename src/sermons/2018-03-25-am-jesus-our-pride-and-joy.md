---
id: 2018-03-25-am
title: 'Jesus, Our Pride And Joy'
date: 2018-03-25
text: Romans 15:17–21
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180325-AM-JesusOurPrideAndJoy.mp3
audioBytes: 44858303
youtube: y59DAPKpFcU
---
