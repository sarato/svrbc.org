---
id: 2016-09-04-pm
title: Beatitudes Part 9
date: 2016-09-04
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160904+Beatitudes+Part+9.mp3
audioBytes: 38194043
youtube: pLbonYzawSg
---
