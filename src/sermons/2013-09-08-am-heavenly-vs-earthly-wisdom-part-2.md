---
id: 2013-09-08-am
title: 'Heavenly vs. Earthly Wisdom, Part 2'
date: 2013-09-08
text: James 3:13–18
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-09-08+Heavenly+vs+Earthly+Wisdom.mp3
audioBytes: 56555208
---
