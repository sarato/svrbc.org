---
id: 2021-12-19-am
title: The Lord With Us All
date: 2021-12-19
text: 2 Thessalonians 3:16–18
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211219-TheLordWithUsAll.aac
audioBytes: 33176152
youtube: 99SbQF7dtLA
---
