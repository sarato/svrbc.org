---
id: 2018-01-07-am
title: The Real Deal
date: 2018-01-07
text: Romans 12:11–19
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180107-AM-TheRealDeal.mp3
audioBytes: 47773245
youtube: 4V_JQKt8uEg
---
