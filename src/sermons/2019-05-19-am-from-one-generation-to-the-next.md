---
id: 2019-05-19-am
title: From One Generation To The Next
date: 2019-05-19
text: Nehemiah 9:9–21
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190519-AM-FromOneGenerationToTheNext.mp3
audioBytes: 50685245
---
