---
id: 2023-01-08-pm
title: So Near Yet So Far
date: 2023-01-08
text: 1 Corinthians 9:27–10:14
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230108-SoNearYetSoFar.aac
audioBytes: 30140996
youtube: 3lEbqz5XSvY
---
