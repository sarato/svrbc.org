---
id: 2007-09-30-am
title: True and Lasting Gain
date: 2007-09-30
text: 1 Timothy 6:6
preacher: michael-phillips
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-09-30+True+and+Lasting+Gain.mp3
audioBytes: 51998703
---
