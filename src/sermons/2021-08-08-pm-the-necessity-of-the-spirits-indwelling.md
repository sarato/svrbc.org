---
id: 2021-08-08-pm
title: The Necessity of the Spirit’s Indwelling
date: 2021-08-08
text: Luke 11:13
preacher: conley-owens
unregisteredSeries: The Necessity of the Spirit's Indwelling
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210808-TheNecessityOfTheSpiritsIndwelling.aac
audioBytes: 43036219
youtube: WnR8e4Vr22o
---
