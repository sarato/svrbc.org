---
id: 2009-05-03-am
title: He Is Willing
date: 2009-05-03
text: Mark 1:40–45
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-05-03+He+Is+Willing.mp3
audioBytes: 41861850
---
