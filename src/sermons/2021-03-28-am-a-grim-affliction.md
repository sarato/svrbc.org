---
id: 2021-03-28-am
title: A Grim Affliction
date: 2021-03-28
text: 1 Thessalonians 3:1–5
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210328-AGrimAffliction.mp3
audioBytes: 68021020
youtube: RWjorX2era8
---
