---
id: 2019-09-08-am
title: The Eternal Son
date: 2019-09-08
text: Hebrews 1:8–12
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190908-AM-TheEternalSon.mp3
audioBytes: 47892842
---
