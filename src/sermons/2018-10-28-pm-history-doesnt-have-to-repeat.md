---
id: 2018-10-28-pm
title: History Doesn’t Have To Repeat
date: 2018-10-28
text: Zechariah 1:1–6
preacher: josh-sheldon
series: zechariah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181028-PM-HistoryDoesntHaveToRepeat.mp3
audioBytes: 39392823
---
