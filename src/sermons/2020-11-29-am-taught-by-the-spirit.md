---
id: 2020-11-29-am
title: Taught by the Spirit
date: 2020-11-29
text: 1 Corinthians 2:10–13
preacher: conley-owens
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201129-TaughtByTheSpirit.mp3
audioBytes: 56113665
youtube: AEUvKVMgEU4
---
