---
id: 2022-12-18-am
title: 'The Gospel of Luke: The Allure of Unbelief'
date: 2022-12-18
text: Luke 1:18–25
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221218-TheGospelOfLuke-TheAllureOfUnbelief.aac
audioBytes: 43016844
youtube: lCYgfgTkfOE
---
