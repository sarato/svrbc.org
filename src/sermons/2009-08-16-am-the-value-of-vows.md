---
id: 2009-08-16-am
title: The Value of Vows
date: 2009-08-16
text: Acts 18:8
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-08-16+The+Value+of+Vows.mp3
audioBytes: 52814772
---
