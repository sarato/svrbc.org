---
id: 2018-10-21-am
title: The Worth And Work Of The Word
date: 2018-10-21
text: Psalm 19:7–11
preacher: chris-kiagiri
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181021-AM-TheWorthAndWorkOfTheWord.mp3
audioBytes: 50594119
---
