---
id: 2015-05-10-am
title: How to Pray Effectively
date: 2015-05-10
text: James 5:16–18
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150510+How+to+Pray+Effectively.mp3
audioBytes: 57687241
---
