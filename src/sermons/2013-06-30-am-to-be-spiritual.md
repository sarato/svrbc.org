---
id: 2013-06-30-am
title: To Be Spiritual
date: 2013-06-30
text: Romans 8:6
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-06-30+To+Be+Spiritual.mp3
audioBytes: 41071635
---
