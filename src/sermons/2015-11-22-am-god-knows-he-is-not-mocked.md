---
id: 2015-11-22-am
title: God Knows; He Is Not Mocked
date: 2015-11-22
text: 1 Kings 21
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151122+God+Knows+-+He+Is+Not+Mocked.mp3
audioBytes: 53417380
youtube: tJ1pmxcTGbQ
---
