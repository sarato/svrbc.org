---
id: 2015-10-11-am
title: What Just Happened?
date: 2015-10-11
text: 1 Kings 18:41–46
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151011+What+Just+Happened.mp3
audioBytes: 47918701
---
