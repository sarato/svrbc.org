---
id: 2018-12-23-am
title: The Hope Of The Advent
date: 2018-12-23
text: Titus 2:11–14
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181223-AM-TheHopeOfTheAdvent.mp3
audioBytes: 46485578
---
