---
id: 2019-06-02-am
title: 'Israel, Glorious At Last'
date: 2019-06-02
text: Nehemiah 9:22–37
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190602-AM-IsraelGloriousAtLast.mp3
audioBytes: 44783656
---
