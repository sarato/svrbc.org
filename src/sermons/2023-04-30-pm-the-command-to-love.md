---
id: 2023-04-30-pm
title: The Command To Love
date: 2023-04-30
text: 2 John 4–6
preacher: josh-sheldon
series: 2-john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230430-TheCommandToLove.aac
audioBytes: 27676612
youtube: XxwvNd-a27Y
---
