---
id: 2022-01-30-am
title: A Baptismal Message
date: 2022-01-30
text: 1 Peter 3:18–22
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220130-ABaptismalMessage.aac
audioBytes: 32246696
youtube: vGI3U_xmjuQ
---
