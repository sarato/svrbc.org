---
id: 2023-03-19-am
title: David’s Rise
date: 2023-03-19
text: 1 Samuel 18
preacher: josh-sheldon
series: the-life-of-david
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230319-DavidsRise.aac
audioBytes: 41150289
youtube: dbnPI4jz6IM
---
