---
id: 2020-10-04-am
title: Of First Importance - Our Unity
date: 2020-10-04
text: 1 Corinthians 1:1–9
preacher: josh-sheldon
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201004-OfFirstImportance-OurUnity.mp3
audioBytes: 65930806
youtube: KJi5jjOZDig
---
