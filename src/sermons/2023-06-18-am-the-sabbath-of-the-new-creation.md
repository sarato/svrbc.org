---
id: 2023-06-18-am
title: The Sabbath Of The New Creation
date: 2023-06-18
text: Luke 6:1–11
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230618-TheSabbathOfTheNewCreation.aac
audioBytes: 49580261
youtube: E0W7GyZnL3U
---
