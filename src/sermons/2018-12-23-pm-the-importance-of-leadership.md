---
id: 2018-12-23-pm
title: The Importance Of Leadership
date: 2018-12-23
text: Isaiah 3:1–15
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181223-PM-TheImportanceOfLeadership.mp3
audioBytes: 26073289
youtube: 455idLtKWGU
---
