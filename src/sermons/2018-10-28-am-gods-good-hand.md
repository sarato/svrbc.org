---
id: 2018-10-28-am
title: God’s Good Hand
date: 2018-10-28
text: Ezra 8
preacher: josh-sheldon
series: ezra-nehemiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/181028-AM-GodsGoodHand.mp3
audioBytes: 56448450
---
