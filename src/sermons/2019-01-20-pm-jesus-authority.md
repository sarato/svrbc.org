---
id: 2019-01-20-pm
title: Jesus’s Authority
date: 2019-01-20
text: Matthew 28:18
preacher: josh-sheldon
series: authority-in-the-church
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190120-PM-AuthorityInTheChurch-JesusAuthority.mp3
audioBytes: 29872556
youtube: abw8IhIsvIg
---
