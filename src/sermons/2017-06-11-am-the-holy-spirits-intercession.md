---
id: 2017-06-11-am
title: The Holy Spirit’s Intercession
date: 2017-06-11
text: Romans 8:26–27
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170611+The+Holy+Spirits+Intercession.mp3
audioBytes: 48806933
youtube: LxQj0iEBXJ4
---
