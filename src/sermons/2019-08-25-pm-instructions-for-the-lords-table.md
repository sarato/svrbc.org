---
id: 2019-08-25-pm
title: Instructions For The Lord’s Table
date: 2019-08-25
text: 1 Corinthians 11:17–22
preacher: josh-sheldon
series: the-lords-table
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190825-PM-InstructionsForTheLordsTable.mp3
audioBytes: 22736733
---
