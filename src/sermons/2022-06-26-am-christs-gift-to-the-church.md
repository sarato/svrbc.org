---
id: 2022-06-26-am
title: Christ’s Gift to the Church
date: 2022-06-26
text: Ephesians 4:11–13
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220626-ChristsGiftToTheChurch.aac
audioBytes: 31920399
youtube: DfHJzUnlbSs
---
