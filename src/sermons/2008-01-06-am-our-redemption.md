---
id: 2008-01-06-am
title: Our Redemption
date: 2008-01-06
text: Titus 2:11–14
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2008/2008-01-06+Joyful+Duty.mp3
audioBytes: 44510634
---
