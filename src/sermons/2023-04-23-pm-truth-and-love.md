---
id: 2023-04-23-pm
title: Truth and Love
date: 2023-04-23
text: 2 John 1–3
preacher: josh-sheldon
series: 2-john
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230423-TruthAndLove.aac
audioBytes: 28473434
youtube: VoEWak71zXc
---
