---
id: 2019-02-17-pm
title: The Cross He Bore – A Cup Refused
date: 2019-02-17
text: Mark 15:23
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190217-PM-TheCrossHeBore-ACupRefused.mp3
audioBytes: 24081717
---
