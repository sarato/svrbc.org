---
id: 2023-05-21-pm
title: The Hubris Of Horses
date: 2023-05-21
text: Isaiah 31:1–3
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230521-TheHubrisOfHorses.aac
audioBytes: 29382464
youtube: ukDy0N8_I4I
---
