---
id: 2023-07-16-am
title: Living out the Mercy of God
date: 2023-07-16
text: Luke 6:24–36
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230716-LivingOutTheMercyOfGod.aac
audioBytes: 46377313
youtube: s66F_w1ZEiU
---
