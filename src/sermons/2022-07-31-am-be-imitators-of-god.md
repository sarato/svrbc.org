---
id: 2022-07-31-am
title: Be Imitators of God
date: 2022-07-31
text: Ephesians 5:1–5
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220731-BeImitatorsOfGod.aac
audioBytes: 47257215
youtube: jEsC33o3Hrk
---
