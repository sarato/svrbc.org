---
id: 2012-02-26-am
title: A Place To Flee
date: 2012-02-26
text: Numbers 35:10–12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-02-26+A+Place+To+Flee.mp3
audioBytes: 46917558
---
