---
id: 2022-11-27-am
title: He Advocates for the Weak
date: 2022-11-27
text: 1 Samuel 30:21–25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221127-HeAdvocatesForTheWeak.aac
audioBytes: 45157778
youtube: N-165lKgREU
---
