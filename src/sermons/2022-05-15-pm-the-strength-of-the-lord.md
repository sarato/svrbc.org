---
id: 2022-05-15-pm
title: The Strength of the Lord
date: 2022-05-15
text: Isaiah 25:2–5
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220515-TheStrengthOfTheLord.aac
audioBytes: 17185853
youtube: a1e-DoXp6w8
---
