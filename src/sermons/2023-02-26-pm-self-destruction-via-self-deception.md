---
id: 2023-02-26-pm
title: Self-Destruction Via Self-Deception
date: 2023-02-26
text: Isaiah 30:8–11
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230226-SelfDestructionViaSelfDeception.aac
audioBytes: 34445370
youtube: 1FJMds81AG8
---

Self Destruction Via Self Deception
