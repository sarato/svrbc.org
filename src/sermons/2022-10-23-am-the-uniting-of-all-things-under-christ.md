---
id: 2022-10-23-am
title: The Uniting of All Things Under Christ
date: 2022-10-23
text: Ephesians 6:21–24
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221023-TheUnitingOfAllThingsUnderChrist.aac
audioBytes: 48043490
youtube: yz58HEH82_Y
---
