---
id: 2021-06-27-pm
title: 'The Oracle Against Babylon: A Signal of Judgment'
date: 2021-06-27
text: Isaiah 13
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210627-TheOracleAgainstBabylon-ASignalOfJudgment.aac
audioBytes: 41516191
youtube: RhgtWt5fYSY
---
