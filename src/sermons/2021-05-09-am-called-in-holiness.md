---
id: 2021-05-09-am
title: Called in Holiness
date: 2021-05-09
text: 1 Thessalonians 4:7–8
preacher: conley-owens
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210509-CalledInHoliness.aac
audioBytes: 44134070
youtube: fZgtW8UL1dE
---
