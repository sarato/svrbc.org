---
id: 2022-10-30-am
title: A Harvest from an Unlikely Place
date: 2022-10-30
text: John 4:7–42
preacher: chris-santiago
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221030-AHarvestfrommanUnlikelyPlace.aac
audioBytes: 55054039
youtube: Z5gzwRZsWd4
---
