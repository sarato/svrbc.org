---
id: 2011-11-27-am
title: A Tale of Two Faiths
date: 2011-11-27
text: John 4:39–54
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-11-27+A+Tale+of+Two+Faiths.mp3
audioBytes: 43045296
---
