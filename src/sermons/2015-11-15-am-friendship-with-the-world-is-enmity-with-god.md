---
id: 2015-11-15-am
title: Friendship With The World Is Enmity With God
date: 2015-11-15
text: 1 Kings 20
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151115+Friendship+With+The+World+Is+Enmity+With+God.mp3
audioBytes: 55710759
youtube: BUqq8887nbg
---
