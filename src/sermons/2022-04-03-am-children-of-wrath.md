---
id: 2022-04-03-am
title: Children of Wrath
date: 2022-04-03
text: Ephesians 2:1–3
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220403-ChildrenofWrath.aac
audioBytes: 54478919
youtube: i9J9auzubCE
---
