---
id: 2017-12-03-am
title: Renewed Minds Think Like This
date: 2017-12-03
text: Romans 12:3–16
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171203-AM-RenewedMindsThinkLikeThis.mp3
audioBytes: 46613482
youtube: eqOSczdZIOA
---
