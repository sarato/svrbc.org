---
id: 2019-07-07-am
title: Wide And Narrow Ways
date: 2019-07-07
text: John 2:13–22
preacher: jason-smathers
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190707-AM-Jesus-OurTemple.mp3
audioBytes: 46068888
---
