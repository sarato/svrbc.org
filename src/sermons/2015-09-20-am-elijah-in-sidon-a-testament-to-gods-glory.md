---
id: 2015-09-20-am
title: Elijah In Sidon – A Testament To God’s Glory
date: 2015-09-20
text: 1 Kings 17:8–16
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150920+Elijah+in+SIdon+-+A+testament+To+God+s+Glory.mp3
audioBytes: 58186758
youtube: EK9adPSEsZY
---
