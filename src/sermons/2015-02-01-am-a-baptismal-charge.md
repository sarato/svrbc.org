---
id: 2015-02-01-am
title: A Baptismal Charge
date: 2015-02-01
text: Matthew 28:19–20
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150201+A+Baptismal+Charge.mp3
audioBytes: 20369308
---
