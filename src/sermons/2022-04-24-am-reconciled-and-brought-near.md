---
id: 2022-04-24-am
title: Reconciled and Brought Near
date: 2022-04-24
text: Ephesians 2:11–16
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220424-ReconciledandBroughtNear.aac
audioBytes: 47134855
youtube: imEUPLvo2co
---
