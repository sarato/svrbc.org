---
id: 2017-03-12-pm
title: How God is Mindful of Man
date: 2017-03-12
text: Psalm 8
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/1760312+Aftrernoon+Service-How+God+is+Mindful+of+Man.mp3
audioBytes: 26316105
youtube: mxnqNO58Yho
---
