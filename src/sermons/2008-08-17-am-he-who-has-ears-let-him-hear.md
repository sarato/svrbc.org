---
id: 2008-08-17-am
title: 'He Who Has Ears, Let Him Hear'
date: 2008-08-17
text: John 8:31–47
preacher: james-torrefranca
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-08-17+He+Who+Have+Ears,+Let+Him+Hear.mp3
audioBytes: 39166362
---
