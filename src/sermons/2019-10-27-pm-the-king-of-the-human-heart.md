---
id: 2019-10-27-pm
title: The King Of The Human Heart
date: 2019-10-27
text: Exodus 20:3
preacher: joshua-camacho
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191027-PM-TheKingOfHumanHeart.mp3
audioBytes: 37397503
---
