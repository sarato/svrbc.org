---
id: 2007-04-29-ev
title: The Just Shall Live
date: 2007-04-29
text: Haggai 2:4
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-04-29+The+Just+Shall+Live.mp3
audioBytes: 40734282
---
