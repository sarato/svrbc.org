---
id: 2011-07-31-am
title: You Must Be Born Again
date: 2011-07-31
text: John 3:1–8
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-07-31+You+Must+Be+Born+Again.mp3
audioBytes: 40095360
---
