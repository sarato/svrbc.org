---
id: 2022-09-11-am
title: Our Enemy’s True Nature
date: 2022-09-11
text: 1 Samuel 17:31–37
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220911-OurEnemysTrueNature.aac
audioBytes: 44137360
youtube: szgz78dBVe4
---
