---
id: 2019-04-07-am
title: Israel Redux
date: 2019-04-07
text: Nehemiah 7
preacher: josh-sheldon
series: ezra-nehemiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/190407-AM-IsraelRedux.mp3
audioBytes: 52100844
---
