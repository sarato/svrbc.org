---
id: 2009-09-27-am
title: 'Forgiveness, the Mark of a Christian'
date: 2009-09-27
text: Philemon
preacher: henry-wiley
audio: https://storage.googleapis.com/pbc-ca-sermons/2009/2009-09-27+Henry+Wiley.mp3
audioBytes: 28858122
---
