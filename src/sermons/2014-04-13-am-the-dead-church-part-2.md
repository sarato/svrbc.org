---
id: 2014-04-13-am
title: 'The Dead Church, Part 2'
date: 2014-04-13
text: Revelation 3:1–6
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-04-13+The+Dead+Church+-+Rev+3v1-6.mp3
audioBytes: 62616774
---
