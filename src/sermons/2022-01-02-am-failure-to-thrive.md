---
id: 2022-01-02-am
title: Failure to Thrive
date: 2022-01-02
text: 1 Corinthians 3:1–4
preacher: josh-sheldon
series: maturity-in-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/2201002-FailureToThrive.aac
audioBytes: 27993987
youtube: yeQg7Y6yJNE
---
