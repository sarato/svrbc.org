---
id: 2014-08-31-am
title: The Last Hour Before
date: 2014-08-31
text: John 16:29–33
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140831+-+The+Last+Hour+Before+-+THAT+HOUR.mp3
audioBytes: 41906350
---
