---
id: 2023-03-05-pm
title: The Strength Of Stillness
date: 2023-03-05
text: Isaiah 30:15–18
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230305-TheStrengthOfStillness.aac
audioBytes: 36746411
youtube: jnulGxbL-MQ
---
