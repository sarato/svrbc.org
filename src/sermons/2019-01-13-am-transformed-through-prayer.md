---
id: 2019-01-13-am
title: Transformed Through Prayer
date: 2019-01-13
text: Nehemiah 2:1–8
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190113-AM-TransformedThroughPrayer.mp3
audioBytes: 47214505
youtube: 8iFlSfroGeE
---
