---
id: 2023-01-15-am
title: 'The Gospel of Luke: Light To Those In Darkness'
date: 2023-01-15
text: Luke 1:57–80
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230115-TheGospelOfLuke-LightToThoseInDarkness.aac
audioBytes: 54634013
youtube: plXUyHEg1xI
---
