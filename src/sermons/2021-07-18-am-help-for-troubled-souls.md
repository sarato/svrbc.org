---
id: 2021-07-18-am
title: Help for Troubled Souls
date: 2021-07-18
text: 1 Thessalonians 5:14–15
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210718-HelpForTroubledSouls.aac
audioBytes: 46214961
youtube: wAM438dtRG4
---
