---
id: 2022-05-22-pm
title: The Feast of the Lord
date: 2022-05-22
text: Isaiah 25:6
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220522-TheFeastOfTheLord.aac
audioBytes: 17369220
youtube: Us0XdeCSBsM
---
