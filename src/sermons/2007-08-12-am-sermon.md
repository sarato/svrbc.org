---
id: 2007-08-12-am
title: Sermon
date: 2007-08-12
text: Acts 5:1–11
preacher: josh-sheldon
series: acts
audio: https://storage.googleapis.com/pbc-ca-sermons/2007/2007-08-12+Sermon.mp3
audioBytes: 52890666
---
