---
id: 2011-10-16-am
title: The Course and Climax of Divine Revelation
date: 2011-10-16
text: Hebrews 1:2–3
preacher: marcus-howard
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-10-16+The+Course+and+Climax+of+Divine+Revelation.mp3
audioBytes: 38622594
---
