---
id: 2019-08-04-pm
title: Be Imitators Of God
date: 2019-08-04
text: Ephesians 4:30–5:2
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190804-PM-BeImitatorsOfGod.mp3
audioBytes: 33990281
---
