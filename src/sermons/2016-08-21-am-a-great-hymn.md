---
id: 2016-08-21-am
title: A Great Hymn
date: 2016-08-21
text: Philippians 2:5–11
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160821+A+Great+Hymn.mp3
audioBytes: 44318812
youtube: qWX4MRLX4bI
---
