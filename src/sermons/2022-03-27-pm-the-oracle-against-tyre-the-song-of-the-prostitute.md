---
id: 2022-03-27-pm
title: 'The Oracle Against Tyre: The Song of the Prostitute'
date: 2022-03-27
text: Isaiah 23:15–18
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220327-TheOracleAgainstTyre-TheSongOfTheProstitute.aac
audioBytes: 30684472
youtube: UXn6O95HuXE
---
