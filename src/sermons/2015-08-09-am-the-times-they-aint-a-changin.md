---
id: 2015-08-09-am
title: The Times They Ain’t a-Changin’
date: 2015-08-09
text: 1 Kings 16:29–17:16
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150809+The+Times+They+Aint+A-changin.mp3
audioBytes: 51475545
youtube: 8yf7zWAt104
---
