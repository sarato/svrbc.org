---
id: 2020-03-01-pm
title: The Fool
date: 2020-03-01
text: Psalm 14
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/200301-PM-TheFool.mp3
audioBytes: 38943481
youtube: goUwTjJkEg0
---
