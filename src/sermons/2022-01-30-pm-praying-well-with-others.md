---
id: 2022-01-30-pm
title: Praying Well With Others
date: 2022-01-30
text: 1 Corinthians 14:16–17
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220130-PrayingWellWithOthers.aac
audioBytes: 33176468
youtube: Q7SEVsBx3mM
---
