---
id: 2022-11-06-am
title: Leading a Dignified Life while Voting
date: 2022-11-06
text: 1 Timothy 2:1–7
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221106-LeadingaDignifiedLifewhileVoting.aac
audioBytes: 47238449
youtube: uZmmqu4J7rk
---
