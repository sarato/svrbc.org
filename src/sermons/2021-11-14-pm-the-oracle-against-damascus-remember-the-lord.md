---
id: 2021-11-14-pm
title: 'The Oracle Against Damascus: Remember the Lord'
date: 2021-11-14
text: Isaiah 17:1–11
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211114-TheOracleAainstDamascus-RememberTheLord.aac
audioBytes: 23689310
youtube: 9e5JT4eyceI
---
