---
id: 2016-06-26-am
title: No Compromise With God
date: 2016-06-26
text: 2 Kings 13:14–25
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160626+Death+-+Not+the+Final+Word.mp3
audioBytes: 52434757
youtube: whkY_mhbjZE
---
