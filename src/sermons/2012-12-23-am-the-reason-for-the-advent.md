---
id: 2012-12-23-am
title: The Reason for the Advent
date: 2012-12-23
text: John 3:16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-12-23+The+Reason+for+the+Advent.mp3
audioBytes: 37719372
---
