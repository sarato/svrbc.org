---
id: 2017-07-23-pm
title: Denying God’s Goodness
date: 2017-07-23
text: 1 Timothy 4:1–5
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170723+PM+Service+-+Denying+Gods+Goodness.mp3
audioBytes: 41741725
youtube: qgCFlN0e36U
---
