---
id: 2021-10-17-am
title: To Love the Truth
date: 2021-10-17
text: 2 Thessalonians 2:9–12
preacher: conley-owens
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211017-ToLoveTheTruth.aac
audioBytes: 42165805
youtube: gGt5YsxYwXw
---
