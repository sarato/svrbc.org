---
id: 2021-09-05-pm
title: The Uniqueness of the 10th Commandment
date: 2021-09-05
text: Luke 18:18–27
preacher: conley-owens
unregisteredSeries: The Uniqueness of the 10th Commandment
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210905-TheUniquenessOfThe10thCommandment.aac
audioBytes: 32886366
youtube: 1UrFJY50MlA
---
