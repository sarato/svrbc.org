---
id: 2016-07-31-pm
title: Beatitudes Part 4
date: 2016-07-31
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160731+Beatitudes+Pt+4.mp3
audioBytes: 32432473
youtube: 0-JP6oUpvRA
---
