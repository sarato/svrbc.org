---
id: 2017-09-03-pm
title: Doctrine or Demise
date: 2017-09-03
text: 1 Timothy 6:2–5
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170903+PM+Service+-+Doctrine+or+Demise.mp3
audioBytes: 30338932
youtube: PJ829w-vggY
---
