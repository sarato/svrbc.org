---
id: 2017-10-15-pm
title: To The Rich
date: 2017-10-15
text: 1 Timothy 6:17–19
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171015+PM+Service+-+To+The+Rich.mp3
audioBytes: 23883608
youtube: psWhevKgmI4
---
