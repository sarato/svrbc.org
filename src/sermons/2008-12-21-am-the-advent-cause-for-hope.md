---
id: 2008-12-21-am
title: The Advent-Cause For Hope
date: 2008-12-21
text: Titus 2:11–14
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-12-21+The+Advent-Cause+For+Hope.mp3
audioBytes: 47206122
---
