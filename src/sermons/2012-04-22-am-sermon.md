---
id: 2012-04-22-am
title: Sermon
date: 2012-04-22
text: John 19:28–29
preacher: josh-sheldon
series: john
audio: https://storage.googleapis.com/pbc-ca-sermons/2012/2012-04-22+Sermon.mp3
audioBytes: 22133580
---
