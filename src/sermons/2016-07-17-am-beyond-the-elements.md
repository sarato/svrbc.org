---
id: 2016-07-17-am
title: Beyond The Elements
date: 2016-07-17
text: Galatians 4:8–11
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160717-Beyond+The+Elements+by+Conley+Owens.mp3
audioBytes: 22672794
youtube: cF-La_JSV04
---
