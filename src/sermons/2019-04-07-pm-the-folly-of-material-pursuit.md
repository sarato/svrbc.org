---
id: 2019-04-07-pm
title: The Folly Of Material Pursuit
date: 2019-04-07
text: Isaiah 5:8–17
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190407-PM-TheFollyOfMaterialPursuit.mp3
audioBytes: 32186785
---
