---
id: 2017-10-01-am
title: A Near-by Word
date: 2017-10-01
text: Romans 10:5–13
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/171001+A+Near-by+Word.mp3
audioBytes: 47878208
youtube: 59gDcb8fvx8
---
