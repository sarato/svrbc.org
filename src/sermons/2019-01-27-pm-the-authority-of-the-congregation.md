---
id: 2019-01-27-pm
title: The Authority Of The Congregation
date: 2019-01-27
text: Matthew 20:18
preacher: conley-owens
series: authority-in-the-church
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190127-PM-TheAuthorityOfTheCongregation.mp3
audioBytes: 30828422
youtube: JfogPIctNRU
---
