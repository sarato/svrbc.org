---
id: 2008-08-24-am
title: What to do Instead of Worrying
date: 2008-08-24
text: Philippians 4
preacher: michael-phillips
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-08-24+What+to+do+Instead+of+Worrying.mp3
audioBytes: 42266340
---
