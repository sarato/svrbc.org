---
id: 2014-12-07-am
title: He Took My Place
date: 2014-12-07
text: John 18:28–40
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141207+He+Took+My+Place.mp3
audioBytes: 35518711
---
