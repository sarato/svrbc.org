---
id: 2023-06-25-pm
title: Do Not Pray For Apostates
date: 2023-06-25
text: 1 John 5:16–17
preacher: conley-owens
unregisteredSeries: I John 5
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230625-DoNotPrayForApostates.aac
audioBytes: 27645365
youtube: 8tHvAI3_O2E
---
