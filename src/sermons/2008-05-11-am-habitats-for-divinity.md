---
id: 2008-05-11-am
title: Habitats for Divinity
date: 2008-05-11
text: 1 Corinthians 6:19–20
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-05-11+Habitats+for+Divinity.mp3
audioBytes: 49783599
---
