---
id: 2014-04-18-am
title: Redemption Accomplished
date: 2014-04-18
text: John 13:12–17
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-04-18+Redemption+Accomplished.mp3
audioBytes: 31450194
---
