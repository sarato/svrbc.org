---
id: 2016-09-04-am
title: A Hymn For Endurance
date: 2016-09-04
text: 2 Timothy 2:11–13
preacher: josh-sheldon
series: hymns-of-scripture
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160904+A+Hymn+for+Endurance.mp3
audioBytes: 47247042
youtube: gY-byPSkeWg
---
