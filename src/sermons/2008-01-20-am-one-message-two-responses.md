---
id: 2008-01-20-am
title: 'One Message, Two Responses'
date: 2008-01-20
text: Acts 8:9–25
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-01-20+One+Message,+Two+Responses.mp3
audioBytes: 46434255
---
