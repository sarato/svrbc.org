---
id: 2021-06-27-am
title: Sons of Light
date: 2021-06-27
text: 1 Thessalonians 5:4–11
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210627-SonsOfLight.aac
audioBytes: 50378046
youtube: ZAm1D-FFZqc
---
