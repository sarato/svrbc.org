---
id: 2017-01-01-am
title: Plagues Released and Recalled
date: 2017-01-01
text: Joel 2:12–25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170101+Plagues+Released+and+Recalled.mp3
audioBytes: 52956373
youtube: 8TezRz-TgLw
---
