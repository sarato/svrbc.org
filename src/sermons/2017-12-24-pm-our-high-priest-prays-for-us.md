---
id: 2017-12-24-pm
title: Our High Priest Prays For Us
date: 2017-12-24
text: John 17:6–10
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171224+PM+Service+-+Our+High+Priest+Prays+For+Us.mp3
audioBytes: 25837867
youtube: 8xkGVytkHFs
---
