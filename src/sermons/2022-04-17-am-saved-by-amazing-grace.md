---
id: 2022-04-17-am
title: Saved by Amazing Grace
date: 2022-04-17
text: Ephesians 2:8–10
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220417-SavedbyAmazingGrace.aac
audioBytes: 55272220
youtube: QNEDO844Jdo
---
