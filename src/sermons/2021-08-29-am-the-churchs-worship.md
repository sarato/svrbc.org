---
id: 2021-08-29-am
title: The Church’s Worship
date: 2021-08-29
text: 1 Thessalonians 5:25–28
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210829-TheChurchsWorship.aac
audioBytes: 46385037
youtube: 8lBbx2tjzZI
---
