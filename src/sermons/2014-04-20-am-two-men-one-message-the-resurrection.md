---
id: 2014-04-20-am
title: 'Two Men, One Message – The Resurrection'
date: 2014-04-20
text: Acts 2:22–36; 13:16–41
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-04-20+Two+Men%2C+One+Message+-+Ther+Resurrection.mp3
audioBytes: 40523280
---
