---
id: 2007-12-09-am
title: A Fixed Gaze
date: 2007-12-09
text: Acts 7:54–60
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-12-09+A+Fixed+Gaze.mp3
audioBytes: 50081754
---
