---
id: 2023-02-12-am
title: Finding Favor With God And Man
date: 2023-02-12
text: Luke 2:41–52
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230212-FindingFavorWithGodAndMan.aac
audioBytes: 49370793
youtube: mP6zBB72R-0
---
