---
id: 2017-10-08-pm
title: Help Along The Way
date: 2017-10-08
text: 1 Timothy 6:12–16
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/20171008+PM+Servicce+-+Help+Along+the+Way.mp3
audioBytes: 23357380
youtube: pEhQNyD-NYE
---
