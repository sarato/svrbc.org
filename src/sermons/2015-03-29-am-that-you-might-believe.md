---
id: 2015-03-29-am
title: That You Might Believe
date: 2015-03-29
text: John 20:30–31; 21:24
preacher: josh-sheldon
series: the-passion-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150329+That+You+Might+Believe.mp3
audioBytes: 42904448
---
