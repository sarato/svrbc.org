---
id: 2019-04-21-am
title: 'Our Victory, In Jesus'
date: 2019-04-21
text: 1 Corinthians 15:50–57
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190421-AM-OVictoryInJesus.mp3
audioBytes: 44344806
---
