---
id: 2009-08-30-am
title: No More Holding Back
date: 2009-08-30
text: Acts 19
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-08-30+No+More+Holding+Back.mp3
audioBytes: 44681604
---
