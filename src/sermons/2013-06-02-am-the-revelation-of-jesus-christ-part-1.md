---
id: 2013-06-02-am
title: 'The Revelation of Jesus Christ, Part 1'
date: 2013-06-02
text: Revelation 1:9–20
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-06-02+The+Revelation+of+Jesus+Christ.mp3
audioBytes: 60377067
---
