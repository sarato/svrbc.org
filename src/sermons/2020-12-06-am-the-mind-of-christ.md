---
id: 2020-12-06-am
title: The Mind of Christ
date: 2020-12-06
text: 1 Corinthians 2:14–16
preacher: conley-owens
series: kingdom-community
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/201206-TheMindOfChrist.mp3
audioBytes: 54217113
youtube: 9Z6c7s6EBgg
---
