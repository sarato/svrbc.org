---
id: 2014-07-06-am
title: A Warning Against the Influence of Worldly Wisdom and its Riches
date: 2014-07-06
text: James 5:1–6
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-07-06+A+Warning+Against+the+influence+of+Wordly+Wisdom+and+its+Riches.mp3
audioBytes: 62640543
---
