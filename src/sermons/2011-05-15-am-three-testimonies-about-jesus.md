---
id: 2011-05-15-am
title: Three Testimonies About Jesus
date: 2011-05-15
text: John 1:29–34
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-05-15+Three+Testimonies+About+Jesus.mp3
audioBytes: 51452160
---
