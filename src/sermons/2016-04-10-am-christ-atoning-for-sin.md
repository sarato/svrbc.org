---
id: 2016-04-10-am
title: 'Christ, Atoning for Sin'
date: 2016-04-10
text: Exodus 32:30–35
preacher: eric-hollingsworth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160410+Christ+Atoning+for+Sin.mp3
audioBytes: 47950476
youtube: DYXCdasF6Hk
---
