---
id: 2023-07-02-pm
title: Living In The Truth
date: 2023-07-02
text: 3 John
preacher: josh-sheldon
unregisteredSeries: 3 John
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230702-LivingInTheTruth-a.aac
audioBytes: 28061005
youtube: wunE1FuRQnw
---
