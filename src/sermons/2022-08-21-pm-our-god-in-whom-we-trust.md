---
id: 2022-08-21-pm
title: 'Our God, In Whom We Trust'
date: 2022-08-21
text: Psalm 91:14–16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220821-OurGodInWhomWeTrust.aac
audioBytes: 39208022
youtube: xQEe45l9I-8
---
