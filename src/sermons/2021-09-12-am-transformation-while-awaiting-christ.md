---
id: 2021-09-12-am
title: Transformation While Awaiting Christ
date: 2021-09-12
text: 1 Thessalonians 2:11–12
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210912-TransformationWhileAwaitingChrist.aac
audioBytes: 46403193
youtube: ChWiK8oOyuc
---
