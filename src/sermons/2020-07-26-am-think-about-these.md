---
id: 2020-07-26-am
title: Think About These
date: 2020-07-26
text: Philippians 4:8
preacher: josh-sheldon
series: philippians
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/200726-ThinkAboutThese.mp3
audioBytes: 64821751
youtube: kq3hrbTagQ0
---
