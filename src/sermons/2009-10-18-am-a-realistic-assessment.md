---
id: 2009-10-18-am
title: A Realistic Assessment
date: 2009-10-18
text: Acts 20:24
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-10-18+A+Realistic+Assessment.mp3
audioBytes: 49452084
---
