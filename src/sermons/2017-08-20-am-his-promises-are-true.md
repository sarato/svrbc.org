---
id: 2017-08-20-am
title: His Promises Are True
date: 2017-08-20
text: Romans 9:6–13
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170820+His+Promises+Are+True.mp3
audioBytes: 49969582
youtube: 3hJtMqT3rVA
---
