---
id: 2022-12-18-pm
title: A Greater Salvation
date: 2022-12-18
text: Isaiah 29:17–19
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221218-AGreaterSalvation.aac
audioBytes: 30597356
youtube: raQtXKDwOfI
---
