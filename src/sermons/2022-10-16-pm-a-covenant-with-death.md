---
id: 2022-10-16-pm
title: A Covenant with Death
date: 2022-10-16
text: Isaiah 28:14–22
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221016-ACovenantWithDeath.aac
audioBytes: 32993688
youtube: 43R0LbHrbe8
---
