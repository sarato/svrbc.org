---
id: 2016-01-31-am
title: God’s Call By The Psalms
date: 2016-01-31
text: Psalm 19
preacher: steve-mixsell
series: gods-call-in-psalm-19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160131+Gods+Call+By+the+Psalms.mp3
audioBytes: 55555650
youtube: bepJdYFJMvY
---
