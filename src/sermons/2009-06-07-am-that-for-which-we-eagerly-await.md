---
id: 2009-06-07-am
title: That For Which We Eagerly Await
date: 2009-06-07
text: Romans 8:23
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-06-07+That+For+Which+We+Eagerly+Await.mp3
audioBytes: 50962458
---
