---
id: 2022-07-10-pm
title: Staying the Mind
date: 2022-07-10
text: Isaiah 26:1–6
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220710-StayingTheMind.aac
audioBytes: 29083969
youtube: 1ydiF0u0-yA
---
