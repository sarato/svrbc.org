---
id: 2023-02-12-pm
title: The Cost of Faithlessness (The Oracle of the Beasts of the Negeb)
date: 2023-02-12
text: Isaiah 30:1–5
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230212-TheCostOfFaithlessness.aac
audioBytes: 27030161
youtube: SPDGn70qUGw
---
