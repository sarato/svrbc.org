---
id: 2017-08-06-pm
title: Taking God’s View – Dignity
date: 2017-08-06
text: 1 Timothy 5:1–16
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170806+PM+Service+-+Taking+Gods+View+-+Dignity.mp3
audioBytes: 27925705
youtube: GTwdVZP3mgg
---
