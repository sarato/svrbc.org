---
id: 2011-06-05-am
title: The Gospel Changes Our Prayer Lives
date: 2011-06-05
text: Romans 8
preacher: michael-phillips
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-06-05+The+Gospel+Changes+Our+Prayer+Lives.mp3
audioBytes: 54527616
---
