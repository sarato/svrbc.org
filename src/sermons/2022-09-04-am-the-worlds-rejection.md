---
id: 2022-09-04-am
title: The World’s Rejection
date: 2022-09-04
text: 1 Samuel 17:28–33
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220904-TheWorldsRejection.aac
audioBytes: 40469540
youtube: MuLnto_8j04
---
