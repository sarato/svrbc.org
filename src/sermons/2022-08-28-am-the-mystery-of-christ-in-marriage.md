---
id: 2022-08-28-am
title: The Mystery of Christ in Marriage
date: 2022-08-28
text: Ephesians 5:28–33
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220828-TheMysteryOfChristInMarriage.aac
audioBytes: 50402179
youtube: vCCk37uIox0
---
