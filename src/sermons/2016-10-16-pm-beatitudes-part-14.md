---
id: 2016-10-16-pm
title: Beatitudes Part 14
date: 2016-10-16
text: Matthew 6:5–14
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161016+Afternoon+Service+-+Beatitudes+Part+14.mp3
audioBytes: 26987794
youtube: Py9r7TgrqH4
---
