---
id: 2011-12-25-am
title: God Sent His Son
date: 2011-12-25
text: Galatians 4:4–7
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-12-25+God+Sent+His+Son.mp3
audioBytes: 26019186
---
