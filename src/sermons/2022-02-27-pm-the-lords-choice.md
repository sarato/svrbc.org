---
id: 2022-02-27-pm
title: The Lord’s Choice
date: 2022-02-27
text: 1 Samuel 16:1–13
preacher: josh-sheldon
series: the-life-of-david
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220227-TheLordsChoice.aac
audioBytes: 48541352
youtube: 0pSub0BQ-cE
---
