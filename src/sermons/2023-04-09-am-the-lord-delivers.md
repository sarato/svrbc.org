---
id: 2023-04-09-am
title: The Lord Delivers
date: 2023-04-09
text: Psalm 34:19–22
preacher: josh-sheldon
unregisteredSeries: Psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230409-TheLordDelivers.aac
audioBytes: 41513715
youtube: kLmYtGAJ1Tk
---
