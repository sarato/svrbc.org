---
id: 2011-10-30-am
title: True Worship Unveiled
date: 2011-10-30
text: John 4:23–26
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-10-30+True+Worship+Unveiled.mp3
audioBytes: 49672260
---
