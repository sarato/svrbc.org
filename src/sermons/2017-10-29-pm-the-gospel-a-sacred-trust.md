---
id: 2017-10-29-pm
title: The Gospel – A Sacred Trust
date: 2017-10-29
text: 1 Timothy 6:20–21
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171022+PM+Service+-+The+Gospel+-+A+Sacred+Trust.mp3
audioBytes: 25967548
youtube: nmPLiuF4NVk
---
