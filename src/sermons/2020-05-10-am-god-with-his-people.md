---
id: 2020-05-10-am
title: God With His People
date: 2020-05-10
text: Ezekiel 11
preacher: josh-sheldon
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200510-GodWithHisPeople-am.mp3
audioBytes: 85489559
youtube: AgZXeRqu-MU
---
