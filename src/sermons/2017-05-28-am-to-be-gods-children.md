---
id: 2017-05-28-am
title: To Be God’s Children
date: 2017-05-28
text: Romans 8:12–17
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170528+To+Be+Gods+Children.mp3
audioBytes: 57052836
youtube: lLDKQvoyy6s
---
