---
id: 2019-02-10-pm
title: The Deacons’ Authority
date: 2019-02-10
text: Acts 6:3
preacher: conley-owens
series: authority-in-the-church
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190210-PM-TheDeaconsAuthority.mp3
audioBytes: 27254850
youtube: T3SwUR661ns
---
