---
id: 2015-06-28-am
title: 'What Just Happened?: The Court’s Decision'
date: 2015-06-28
text: Psalm 11
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150628+What+Just+Happened.mp3
audioBytes: 44618913
---
