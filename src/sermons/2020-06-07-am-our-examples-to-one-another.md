---
id: 2020-06-07-am
title: Our Examples To One Another
date: 2020-06-07
text: Philippians 3:17–21
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200607-OurExamplesToOneAnother.mp3
audioBytes: 65345344
youtube: 6cebfXnG9OI
---
