---
id: 2016-03-27-am
title: The Risen Lord
date: 2016-03-27
text: Luke 24:13–27
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160327+The+Risen+Lord.mp3
audioBytes: 40761145
youtube: pjs0ozht71o
---
