---
id: 2021-07-11-am
title: Respect in the Church
date: 2021-07-11
text: 1 Thessalonians 5:12–13
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210711-RespectInTheChurch.aac
audioBytes: 48226800
youtube: 8TSVVKRTlmU
---
