---
id: 2011-09-25-am
title: He is Not Mocked
date: 2011-09-25
text: John 3:31–36
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-09-25+He+is+Not+Mocked.mp3
audioBytes: 38566299
---
