---
id: 2022-08-14-pm
title: To Trust the Lord
date: 2022-08-14
text: Psalm 91:11–13
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220814-ToTrustTheLord.aac
audioBytes: 32920707
youtube: Lp78vgcBvUc
---
