---
id: 2022-02-27-am
title: Chosen in Love
date: 2022-02-27
text: Ephesians 1:3–6
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220227-ChoseninLove.aac
audioBytes: 58594078
youtube: t0K35xnyeoc
---
