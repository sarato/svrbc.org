---
id: 2021-12-26-am
title: Triumphant in the Lord
date: 2021-12-26
text: 2 Corinthians 2:14–17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211226-TriumphantintheLord.aac
audioBytes: 31489919
youtube: CLWTGVhXCAA
---
