---
id: 2018-02-11-pm
title: 'Jonah’s Prayer, God’s Deliverance'
date: 2018-02-11
text: Jonah 1:7–2:10
preacher: josh-sheldon
series: jonah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180211-PM-JonahsPrayerGodsDeliverance.mp3
audioBytes: 33281018
youtube: bn8SwC5YXGU
---
