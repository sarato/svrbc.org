---
id: 2020-09-06-am
title: Our God Of The Ordinary
date: 2020-09-06
text: Ruth 1
preacher: josh-sheldon
series: ruth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200907-OurGodOfTheOrdinary.mp3
audioBytes: 68589653
youtube: zm7c9blrQJc
---
