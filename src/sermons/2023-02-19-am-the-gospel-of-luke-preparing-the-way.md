---
id: 2023-02-19-am
title: 'The Gospel of Luke: Preparing The Way'
date: 2023-02-19
text: Luke 3:1–6
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230219-PreparingTheWay.aac
audioBytes: 46468451
youtube: xOPCydrMxts
---
