---
id: 2009-06-14-am
title: How We are Saved
date: 2009-06-14
text: Acts 15:11
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-06-14+How+We+are+Saved.mp3
audioBytes: 54180447
---
