---
id: 2023-04-02-am
title: A Temporary Humility
date: 2023-04-02
text: Hebrews 2:8–9
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230402-ATemporaryHumility.aac
audioBytes: 51326902
youtube: sU1D3TU_TWU
---
