---
id: 2022-07-03-am
title: Speaking the Truth in Love
date: 2022-07-03
text: Ephesians 4:14–16
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220703-SpeakingTheTruthInLove.aac
audioBytes: 21916794
youtube: NW6K3zTqCnc
---
