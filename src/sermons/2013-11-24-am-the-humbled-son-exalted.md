---
id: 2013-11-24-am
title: The Humbled Son Exalted
date: 2013-11-24
text: Genesis 45
preacher: josh-sheldon
series: the-life-of-joseph
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-11-24+The+Humbled+Son+Exalted.mp3
audioBytes: 47530548
---
