---
id: 2022-08-07-am
title: Walk as Children of Light
date: 2022-08-07
text: Ephesians 5:6–14
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220807-WalkAsChildrenOfLight.aac
audioBytes: 48339795
youtube: agIhA4SQYU4
---
