---
id: 2021-07-04-am
title: 'Sons of Light, Part 2'
date: 2021-07-04
text: 1 Thessalonians 5:4–11
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210704-SonsOfLight-Part2.aac
audioBytes: 54881119
youtube: FcbekHghs4U
---
