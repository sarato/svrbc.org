---
id: 2016-05-15-am
title: God’s Word Is Final
date: 2016-05-15
text: 2 Kings 8:7–15; 9:1–13
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160515+Gods+Word+is+Final.mp3
audioBytes: 52868218
youtube: K7VxoSPzhUU
---
