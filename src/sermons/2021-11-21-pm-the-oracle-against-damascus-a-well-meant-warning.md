---
id: 2021-11-21-pm
title: 'The Oracle Against Damascus: A Well-Meant Warning'
date: 2021-11-21
text: Isaiah 17:12–18:7
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211121-TheOracleAgainstDamascus-AWell%3DMeantWarning.aac
audioBytes: 30348132
youtube: OV8BuoFoNZI
---
