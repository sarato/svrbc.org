---
id: 2019-11-17-am
title: 'Insignificant, But Not Unimportant'
date: 2019-11-17
text: Psalm 8
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191117-AM-InsignificantButNotUnimportant.mp3
audioBytes: 55772631
---
