---
id: 2018-09-23-am
title: The Lord Finishes What He Starts
date: 2018-09-23
text: Ezra 6:13–18
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180923-AM-TheLordFinishesWhatHeStarts.mp3
audioBytes: 54252939
---
