---
id: 2023-05-07-pm
title: The Greatest Deception
date: 2023-05-07
text: 2 John 7–13
preacher: josh-sheldon
series: 2-john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230430-TheGreatestDeception.aac
audioBytes: 28286836
youtube: gYOGpqklOY8
---
