---
id: 2021-06-13-am
title: With Jesus Together and at Once
date: 2021-06-13
text: 1 Thessalonians 4:13–18
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210613-WithJesusTogetherAndAtOnce.aac
audioBytes: 50340438
youtube: eHCdFaFO7i8
---
