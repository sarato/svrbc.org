---
id: 2018-09-09-pm
title: The Power Of Discouragement
date: 2018-09-09
text: Haggai 2:1–9
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180909-PM-ThePowerOfDiscouragement.mp3
audioBytes: 35171008
---
