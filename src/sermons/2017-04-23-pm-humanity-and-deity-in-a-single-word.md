---
id: 2017-04-23-pm
title: Humanity And Deity In A Single Word
date: 2017-04-23
text: John 19:28–29
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170423+PM+Service+-+Humanity+And+Deity+In+A+Single+Word.mp3
audioBytes: 21363748
youtube: 5cADebjGqV8
---
