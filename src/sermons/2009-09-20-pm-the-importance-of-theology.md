---
id: 2009-09-20-pm
title: The Importance of Theology
date: 2009-09-20
text: Colossians 2:6–8
preacher: baruch-maoz
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-09-20+Baruch+Maoz+Part+2.mp3
audioBytes: 47432970
---
