---
id: 2014-08-31-ev
title: The Atonement
date: 2014-08-31
text: Matthew 12:29
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140831+5th+Sunday+-+Atonement.mp3
audioBytes: 51167471
---

This message was delivered at an evening gathering of several local churches.
