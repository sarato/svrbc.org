---
id: 2009-09-20-am
title: God’s Righteousness in Romans
date: 2009-09-20
text: Romans 6–8
preacher: baruch-maoz
audio: https://storage.googleapis.com/pbc-ca-sermons/2009/2009-09-20+Baruch+Maoz.mp3
audioBytes: 72650628
---
