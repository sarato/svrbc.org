---
id: 2007-05-27-am
title: The Assurance of Salvation
date: 2007-05-27
text: 2 Peter
preacher: chris-rhodes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-05-27+The+Assurance+of+Salvation.mp3
audioBytes: 46154031
---
