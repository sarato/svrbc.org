---
id: 2011-09-11-am
title: A Privilege and a Peril
date: 2011-09-11
text: John 3:18–21
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-09-11+A+Privilege+and+a+Peril.mp3
audioBytes: 38812224
---
