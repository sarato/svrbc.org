---
id: 2022-10-30-pm
title: The Destruction of Ariel
date: 2022-10-30
text: Isaiah 29:1–4
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221030-TheDestructionofAriel.aac
audioBytes: 33529470
youtube: 3QvLaTOVCkw
---
