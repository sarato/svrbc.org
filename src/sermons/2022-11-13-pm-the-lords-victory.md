---
id: 2022-11-13-pm
title: The Lord’s Victory
date: 2022-11-13
text: 1 Samuel 17:38–54
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/221113-TheLordsVictory.aac
audioBytes: 33306261
youtube: mdgXcakTQew
---
