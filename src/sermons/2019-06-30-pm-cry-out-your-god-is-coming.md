---
id: 2019-06-30-pm
title: 'Cry Out, Your God Is Coming'
date: 2019-06-30
text: Ezekiel 3:16–21
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190630-PM-CryOutYourGodIsComing.mp3
audioBytes: 36959879
---
