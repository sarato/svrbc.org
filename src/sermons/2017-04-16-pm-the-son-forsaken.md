---
id: 2017-04-16-pm
title: The Son Forsaken
date: 2017-04-16
text: Matthew 27:45–46
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170416+PM+Service+-+The+Son+Forsaken.mp3
audioBytes: 21679294
youtube: UoHw1Fvn21E
---
