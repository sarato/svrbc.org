---
id: 2018-12-02-am
title: Sin’s Terrible Reach
date: 2018-12-02
text: Ezra 10
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181202-AM-SinsTerribleReach.mp3
audioBytes: 52062811
---
