---
id: 2016-01-17-am
title: 'God’s Care, Up Close and Personal'
date: 2016-01-17
text: 2 Kings 4:1–7
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160117+Gods+Care+Up+Close+and+Personal.mp3
audioBytes: 48878767
youtube: VN-IKhzOmas
---
