---
id: 2017-04-09-am
title: In Defense Of God
date: 2017-04-09
text: Romans 7:7–12
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170409+In+Defense+Of+God.mp3
audioBytes: 51559176
youtube: 648aXVLpzTM
---
