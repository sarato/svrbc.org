---
id: 2013-02-17-am
title: Christian Worship (by Don Penhale)
date: 2013-02-17
text: Deuteronomy 4:16–19
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-02-17+Christian+Worship+by+Don+Penhale.mp3
audioBytes: 15949470
---
