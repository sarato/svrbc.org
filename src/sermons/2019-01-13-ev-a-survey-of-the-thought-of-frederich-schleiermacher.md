---
id: 2019-01-13-ev
title: A Survey Of The Thought Of Frederich Schleiermacher
date: 2019-01-13
preacher: steve-watkins
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190113-JS-ASurveyOfTheThoughtOfFrederichSchleiermacher.mp3
audioBytes: 61374956
---

This message was delivered at an evening gathering of several local churches.
