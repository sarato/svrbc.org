---
id: 2009-01-25-am
title: Preparing For Worship
date: 2009-01-25
text: 2 Chronicles 29
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-01-25+Preparing+For+Worship.mp3
audioBytes: 47455071
---
