---
id: 2007-04-15-am
title: Sermon
date: 2007-04-15
text: 2 Timothy 4:17
preacher: mike-mccullough
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-04-15+guest+preacher.mp3
audioBytes: 33626100
---
