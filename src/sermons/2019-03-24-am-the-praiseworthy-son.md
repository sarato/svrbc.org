---
id: 2019-03-24-am
title: The Praiseworthy Son
date: 2019-03-24
text: Hebrews 1:6–7
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190324-AM-ThePraiseworthySon.mp3
audioBytes: 35761161
---
