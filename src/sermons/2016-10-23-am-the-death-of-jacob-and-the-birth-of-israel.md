---
id: 2016-10-23-am
title: The Death of Jacob and the Birth of Israel
date: 2016-10-23
text: Genesis 49:29–50:14
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161023+The+Death+of+Jacob+and+the+Birth+of+Isreal.mp3
audioBytes: 33998233
youtube: LINKhxL4xE0
---
