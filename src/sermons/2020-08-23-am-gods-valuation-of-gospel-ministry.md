---
id: 2020-08-23-am
title: God’s Valuation Of Gospel Ministry
date: 2020-08-23
text: Philippians 4:14–20
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200823-GodsValuationOfGospelMinistry.mp3
audioBytes: 59198737
youtube: XZhnvJmBjIY
---
