---
id: 2007-09-09-am
title: Snares and Traps
date: 2007-09-09
text: 1 Samuel 18:21
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-09-09+Snares+and+Traps.mp3
audioBytes: 52116714
---
