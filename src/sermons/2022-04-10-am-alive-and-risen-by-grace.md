---
id: 2022-04-10-am
title: Alive and Risen by Grace
date: 2022-04-10
text: Ephesians 2:4–7
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220410-AliveandRisenByGrace.aac
audioBytes: 51036447
youtube: OHyxiMuDnVM
---
