---
id: 2023-06-11-pm
title: The Spiritual Discipline of Watchfulness
date: 2023-06-11
text: Proverbs 4:23; Matthew 26:41
unregisteredPreacher: Stephen Louis
unregisteredSeries: Watchfulness
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230611-TheSpiritualDisciplineOfWatchfulness.aac
audioBytes: 29936001
youtube: iX73jWKG91s
---
