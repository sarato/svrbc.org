---
id: 2022-11-20-pm
title: Like a Dream
date: 2022-11-20
text: Isaiah 29:5–8
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/221120-LikeADream.aac
audioBytes: 30954410
youtube: hRqb_KeuGK8
---
