---
id: 2018-12-02-pm
title: The Crown He Wore
date: 2018-12-02
text: John 19:1–5
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181202-PM-TheCrownHeWore.mp3
audioBytes: 25173830
---
