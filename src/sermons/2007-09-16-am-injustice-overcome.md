---
id: 2007-09-16-am
title: Injustice Overcome
date: 2007-09-16
text: Acts 5:17–42
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-09-16+Injustice+Overcome.mp3
audioBytes: 55468143
---
