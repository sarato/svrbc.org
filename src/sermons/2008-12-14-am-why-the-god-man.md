---
id: 2008-12-14-am
title: Why The God Man
date: 2008-12-14
text: Matthew 1:18–25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-12-14+Why+The+God+Man.mp3
audioBytes: 42909354
---
