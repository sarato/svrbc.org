---
id: 2023-03-12-pm
title: 'The Teacher, Revealed'
date: 2023-03-12
text: Isaiah 30:18–22
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230312-TheTeacher_Revealed-a.aac
audioBytes: 36334435
youtube: b2uoXPeAqug
---
