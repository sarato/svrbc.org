---
id: 2017-05-07-pm
title: Into You Hands
date: 2017-05-07
text: Luke 23:44–46
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170507+PM+Service+-+Into+You+Hands.mp3
audioBytes: 27061347
youtube: 5AF2RW_w3jQ
---
