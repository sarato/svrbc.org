---
id: 2022-07-10-am
title: God’s Loving Kindness
date: 2022-07-10
text: Psalm 23
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220710-GodsLovingKindness.aac
audioBytes: 41914711
youtube: AK4_Z6n9xjQ
---
