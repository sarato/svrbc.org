---
id: 2018-08-05-am
title: First Things First
date: 2018-08-05
text: Ezra 3
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180805-AM-FirstThingsFirst.mp3
audioBytes: 50462024
---
