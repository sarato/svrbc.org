---
id: 2021-03-21-am
title: A Legacy You Can Take With You
date: 2021-03-21
text: 1 Thessalonians 2:17–20
preacher: conley-owens
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210321-ALegacyYouCanTakeWithYou.mp3
audioBytes: 58513635
youtube: 3jjsTbKGX9g
---
