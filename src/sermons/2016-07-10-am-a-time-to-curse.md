---
id: 2016-07-10-am
title: A Time To Curse
date: 2016-07-10
text: Psalm 109
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160710+A+Time+to+Curse.mp3
audioBytes: 49886031
youtube: LrKW-rHy92E
---
