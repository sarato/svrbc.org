---
id: 2013-03-03-am
title: The Tongue Part 1
date: 2013-03-03
text: James 3:1–12
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-03-03+The+Tongue+Part+1.mp3
audioBytes: 52672992
---
