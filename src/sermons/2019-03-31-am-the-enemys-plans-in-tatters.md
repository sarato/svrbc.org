---
id: 2019-03-31-am
title: The Enemy’s Plans In Tatters
date: 2019-03-31
text: Nehemiah 6
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190331-AM-TheEnemysPlansInTatters.mp3
audioBytes: 58028785
---
