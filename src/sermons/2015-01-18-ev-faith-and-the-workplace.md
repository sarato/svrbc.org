---
id: 2015-01-18-ev
title: Faith And the Workplace
date: 2015-01-18
text: Matthew 6:24–25; 16:25
preacher: jeff-louie
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150118+Faith+And+the+Workplace.mp3
audioBytes: 58079676
---

This message was delivered at an evening gathering of several local churches.
