---
id: 2018-07-01-pm
title: What’s The Big Deal?
date: 2018-07-01
text: Zephaniah 1:4–18
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180701-PM-WhatsTheBigDeal.mp3
audioBytes: 36330010
youtube: OW0xxxWw7Gg
---
