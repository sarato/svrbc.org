---
id: 2019-10-20-am
title: Are You Following Jesus?
date: 2019-10-20
text: Matthew 4:12–25
preacher: daniel-uhm
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191020-AM-AreYouFollowingJesus.mp3
audioBytes: 39830020
---
