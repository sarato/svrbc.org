---
id: 2012-01-29-am
title: Hard Hearts Miss The Point
date: 2012-01-29
text: John 6:16–21
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-01-29+Hard+Hearts+Miss+The+Point.mp3
audioBytes: 39175119
---
