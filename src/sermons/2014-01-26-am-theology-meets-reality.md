---
id: 2014-01-26-am
title: Theology Meets Reality
date: 2014-01-26
text: Psalm 118
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-01-26+Theology+Meets+Reality.mp3
audioBytes: 37710198
---
