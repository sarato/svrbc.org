---
id: 2023-02-05-am
title: The Consolation Of Israel
date: 2023-02-05
text: Luke 2:22–40
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230205-TheConsolationOfIsrael.aac
audioBytes: 51029221
youtube: vRGzQ_eIIMk
---
