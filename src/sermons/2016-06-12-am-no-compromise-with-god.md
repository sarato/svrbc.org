---
id: 2016-06-12-am
title: No Compromise With God
date: 2016-06-12
text: 2 Kings 10:28–36
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160612+No+Compromise+With+God.mp3
audioBytes: 53279449
youtube: Y_fgloY_PtE
---
