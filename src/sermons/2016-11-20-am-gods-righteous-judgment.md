---
id: 2016-11-20-am
title: God’s Righteous Judgment
date: 2016-11-20
text: Romans 1:32–2:16
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161120+Gods+Righteus+Judgement.mp3
audioBytes: 47719339
---
