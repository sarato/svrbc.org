---
id: 2007-02-11-am
title: 'Syncretism, Alive and Well'
date: 2007-02-11
text: Ezekiel 8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-02-11+Syncretism,+Alive+and+Well.mp3
audioBytes: 50542122
---
