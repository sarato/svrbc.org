---
id: 2020-07-12-am
title: Guarded By God’s Peace
date: 2020-07-12
text: Philippians 4:7
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200712-GuardedByGodsPeace.mp3
audioBytes: 60710287
youtube: A8dIpg-Bf9s
---
