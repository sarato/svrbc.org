---
id: 2023-05-28-am
title: The Call of Jesus to be Fishers of Men
date: 2023-05-28
text: Luke 5:1–11
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230528-TheCallOfJesusToBeFishersOfMen.aac
audioBytes: 46734537
youtube: 0Pc_ZnuLczM
---
