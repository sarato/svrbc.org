---
id: 2007-12-23-am
title: Christmas-Sovereignty on Display
date: 2007-12-23
text: Matthew 2:1–12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-12-23+Christmas-Sovereignty+on+Display.mp3
audioBytes: 51238095
---
