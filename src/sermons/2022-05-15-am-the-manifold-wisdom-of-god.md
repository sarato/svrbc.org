---
id: 2022-05-15-am
title: The Manifold Wisdom of God
date: 2022-05-15
text: Ephesians 3:7–13
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220515-TheManifoldWisdomofGod.aac
audioBytes: 41098389
youtube: NkiI-oozoLg
---
