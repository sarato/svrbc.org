---
id: 2021-02-21-am
title: Sharing Our Very Selves
date: 2021-02-21
text: 1 Thessalonians 2:6–8
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210221-SharingOurVerySelves.mp3
audioBytes: 60364217
youtube: REiZrJXdkMQ
---
