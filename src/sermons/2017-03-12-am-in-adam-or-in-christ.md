---
id: 2017-03-12-am
title: 'In Adam, or in Christ'
date: 2017-03-12
text: Romans 5:12–21
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170312+In+Adam+or+in+Christ.mp3
audioBytes: 52251268
youtube: TqH79cJ_RLE
---
