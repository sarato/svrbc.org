---
id: 2018-12-09-pm
title: The Exaltation Of The Lord
date: 2018-12-09
text: Isaiah 2:5–22
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181209-PM-TheExhaltationOfTheLord.mp3
audioBytes: 33484123
---
