---
id: 2019-02-17-am
title: 'If God Is For Us, Nothing Can Stop Us'
date: 2019-02-17
text: Nehemiah 4:1–9
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190217-AM-IfGodIsForUsNothingCanStopUs.mp3
audioBytes: 47932988
youtube: _gbH_AKrN9c
---
