---
id: 2022-09-18-pm
title: Praying in Trials
date: 2022-09-18
text: Isaiah 27:7–11
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220918-PrayingInTrials.aac
audioBytes: 31352719
youtube: N3wqr8Mp5dw
---
