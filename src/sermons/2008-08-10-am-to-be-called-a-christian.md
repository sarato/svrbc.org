---
id: 2008-08-10-am
title: To Be Called A Christian
date: 2008-08-10
text: Acts 11:22–30
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-08-10+To+Be+Called+A+Christian.mp3
audioBytes: 48949599
---
