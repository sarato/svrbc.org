---
id: 2020-08-30-am
title: Trusting In God’s Provision
date: 2020-08-30
text: Philippians 4:19–23
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200830-TrustingInGodsProvision.mp3
audioBytes: 70437242
youtube: 31tvGEBJaCs
---
