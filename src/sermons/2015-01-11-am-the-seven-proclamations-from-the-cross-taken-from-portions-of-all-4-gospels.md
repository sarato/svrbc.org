---
id: 2015-01-11-am
title: The Seven Proclamations From The Cross (taken from portions of all 4 gospels)
date: 2015-01-11
text: John 19:17–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150111+The+Seven+Proclamations+From+The+Cross.mp3
audioBytes: 39822091
---
