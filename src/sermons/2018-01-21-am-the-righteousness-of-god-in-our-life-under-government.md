---
id: 2018-01-21-am
title: The Righteousness of God in Our Life Under Government
date: 2018-01-21
text: Romans 13:1–7
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180121-AM-TheRighteousnessOfGodInOurLifeUnderGovernment.mp3
audioBytes: 48262349
youtube: fhA7H0zHkgk
---
