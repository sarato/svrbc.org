---
id: 2019-01-06-am
title: Nehemiah’s Prayer
date: 2019-01-06
text: Nehemiah 1:6–11
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190106-AM-NehemiahsPrayer.mp3
audioBytes: 76424172
---
