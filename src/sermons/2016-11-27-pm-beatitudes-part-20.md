---
id: 2016-11-27-pm
title: Beatitudes Part 20
date: 2016-11-27
text: Matthew 6:25–34
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161127+Afternoon+Service+-+Beatitudes+Part+20.mp3
audioBytes: 23758643
youtube: YfDE_dhuAZE
---
