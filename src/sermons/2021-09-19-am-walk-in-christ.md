---
id: 2021-09-19-am
title: Walk in Christ
date: 2021-09-19
text: Colossians 2:1–10
preacher: brian-garcia
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210919-WalkInChrist.aac
audioBytes: 55882117
youtube: h1cpPoWjPgE
---
