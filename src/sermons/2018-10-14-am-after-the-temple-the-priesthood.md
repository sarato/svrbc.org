---
id: 2018-10-14-am
title: 'After The Temple, The Priesthood'
date: 2018-10-14
text: Ezra 7
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181014-AM-AfterTheTemple-ThePriesthood.mp3
audioBytes: 51946629
---
