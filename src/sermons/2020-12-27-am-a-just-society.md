---
id: 2020-12-27-am
title: A Just Society
date: 2020-12-27
text: Isaiah 11:1–5
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/201227-TheMindOfChrist.mp3
audioBytes: 59717216
youtube: qGLvEDpaiqs
---
