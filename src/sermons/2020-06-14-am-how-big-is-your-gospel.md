---
id: 2020-06-14-am
title: How Big Is Your Gospel?
date: 2020-06-14
text: Philippians 4:1–3
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200614-HowBigIsYourGospel.mp3
audioBytes: 63421269
youtube: xIZ-0FjPmp8
---
