---
id: 2020-05-31-am
title: Don’t Look Back
date: 2020-05-31
text: Philippians 3:12–16
preacher: josh-sheldon
series: philippians
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/200531-DontLookBack.mp3
audioBytes: 73899301
youtube: AoyS3269puI
---
