---
id: 2018-12-16-am
title: Meet The Man
date: 2018-12-16
text: Nehemiah 1:1–4
preacher: josh-sheldon
series: ezra-nehemiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/181216-AM-MeetTheMan.mp3
audioBytes: 51909426
---
