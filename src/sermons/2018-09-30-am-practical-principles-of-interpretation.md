---
id: 2018-09-30-am
title: Practical Principles Of Interpretation
date: 2018-09-30
text: Acts 8:35
preacher: murungi-igweta
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180930-AM-PracticalPrinciplesOfInterpretation.mp3
audioBytes: 58433617
---
