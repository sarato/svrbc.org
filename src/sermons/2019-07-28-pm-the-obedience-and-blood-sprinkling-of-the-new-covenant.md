---
id: 2019-07-28-pm
title: The Obedience And Blood Sprinkling Of The New Covenant
date: 2019-07-28
text: 1 Peter 1:1–2
preacher: ken-tompkins
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190728-PM-TheObedienceAndBloodSprinklingOfTheNewCovenant.mp3
audioBytes: 35481164
---
