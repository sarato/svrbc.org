---
id: 2020-04-02-am
title: An Ode to God’s Sovereignty
date: 2020-04-02
text: Psalm 29
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200405-AM-AnOdeToGodsSovereignty.mp3
audioBytes: 45737167
youtube: ccdSquF_bz8
---
