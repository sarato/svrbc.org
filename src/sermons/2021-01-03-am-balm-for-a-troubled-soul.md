---
id: 2021-01-03-am
title: Balm for a Troubled Soul
date: 2021-01-03
text: Psalm 77
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210103-BalmForATroubledSoul.mp3
audioBytes: 61851942
youtube: K64rm-4kdD0
---
