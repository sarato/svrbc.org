---
id: 2011-07-24-am
title: What Jesus Believes
date: 2011-07-24
text: John 2:23–25
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-07-24+What+Jesus+Believes.mp3
audioBytes: 44729472
---
