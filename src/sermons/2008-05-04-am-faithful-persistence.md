---
id: 2008-05-04-am
title: Faithful Persistence
date: 2008-05-04
text: Mark 7:24–30
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-05-04+Faithful+Persistence.mp3
audioBytes: 51671775
---
