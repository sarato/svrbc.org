---
id: 2018-08-26-am
title: The Lord Will Provide
date: 2018-08-26
text: Ezra 5:1–11
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180826-AM-TheLordWillProvide.mp3
audioBytes: 47400486
---
