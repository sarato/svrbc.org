---
id: 2016-10-30-pm
title: Beatitudes Part 16
date: 2016-10-30
text: Matthew 6:10–11
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161030+Afternoon+Service+Beatitudes+Part+16.mp3
audioBytes: 20424165
youtube: KmEg6YVDOwg
---
