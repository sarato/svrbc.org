---
id: 2018-06-03-am
title: The Church’s One Foundation
date: 2018-06-03
text: Matthew 16:13–20
preacher: josh-sheldon
series: church-membership
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180603-AM-AWholeLotOfEstimatingGoingOn.mp3
audioBytes: 54303094
youtube: Xj8ut1AYh7o
---
