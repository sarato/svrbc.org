---
id: 2013-08-04-am
title: 'Philemon, A Little Letter with Lots of Gospel'
date: 2013-08-04
text: Philemon
preacher: matt-leighton
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-08-04+A+Little+Letter+With+Lots+of+Gospel.mp3
audioBytes: 42237567
---
