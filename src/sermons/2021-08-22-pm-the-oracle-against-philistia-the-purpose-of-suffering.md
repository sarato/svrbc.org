---
id: 2021-08-22-pm
title: 'The Oracle Against Philistia: The Purpose of Suffering'
date: 2021-08-22
text: Isaiah 14:28–32
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210822-TheOracleAgainstPhilistia-ThePurposeOfSuffering.aac
audioBytes: 27261843
youtube: juTyFVtZPM0
---
