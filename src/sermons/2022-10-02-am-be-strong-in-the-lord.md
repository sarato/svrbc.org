---
id: 2022-10-02-am
title: Be Strong in the Lord
date: 2022-10-02
text: Ephesians 6:10–12
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221002-BeStrongInTheLord.aac
audioBytes: 46946704
youtube: KRMKZ83CGwk
---
