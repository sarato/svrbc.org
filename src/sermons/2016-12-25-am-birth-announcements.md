---
id: 2016-12-25-am
title: Birth Announcements
date: 2016-12-25
text: Luke 1:57–2:14
preacher: josh-sheldon
series: advent-2016
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161225+-+Birth+Announcements.mp3
audioBytes: 44576339
youtube: _ekv2gXjlfg
---
