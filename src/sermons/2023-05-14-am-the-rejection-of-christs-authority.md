---
id: 2023-05-14-am
title: The Rejection of Christ’s Authority
date: 2023-05-14
text: Luke 4:23–37
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230514-TheRejectionOfChristsAuthority.aac
audioBytes: 52991006
youtube: 1onVHCGxYSk
---
