---
id: 2019-11-03-am
title: To Be Done With Sin
date: 2019-11-03
text: Matthew 5:29–30; 18:7–9
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191103-AM-ToBeDoneWithSin.mp3
audioBytes: 55315401
---
