---
id: 2019-09-01-am
title: No Condemnation For Those In Christ
date: 2019-09-01
text: Romans 8:1–4
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190901-AM-NoCondemnationForThoseInChrist.mp3
audioBytes: 49719339
---
