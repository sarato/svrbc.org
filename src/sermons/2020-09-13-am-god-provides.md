---
id: 2020-09-13-am
title: God Provides
date: 2020-09-13
text: Ruth 1:19–2:23
preacher: josh-sheldon
series: ruth
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/200913-GodProvides.mp3
audioBytes: 74257318
youtube: mNUVWbDl0Xo
---
