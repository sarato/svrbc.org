---
id: 2015-01-18-am
title: The Disposition of the Lord’s Body; A Key to Our Faith
date: 2015-01-18
text: John 19:31–42
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150118+The+Disposition+of+the+Lord's+Body-A+Key+to+Our+Faith.mp3
audioBytes: 39413599
---
