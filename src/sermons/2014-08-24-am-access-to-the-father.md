---
id: 2014-08-24-am
title: Access To The Father
date: 2014-08-24
text: John 16:23–28
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-08-24+Access+To+The+Father.mp3
audioBytes: 39293248
---
