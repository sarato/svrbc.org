---
id: 2016-06-19-am
title: His Word is Sweeter Than Honey
date: 2016-06-19
text: Psalm 19
preacher: steve-mixsell
series: gods-call-in-psalm-19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160619+His+Word+is+Sweeter+Than+Honey.mp3
audioBytes: 69463666
youtube: 8i9sGJIMsmw
---
