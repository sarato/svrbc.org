---
id: 2011-05-29-am
title: Treasures in Earthen Vessels
date: 2011-05-29
text: 2 Corinthians 4:7
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-05-29+Treasures+in+Earthen+Vessels.mp3
audioBytes: 57018240
---
