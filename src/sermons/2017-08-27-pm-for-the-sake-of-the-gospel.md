---
id: 2017-08-27-pm
title: For The Sake Of The Gospel
date: 2017-08-27
text: 1 Timothy 6:1–2
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170827+PM+Service+-+For+The+Sake+Of+The+Gospel.mp3
audioBytes: 26673392
youtube: MThiwNK2tw4
---
