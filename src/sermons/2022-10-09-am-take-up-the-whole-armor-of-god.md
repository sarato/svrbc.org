---
id: 2022-10-09-am
title: Take up the Whole Armor of God
date: 2022-10-09
text: Ephesians 6:13–18
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221009-TakeUpTheWholeArmorOfGod.aac
audioBytes: 43341853
youtube: KZSpkhzsWbo
---
