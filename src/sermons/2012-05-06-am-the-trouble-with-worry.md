---
id: 2012-05-06-am
title: The Trouble with Worry
date: 2012-05-06
text: Matthew 6:25–34
preacher: josh-sheldon
series: lessons-from-the-sermon-on-the-mount
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-05-06+The+Trouble+with+Worry.mp3
audioBytes: 34782024
---
