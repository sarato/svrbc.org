---
id: 2022-07-24-pm
title: Remembering the Lord
date: 2022-07-24
text: Isaiah 26:12–15
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220724-RememberingTheLord.aac
audioBytes: 31548739
youtube: pVRrBaIbA8g
---
