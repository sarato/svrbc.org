---
id: 2019-07-21-pm
title: The Lost Son
date: 2019-07-21
text: Luke 15
preacher: john-burchett
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/190721-PM-TheLostSon.mp3
audioBytes: 32840037
---
