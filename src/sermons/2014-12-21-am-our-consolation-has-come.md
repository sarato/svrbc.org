---
id: 2014-12-21-am
title: Our Consolation Has Come!
date: 2014-12-21
text: Luke 2:25–35
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141221+Our+Consolation+Has+Come.mp3
audioBytes: 43397667
---
