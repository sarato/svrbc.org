---
id: 2022-02-20-pm
title: 'The Oracle Against the Valley of Vision: The Spirit of Defeatism'
date: 2022-02-20
text: Isaiah 22:12–14
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220220-TheSpiritOfDefeatism.aac
audioBytes: 28434084
youtube: saKEX02qYbI
---
