---
id: 2018-06-10-am
title: What To Fear? No Discipline
date: 2018-06-10
text: Matthew 18:15–20
preacher: josh-sheldon
series: church-membership
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180610-AM-WhatToFear-NoDiscipline.mp3
audioBytes: 51405383
youtube: q4MaeV_7e1E
---
