---
id: 2022-11-06-pm
title: Self-Deceived About Sin
date: 2022-11-06
text: 1 John 1:8–10
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221106-Self-DeceivedAboutSin.aac
audioBytes: 32319056
youtube: 8q9bbBwEP_s
---
