---
id: 2021-11-07-pm
title: Christ-Centered Contribution
date: 2021-11-07
text: Luke 7:36–50
preacher: conley-owens
series: kingdom-investing
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211107-ChristCenteredContribution.aac
audioBytes: 39635383
youtube: AfyZRpJpi-4
---
