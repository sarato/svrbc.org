---
id: 2022-02-20-am
title: By the Will of God
date: 2022-02-20
text: Ephesians 1:1–2
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220220-ByTheWillOfGod.aac
audioBytes: 69360304
youtube: E4kkVB-vCqk
---
