---
id: 2019-11-17-pm
title: Creations Redemption
date: 2019-11-17
text: Psalm 8
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191117-PM-CreationsRedemption.mp3
audioBytes: 25904836
---
