---
id: 2009-10-25-am
title: Things Helpful Things Necessary
date: 2009-10-25
text: Acts 20:21
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-10-25+Things+Helpful+Things+Necessary.mp3
audioBytes: 37776084
---
