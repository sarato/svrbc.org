---
id: 2007-06-24-am
title: He Came To Bless
date: 2007-06-24
text: Acts 3:11–26
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-06-24+He+Came+To+Bless.mp3
audioBytes: 47423796
---
