---
id: 2009-09-06-am
title: 'Christians Aren’t Perfect, Just...'
date: 2009-09-06
text: Matthew 5:23–24; Mark 11:25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-09-06+Christians+Arent+Perfect,+Just.mp3
audioBytes: 40901082
---
