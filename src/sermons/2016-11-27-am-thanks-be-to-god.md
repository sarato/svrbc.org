---
id: 2016-11-27-am
title: Thanks Be To God
date: 2016-11-27
text: Psalm 136
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161127+Thanks+Be+To+God.mp3
audioBytes: 45740295
youtube: dPzRwoAOj7A
---
