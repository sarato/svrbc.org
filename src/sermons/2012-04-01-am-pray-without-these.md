---
id: 2012-04-01-am
title: Pray Without These
date: 2012-04-01
text: Matthew 6:1–15
preacher: josh-sheldon
series: lessons-from-the-sermon-on-the-mount
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-04-01+Pray+Without+These.mp3
audioBytes: 44533986
---
