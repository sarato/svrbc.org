---
id: 2013-03-31-am
title: 'Christ is Risen, He is Risen Indeed'
date: 2013-03-31
text: Luke 24:6
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-03-31+Christ+is+risen+-+He+is+resin+indeed.mp3
audioBytes: 47045160
---
