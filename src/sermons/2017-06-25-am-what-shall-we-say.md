---
id: 2017-06-25-am
title: What Shall We Say
date: 2017-06-25
text: Romans 8:31–34
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170625+What+Shall+We+Say.mp3
audioBytes: 57641324
youtube: EnpDdXfvHcE
---
