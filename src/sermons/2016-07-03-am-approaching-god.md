---
id: 2016-07-03-am
title: Approaching God
date: 2016-07-03
text: Leviticus 10:1–11
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160703+Approaching+God.mp3
audioBytes: 51980842
youtube: c382w5ss2D0
---
