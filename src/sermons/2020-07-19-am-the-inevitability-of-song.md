---
id: 2020-07-19-am
title: The Inevitability Of Song
date: 2020-07-19
text: Luke 19:37–40
preacher: conley-owens
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200719-TheInevitabilityOfSong.mp3
audioBytes: 58969905
youtube: lTAIR5kLhTQ
---
