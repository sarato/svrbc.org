---
id: 2012-01-15-am
title: The Danger of Religion
date: 2012-01-15
text: John 5:31–47
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-01-15+The+Danger+of+Religion.mp3
audioBytes: 37839468
---
