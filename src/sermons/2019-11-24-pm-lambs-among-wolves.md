---
id: 2019-11-24-pm
title: Lambs Among Wolves
date: 2019-11-24
text: Luke 10:1–12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191124-PM-DancesWithWolves.mp3
audioBytes: 35200592
---
