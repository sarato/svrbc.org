---
id: 2018-11-11-am
title: We’re All In This Together
date: 2018-11-11
text: Ezra 9:1–5
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181111-AM-WereAllInThisTogether.mp3
audioBytes: 47615738
---
