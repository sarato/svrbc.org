---
id: 2013-11-03-am
title: Heavenly vs Earthly Wisdom part 3
date: 2013-11-03
text: James 4:1–10
preacher: steve-mixsell
series: james
audio: https://storage.googleapis.com/pbc-ca-sermons/2013/2013-11-03+Heavenly+vs.mp3
audioBytes: 55087368
---
