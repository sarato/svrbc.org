---
id: 2016-05-29-am
title: Rejoice! God’s Enemies Have Been Scattered
date: 2016-05-29
text: 2 Kings 9:14–10:28
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160529+Rejoice+Gods+Enemies+Have+Been+Scattered.mp3
audioBytes: 59380837
youtube: 5oLeFcEU8sM
---
