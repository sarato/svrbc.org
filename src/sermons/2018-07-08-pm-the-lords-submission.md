---
id: 2018-07-08-pm
title: The Lord’s Submission
date: 2018-07-08
text: Matthew 26:39
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180708-PM-TheLordsSubmission.mp3
audioBytes: 21189851
youtube: vPk7fqFgzJE
---
