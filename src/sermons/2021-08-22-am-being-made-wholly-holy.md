---
id: 2021-08-22-am
title: Being Made Wholly Holy
date: 2021-08-22
text: 1 Thessalonians 5:23–24
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210822-BeingMadeWhollyHoly.aac
audioBytes: 48949330
youtube: NKlbmWI5zDI
---
