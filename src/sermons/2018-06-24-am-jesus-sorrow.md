---
id: 2018-06-24-am
title: Jesus’ Sorrow
date: 2018-06-24
text: Matthew 26:36–37
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180624-AM-JesusSorrow.mp3
audioBytes: 48758851
youtube: oRHLAGkYQbU
---
