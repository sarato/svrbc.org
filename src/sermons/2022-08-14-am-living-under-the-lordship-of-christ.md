---
id: 2022-08-14-am
title: Living Under the Lordship of Christ
date: 2022-08-14
text: Ephesians 5:15–21
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220814-LivingUnderTheLordshipOfChrist.aac
audioBytes: 51455723
youtube: zEJnD6DevBI
---
