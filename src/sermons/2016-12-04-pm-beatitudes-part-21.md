---
id: 2016-12-04-pm
title: Beatitudes Part 21
date: 2016-12-04
text: Matthew 7:1–5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161127+Afternoon+Service+-+Beatitudes+Part+20.mp3
audioBytes: 23758643
youtube: KeGiyabRNvk
---
