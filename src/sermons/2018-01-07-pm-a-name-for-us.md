---
id: 2018-01-07-pm
title: A Name For Us
date: 2018-01-07
text: John 17:11–19
preacher: josh-sheldon
series: the-road-to-the-cross
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180107-PM-ANameForUs.mp3
audioBytes: 31069992
youtube: OilOAXPXQYI
---
