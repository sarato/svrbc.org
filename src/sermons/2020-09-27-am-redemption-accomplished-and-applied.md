---
id: 2020-09-27-am
title: 'Redemption, Accomplished & Applied'
date: 2020-09-27
text: Ruth 4
preacher: josh-sheldon
series: ruth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200927-RedemptionAcoomplished%26Applied.mp3
audioBytes: 83726463
youtube: 9LuHCUc20Rs
---
