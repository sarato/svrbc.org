---
id: 2015-04-12-am
title: To Be Assured of Salvation
date: 2015-04-12
text: 1 John 5:13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150412+To+Be+Assured+of+Salvation.mp3
audioBytes: 41905111
---
