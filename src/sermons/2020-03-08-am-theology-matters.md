---
id: 2020-03-08-am
title: Theology Matters
date: 2020-03-08
text: Philippians 2:5–11
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200308-AM-TheologyMatters.mp3
audioBytes: 51970878
youtube: 7R7wLyhGk-Y
---
