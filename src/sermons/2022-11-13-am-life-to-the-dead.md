---
id: 2022-11-13-am
title: Life to the Dead
date: 2022-11-13
text: Exodus 3:1–14
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/221113-LifeToTheDead.aac
audioBytes: 45069784
youtube: UMV7FjLPi9k
---
