---
id: 2023-04-09-pm
title: The Immanent Name of the Lord
date: 2023-04-09
text: Isaiah 30:27–28
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230409-TheImmanentNameOfTheLord-v2.aac
audioBytes: 26347733
youtube: B3ikEGgR7sw
---
