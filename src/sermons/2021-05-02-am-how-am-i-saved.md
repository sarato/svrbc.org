---
id: 2021-05-02-am
title: How Am I Saved?
date: 2021-05-02
text: Isaiah 53:10–12
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210502-HowAmISaved.aac
audioBytes: 53881493
youtube: 64Tkz0cVAyg
---
