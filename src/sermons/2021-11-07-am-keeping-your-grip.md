---
id: 2021-11-07-am
title: Keeping Your Grip
date: 2021-11-07
text: 2 Thessalonians 2:15
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211107-KeepingYourGrip.aac
audioBytes: 31982291
youtube: 4WLAqE52n44
---
