---
id: 2018-07-29-am
title: The Role Of Repentance
date: 2018-07-29
text: 2 Corinthians 7:5–13
preacher: matt-leighton
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180729-AM-TheRoleOfRepentance.mp3
audioBytes: 44900692
youtube: lGl87u5MoD0
---
