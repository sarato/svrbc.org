---
id: 2013-02-10-am
title: The Times Are in God’s Hands
date: 2013-02-10
text: Ecclesiastes 3:1–8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-02-10+The+Times+Are+In+God%27s+Hands.mp3
audioBytes: 58974696
---
