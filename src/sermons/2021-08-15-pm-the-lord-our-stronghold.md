---
id: 2021-08-15-pm
title: 'The Lord, Our Stronghold'
date: 2021-08-15
text: Psalm 91
preacher: josh-sheldon
unregisteredSeries: 'The Lord, Our Stronghold'
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210815-TheLordOurStronghold.aac
audioBytes: 49020057
youtube: XdPtwJeyKFY
---
