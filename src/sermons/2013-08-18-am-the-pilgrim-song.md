---
id: 2013-08-18-am
title: The Pilgrim Song
date: 2013-08-18
text: Psalm 120
preacher: josh-sheldon
series: psalms-of-ascents
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-08-18+The+Pilgrim+Song.mp3
audioBytes: 37126815
---
