---
id: 2018-07-15-pm
title: A Look Forward
date: 2018-07-15
text: Zephaniah 2–3
preacher: josh-sheldon
series: minor-prophets
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180715-PM-ALookForward.mp3
audioBytes: 33621622
youtube: sHOsgIcjWQE
---
