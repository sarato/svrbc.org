---
id: 2021-04-18-am
title: A Sin Too Far
date: 2021-04-18
text: 1 Thessalonians 4:1–6
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210418-ASinTooFar.mp3
audioBytes: 76492844
youtube: YtynyTpJ4MQ
---
