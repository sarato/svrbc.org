---
id: 2018-12-09-am
title: Our Good Shepherd
date: 2018-12-09
text: John 10:1–8
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181209-AM-OurGoodShepherd.mp3
audioBytes: 37293415
---
