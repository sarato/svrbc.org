---
id: 2017-05-14-am
title: To Be Pleasing To God
date: 2017-05-14
text: Romans 8:9–11
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170514+To+Be+Pleasing+To+God.mp3
audioBytes: 47108336
youtube: eI-7p9QyXr8
---
