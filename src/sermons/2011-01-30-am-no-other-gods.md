---
id: 2011-01-30-am
title: No Other Gods
date: 2011-01-30
text: Exodus 20:3
preacher: josh-sheldon
series: the-ten-commandments
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-01-30+No+Other+Gods.mp3
audioBytes: 41806080
---
