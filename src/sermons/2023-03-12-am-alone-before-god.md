---
id: 2023-03-12-am
title: Alone Before God
date: 2023-03-12
text: Jeremiah 25:1–29
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230312-AloneBeforeGod.aac
audioBytes: 43981427
youtube: jUvbrx2OkYk
---
