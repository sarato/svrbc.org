---
id: 2015-09-13-am
title: King David Foretells of Christ
date: 2015-09-13
text: 1 Samuel 30:24
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150913+King+David+Foretells+of+Christ.mp3
audioBytes: 44108178
youtube: FYJuWs1OPtM
---
