---
id: 2016-01-24-am
title: The Temple in Israel
date: 2016-01-24
text: 2 Kings 4:8–37
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160124+The+Temple+in+Israel.mp3
audioBytes: 58478859
youtube: jO06ueo9lRk
---
