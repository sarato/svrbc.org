---
id: 2009-05-17-am
title: They Pursued Them
date: 2009-05-17
text: Acts 14:19
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-05-17+They+Pursued+Them.mp3
audioBytes: 47461743
---
