---
id: 2019-06-30-am
title: God’s Entreaty To You
date: 2019-06-30
text: 2 Corinthians 5:16–21
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190630-AM-GodsEntreatyToYou.mp3
audioBytes: 46133671
---
