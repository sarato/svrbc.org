---
id: 2017-11-05-pm
title: The True Vine Produces True Fruit
date: 2017-11-05
text: John 15:1–11
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171105+PM+Service+-+The+True+Vine+Produces+True+Fruit.mp3
audioBytes: 32563313
youtube: hw21noBwoU0
---
