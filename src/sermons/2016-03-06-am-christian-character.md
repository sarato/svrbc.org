---
id: 2016-03-06-am
title: Christian Character
date: 2016-03-06
text: Ephesians 4:32
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160306+Christian+Character.mp3
audioBytes: 45025170
youtube: naqvgz8jNTI
---
