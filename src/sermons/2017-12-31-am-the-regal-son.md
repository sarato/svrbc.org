---
id: 2017-12-31-am
title: The Regal Son
date: 2017-12-31
text: Hebrews 1:5–14
preacher: conley-owens
series: hebrews
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/171231-AM-TheRegalSon.mp3
audioBytes: 42786210
youtube: BKeKmcXm0PU
---
