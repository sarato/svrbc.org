---
id: 2017-11-12-pm
title: 'As To The Father, So Also To The Son'
date: 2017-11-12
text: John 15:18–16:4
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171112+PM+Service+-+As+To+The+Father+So+Also+To+The+Son.mp3
audioBytes: 31378891
youtube: VtggG-dCfZY
---
