---
id: 2016-05-22-am
title: A Living Sacrifice
date: 2016-05-22
text: Romans 12
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160522+A+Living+Sacrifice.mp3
audioBytes: 41021119
youtube: PoqnXCC8Q90
---
