---
id: 2023-03-05-am
title: 'The Gospel of Luke: The Coming Baptism of Fire'
date: 2023-03-05
text: Luke 3:15–22
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230305-TheComingBaptismOfFire.aac
audioBytes: 47684931
youtube: jdEBi4nAwzU
---
