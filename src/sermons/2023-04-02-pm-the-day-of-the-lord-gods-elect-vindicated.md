---
id: 2023-04-02-pm
title: The Day Of The Lord - God’s Elect Vindicated
date: 2023-04-02
text: Obadiah 15–21
preacher: josh-sheldon
unregisteredSeries: Obadiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230402-TheDayOfTheLord-GodsElectVindicated.aac
audioBytes: 31378522
youtube: 2WcPjm6pSXM
---
