---
id: 2021-11-28-am
title: The Lord Our Guard
date: 2021-11-28
text: 2 Thessalonians 3:3–5
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211128-TheLordOurGuard.aac
audioBytes: 30973303
youtube: FRwZE5d8pHY
---
