---
id: 2022-10-16-am
title: Take up the Whole Armor of God Part 2
date: 2022-10-16
text: Ephesians 6:16–20
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221016-TakeUpTheWholeArmorOfGodPart2.aac
audioBytes: 49528696
youtube: KPUVfDewNro
---
