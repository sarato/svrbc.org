---
id: 2021-09-05-am
title: Evidence of God’s Righteousness
date: 2021-09-05
text: 2 Thessalonians 1:1–10
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210905-EvidenceOfGodsRighteousness.aac
audioBytes: 49220831
youtube: 5-U_tz2k_ZI
---
