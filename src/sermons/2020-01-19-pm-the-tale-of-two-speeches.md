---
id: 2020-01-19-pm
title: The Tale Of Two Speeches
date: 2020-01-19
text: Psalm 12
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200119-PM-TheTaleOfTwoSpeeches.mp3
audioBytes: 36827369
---
