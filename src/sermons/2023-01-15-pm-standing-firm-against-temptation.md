---
id: 2023-01-15-pm
title: Standing Firm Against Temptation
date: 2023-01-15
text: 1 Corinthians 10:6–14
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230115-StandingFirmAgainstTemptation.aac
audioBytes: 32173583
youtube: CeDBtozMzRU
---
