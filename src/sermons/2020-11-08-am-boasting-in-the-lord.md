---
id: 2020-11-08-am
title: Boasting in the Lord
date: 2020-11-08
text: 1 Corinthians 1:26–31
preacher: josh-sheldon
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201108-BoastingInTheLord.mp3
audioBytes: 61826238
youtube: q8V6gke-gW4
---
