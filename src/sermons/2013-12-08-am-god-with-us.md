---
id: 2013-12-08-am
title: God With Us
date: 2013-12-08
text: Matthew 1:22–23
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2013/2013-12-08+God+With+Us.mp3
audioBytes: 44283315
---
