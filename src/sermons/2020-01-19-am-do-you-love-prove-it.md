---
id: 2020-01-19-am
title: Do You Love? Prove It!
date: 2020-01-19
text: Philippians 1:7–11
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200119-AM-DoYouLoveProveIt.mp3
audioBytes: 50301973
---
