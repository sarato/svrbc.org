---
id: 2016-01-03-am
title: First Resolution
date: 2016-01-03
text: Philippians 1:6
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160103+First+Resolution.mp3
audioBytes: 37328448
youtube: WlcbsJi-9Mc
---
