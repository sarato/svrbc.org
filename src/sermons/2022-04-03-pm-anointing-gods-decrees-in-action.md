---
id: 2022-04-03-pm
title: Anointing - God’s Decrees in Action
date: 2022-04-03
text: 1 Samuel 16:12
preacher: josh-sheldon
series: the-life-of-david
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220403-AnointingGodsDecreesinAction.aac
audioBytes: 41704668
youtube: lhptyHlLiOE
---
