---
id: 2009-02-08-am
title: The Promised Rest
date: 2009-02-08
text: Matthew 11:28–30
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-02-08+The+Promised+Rest.mp3
audioBytes: 63332346
---
