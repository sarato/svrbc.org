---
id: 2015-05-17-am
title: The Vanity of a Life Lived Apart from Christ
date: 2015-05-17
text: Ecclesiastes 1:1–11
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150517+The+Vanity+of+a+Life+Lived+Apart+From+Christ.mp3
audioBytes: 41154892
---
