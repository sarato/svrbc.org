---
id: 2022-10-02-pm
title: The Crown of Israel
date: 2022-10-02
text: Isaiah 28:1–6
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221002-TheCrownOfIsrael.aac
audioBytes: 26843627
youtube: 3v0G94Xqvzo
---
