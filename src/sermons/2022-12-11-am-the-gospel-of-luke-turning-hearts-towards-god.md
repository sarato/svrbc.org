---
id: 2022-12-11-am
title: 'The Gospel of Luke: Turning Hearts Towards God'
date: 2022-12-11
text: Luke 1:13–17
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221211-TheGospelOfLuke-TurningHeartsTowardsGod.aac
audioBytes: 46196511
youtube: 6V5FjKY-WrI
---
