---
id: 2007-01-28-am
title: The Necessity of Urgent Prayer
date: 2007-01-28
text: James 5:16
preacher: mike-mccullough
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-01-28+The+Necessity+of+Urgent+Prayer.mp3
audioBytes: 34204479
---
