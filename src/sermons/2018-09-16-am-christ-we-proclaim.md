---
id: 2018-09-16-am
title: Christ We Proclaim
date: 2018-09-16
text: Colossians 1:28–29
preacher: chris-kiagiri
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180916-AM-ChristWeProclaim.mp3
audioBytes: 52502088
---
