---
id: 2016-08-14-am
title: A Hymn to Christ
date: 2016-08-14
text: Colossians 1:13–23
preacher: josh-sheldon
series: hymns-of-scripture
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160814+A+Hymn+to+Christ.mp3
audioBytes: 43386349
youtube: rlODzQxLMkg
---
