---
id: 2007-03-25-am
title: The First Christian Sermon
date: 2007-03-25
text: Acts 2:14–41
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-03-25+The+First+Christain+Sermon.mp3
audioBytes: 54282612
---
