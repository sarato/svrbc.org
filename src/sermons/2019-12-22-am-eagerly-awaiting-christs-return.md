---
id: 2019-12-22-am
title: Eagerly Awaiting Christ’s Return
date: 2019-12-22
text: Hebrews 9:27–28
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191222-AM-EagerlyAwaitingChristsReturn.mp3
audioBytes: 43224256
---
