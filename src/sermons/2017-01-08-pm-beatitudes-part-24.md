---
id: 2017-01-08-pm
title: Beatitudes Part 24
date: 2017-01-08
text: Matthew 7:16
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170108+Afternoon+Service+-+Beatitudes+Part+24.mp3
audioBytes: 28138802
youtube: HjQ-mAKGYWA
---
