---
id: 2017-01-29-am
title: In Defense Of God
date: 2017-01-29
text: Romans 3:1–20
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170129+In+Defence+of+God.mp3
audioBytes: 45882040
youtube: XInd5GNJves
---
