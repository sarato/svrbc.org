---
id: 2023-07-09-am
title: Answering The Call Of The Beatitudes
date: 2023-07-09
text: Luke 6:12–23
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230709-AnsweringTheCallOfTheBeatitudes.aac
audioBytes: 49135490
youtube: buWi_5TZ2Mg
---
