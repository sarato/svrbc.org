---
id: 2008-04-13-am
title: A New Heart
date: 2008-04-13
text: Matthew 15:1–20
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2008/2008-04-13+A+New+Heart.mp3
audioBytes: 43069482
---
