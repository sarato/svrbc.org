---
id: 2021-05-16-am
title: Love as Taught by God
date: 2021-05-16
text: 1 Thessalonians 4:9–10
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210516-LoveAsTaughtByGod.aac
audioBytes: 47714820
youtube: wz0GxxUs1Fs
---
