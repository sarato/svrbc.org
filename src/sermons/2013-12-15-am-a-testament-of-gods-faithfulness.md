---
id: 2013-12-15-am
title: A Testament of God’s Faithfulness
date: 2013-12-15
text: Ezra 2
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-12-15+A+Testament+of+God%27s+Faithfulness.mp3
audioBytes: 40613352
---
