---
id: 2019-10-06-pm
title: Morning Prayer
date: 2019-10-06
text: Psalm 5
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191006-PM-MorningPrayer.mp3
audioBytes: 27634763
---
