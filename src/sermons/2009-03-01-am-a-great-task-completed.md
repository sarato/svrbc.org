---
id: 2009-03-01-am
title: A Great Task Completed
date: 2009-03-01
text: Philippians 1:6
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-03-01+A+Great+Task+Completed.mp3
audioBytes: 53631258
---
