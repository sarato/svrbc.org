---
id: 2022-02-13-pm
title: 'Full Assurance - Hebrews 10:19-39'
date: 2022-02-13
text: Hebrews 10:19–39
preacher: brian-garcia
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220213-FullAssurance.aac
audioBytes: 34488940
youtube: eb3I6dGmnps
---
