---
id: 2008-10-05-am
title: But Jehoshaphat Cried Out...
date: 2008-10-05
text: 2 Chronicles 18
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-10-05+But+Jehoshaphat+Cried+Out.mp3
audioBytes: 58379220
---
