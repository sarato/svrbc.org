---
id: 2017-06-18-am
title: God’s Purpose
date: 2017-06-18
text: Romans 8:28–30
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/170618+Gods+Purpose.mp3
audioBytes: 58119879
youtube: _oJJHu2vONE
---
