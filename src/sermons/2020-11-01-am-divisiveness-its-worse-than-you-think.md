---
id: 2020-11-01-am
title: 'Divisiveness, It’s Worse Than You Think'
date: 2020-11-01
text: 1 Corinthians 1:21–25
preacher: josh-sheldon
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201101-DivisivenessItsWorseThanYouThink.mp3
audioBytes: 66673729
youtube: '-qOgxxTpdMw'
---
