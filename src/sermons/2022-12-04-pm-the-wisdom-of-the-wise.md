---
id: 2022-12-04-pm
title: The Wisdom of the Wise
date: 2022-12-04
text: Isaiah 29:13–14
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221204-TheWisdomoftheWise.aac
audioBytes: 34489717
youtube: DWP57KBHpjs
---
