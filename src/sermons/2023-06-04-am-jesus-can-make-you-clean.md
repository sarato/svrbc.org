---
id: 2023-06-04-am
title: Jesus Can Make You Clean
date: 2023-06-04
text: Luke 5:12–26
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230604-JesusCanMakeYouClean.aac
audioBytes: 45017681
youtube: auW5LSzr1FM
---
