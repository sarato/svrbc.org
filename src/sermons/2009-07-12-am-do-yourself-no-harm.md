---
id: 2009-07-12-am
title: Do Yourself No Harm
date: 2009-07-12
text: Acts 16:19–34
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-07-12+Do+Yourself+No+Harm.mp3
audioBytes: 47246154
---
