---
id: 2022-05-01-am
title: A Holy Temple in the Lord
date: 2022-05-01
text: Ephesians 2:17–22
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220501-AHolyTempleintheLord.aac
audioBytes: 51772956
youtube: uz7BDuvhLQs
---
