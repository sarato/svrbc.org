---
id: 2022-09-04-pm
title: Hiding from the Fury
date: 2022-09-04
text: Isaiah 26:20–21
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220904-HidingFromTheFury.aac
audioBytes: 32875978
youtube: SEcxvxG-9l4
---
