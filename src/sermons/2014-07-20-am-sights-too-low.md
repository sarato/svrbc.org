---
id: 2014-07-20-am
title: Sights Too Low
date: 2014-07-20
text: John 15:9–17
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-07-20+Sights+Too+Low.mp3
audioBytes: 52744299
---
