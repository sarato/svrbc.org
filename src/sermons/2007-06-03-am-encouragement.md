---
id: 2007-06-03-am
title: Encouragement
date: 2007-06-03
text: Acts 15:36–41
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-06-03+Encouragement.mp3
audioBytes: 37280271
---
