---
id: 2014-02-09-am
title: The Tolerant Church
date: 2014-02-09
text: Revelation 2:18–29
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-02-09+The+Tolerant+Church.mp3
audioBytes: 60560130
---
