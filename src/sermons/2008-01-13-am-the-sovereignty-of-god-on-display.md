---
id: 2008-01-13-am
title: The Sovereignty of God on Display
date: 2008-01-13
text: Acts 8:1–8
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-01-13+The+Sovereignty+of+God+on+Display.mp3
audioBytes: 48633930
---
