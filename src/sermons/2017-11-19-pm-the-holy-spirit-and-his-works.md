---
id: 2017-11-19-pm
title: The Holy Spirit and His Works
date: 2017-11-19
text: John 16:4–15
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171119+PM+Service+-+The+Holy+Spirit+and+His+Works.mp3
audioBytes: 26514669
youtube: mEX5pOxGyZk
---
