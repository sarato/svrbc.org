---
id: 2014-12-28-am
title: 'New Year, A Call To Arms'
date: 2014-12-28
text: 2 Chronicles 34
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141228+New+Year+A+CallTo+Arms.mp3
audioBytes: 45110827
---
