---
id: 2018-07-22-pm
title: Strengthened To Suffer
date: 2018-07-22
text: Luke 22:43
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180722-PM-StrengthenedToSuffer.mp3
audioBytes: 17329157
youtube: 3tQn31W7EN0
---
