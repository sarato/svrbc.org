---
id: 2019-09-01-pm
title: In Remembrance Of Him
date: 2019-09-01
text: 1 Corinthians 11:23–26
preacher: josh-sheldon
series: the-lords-table
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190901-PM-InRemembranceOfHim.mp3
audioBytes: 29408605
---
