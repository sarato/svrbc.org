---
id: 2021-01-31-am
title: Waiting for the Son
date: 2021-01-31
text: 1 Thessalonians 1:10
preacher: conley-owens
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210131-WaitingForTheSon.mp3
audioBytes: 56211374
youtube: paogzzieKxg
---
