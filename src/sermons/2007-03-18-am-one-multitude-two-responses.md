---
id: 2007-03-18-am
title: 'One Multitude, Two Responses'
date: 2007-03-18
text: Acts 2:1–13
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-03-18+One+Multitude,+Two+Responses.mp3
audioBytes: 44592783
---
