---
id: 2015-08-30-am
title: Commissioning Our Deacons
date: 2015-08-30
text: Acts 6:1–7
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150830+Commissioning+Our+Deacons.mp3
audioBytes: 35272954
youtube: Wrg2qOv5s7k
---
