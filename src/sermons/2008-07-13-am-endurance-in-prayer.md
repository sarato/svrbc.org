---
id: 2008-07-13-am
title: Endurance in Prayer
date: 2008-07-13
text: Acts 12
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-07-13+Endurance+in+Prayer.mp3
audioBytes: 40247226
---
