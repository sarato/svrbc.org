---
id: 2015-04-26-am
title: A Return To Sanity
date: 2015-04-26
text: Psalm 73
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150426+A+Return+to+Sanity.mp3
audioBytes: 44845028
---
