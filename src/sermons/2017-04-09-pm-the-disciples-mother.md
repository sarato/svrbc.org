---
id: 2017-04-09-pm
title: The Disciple’s Mother
date: 2017-04-09
text: John 19:23–27
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170409+PM+Srv+-+The+DIsciples+Mother.mp3
audioBytes: 19643820
youtube: 6K2pbhUuQd0
---
