---
id: 2019-03-10-am
title: God’s Honor First
date: 2019-03-10
text: Nehemiah 5:1–13
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190310-AM-GodsHonorFirst.mp3
audioBytes: 51559601
---
