---
id: 2018-06-03-pm
title: A Confident Faith
date: 2018-06-03
text: Habakkuk 3
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180603-PM-AConfidentFaith.mp3
audioBytes: 35843490
youtube: IqTffClauH4
---
