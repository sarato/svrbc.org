---
id: 2016-04-03-am
title: God’s Call By The Psalms Part 2
date: 2016-04-03
text: Psalm 19
preacher: steve-mixsell
series: gods-call-in-psalm-19
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160403+Psalm+19+Pt+2.mp3
audioBytes: 48824825
youtube: qjNjYnjC5Hg
---
