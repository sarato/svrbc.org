---
id: 2020-12-20-am
title: On Earth as it is in Heaven
date: 2020-12-20
text: Psalm 133
preacher: josh-sheldon
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201220-OnEarthAsItIsInHeaven.mp3
audioBytes: 71853497
youtube: 7kQcGAb5lSY
---
