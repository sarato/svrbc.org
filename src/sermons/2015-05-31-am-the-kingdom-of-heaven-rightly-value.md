---
id: 2015-05-31-am
title: The Kingdom of Heaven Rightly Value
date: 2015-05-31
text: Matthew 13:44–46
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150531+The+Kingdom+of+Heaven+Rightly+Valuee.mp3
audioBytes: 47228925
---
