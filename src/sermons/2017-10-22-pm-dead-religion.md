---
id: 2017-10-22-pm
title: Dead Religion
date: 2017-10-22
text: Isaiah 1:10–17
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171022+PM+Service+-+Dead+Religion.mp3
audioBytes: 32487994
youtube: Mh0iFYBoL_8
---
