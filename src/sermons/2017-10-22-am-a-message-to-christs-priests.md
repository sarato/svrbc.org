---
id: 2017-10-22-am
title: A Message to Christ’s Priests
date: 2017-10-22
text: Hebrews 5:1–10
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171022+A+Message+to+Christs+Priests.mp3
audioBytes: 60727128
youtube: ouug4XVTb2Y
---
