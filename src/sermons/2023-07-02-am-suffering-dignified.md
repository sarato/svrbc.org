---
id: 2023-07-02-am
title: 'Suffering, Dignified'
date: 2023-07-02
text: Hebrews 2:10–13
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230702-SufferingDignified.aac
audioBytes: 53223890
youtube: cNOV29qMAzw
---
