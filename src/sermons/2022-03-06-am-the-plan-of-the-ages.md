---
id: 2022-03-06-am
title: The Plan of The Ages
date: 2022-03-06
text: Ephesians 1:1–10
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220306-ThePlanofTheAges.aac
audioBytes: 48631520
youtube: 80Kvo2SvyEc
---
