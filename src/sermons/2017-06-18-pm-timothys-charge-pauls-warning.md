---
id: 2017-06-18-pm
title: 'Timothy’s Charge, Paul’s Warning'
date: 2017-06-18
text: 1 Timothy 1:18–20
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170618+PM+Service+Timothys+Charge+-+Pauls+Warning.mp3
audioBytes: 30367843
youtube: RT90POYBNx0
---
