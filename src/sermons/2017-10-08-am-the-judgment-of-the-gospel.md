---
id: 2017-10-08-am
title: The Judgment of the Gospel
date: 2017-10-08
text: Romans 10:14–21
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/20171008+The+Judgment+of+the+Gospel.mp3
audioBytes: 47913328
youtube: pO3NnOiwrZo
---
