---
id: 2015-08-02-am
title: The Perseverance of the Saints
date: 2015-08-02
text: Colossians 1:19–23
preacher: josh-sheldon
series: the-doctrines-of-grace
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150802+The+Perseverance+of+the+Saints.mp3
audioBytes: 49206862
youtube: tZWwk59_OcE
---
