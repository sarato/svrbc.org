---
id: 2007-05-06-am
title: We Also Have It
date: 2007-05-06
text: Acts 2:42–47
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-05-06We+Also+Have+It.mp3
audioBytes: 46380879
---
