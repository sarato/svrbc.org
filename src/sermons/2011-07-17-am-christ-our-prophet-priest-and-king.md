---
id: 2011-07-17-am
title: Christ Our Prophet Priest and King
date: 2011-07-17
text: John 2:13–22
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-07-17+Christ+Our+Prophet+Priest+and+King.mp3
audioBytes: 37516416
---
