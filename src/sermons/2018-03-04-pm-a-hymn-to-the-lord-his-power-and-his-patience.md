---
id: 2018-03-04-pm
title: A Hymn to the Lord – His Power and His Patience
date: 2018-03-04
text: Nahum 1:1–8
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180304-PM-AHymntotheLord-HisPowerAndHisPatience.mp3
audioBytes: 27274117
youtube: 2jsrAzgXIno
---
