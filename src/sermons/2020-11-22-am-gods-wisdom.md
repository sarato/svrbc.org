---
id: 2020-11-22-am
title: God’s Wisdom
date: 2020-11-22
text: 1 Corinthians 2:6–10
preacher: josh-sheldon
series: kingdom-community
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/201122-GodsWisdom.mp3
audioBytes: 65522669
youtube: EbEtHQFRBaw
---
