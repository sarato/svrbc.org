---
id: 2012-12-09-am
title: Denying Yourself
date: 2012-12-09
text: Luke 9:23–26
preacher: jesse-lu
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-12-09+Denying+Yourself.mp3
audioBytes: 60043884
---
