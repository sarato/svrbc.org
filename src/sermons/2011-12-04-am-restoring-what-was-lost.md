---
id: 2011-12-04-am
title: Restoring What Was Lost
date: 2011-12-04
text: Joel 2:25
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-12-04+Restoring+What+Was+Lost.mp3
audioBytes: 29685867
---
