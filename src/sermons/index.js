module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'sermons/*.md',
    typeName: 'Sermon',
    refs: {
      preacher: 'Preacher',
      series: 'SermonSeries'
    }
  }
}
