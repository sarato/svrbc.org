---
id: 2022-12-04-am
title: 'The Gospel of Luke: The Promise'
date: 2022-12-04
text: Luke 1:1–13
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221204-TheGospelOfLuke-ThePromise.aac
audioBytes: 50026545
youtube: nsxxVTGr2JQ
---
