---
id: 2016-07-17-pm
title: Beatitudes Part 2
date: 2016-07-17
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160717+Beatitudes+Part+2.mp3
audioBytes: 34220505
youtube: tuYOwnegk5M
---
