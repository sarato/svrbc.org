---
id: 2019-04-21-pm
title: 'Our Resurrection: Our Clear and Present Reality'
date: 2019-04-21
text: Ephesians 1:15–23
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190421-PM-OurResurrection-OurClearAndPresentReality.mp3
audioBytes: 27557075
---
