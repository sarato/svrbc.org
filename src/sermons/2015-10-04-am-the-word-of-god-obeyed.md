---
id: 2015-10-04-am
title: The Word Of God Obeyed
date: 2015-10-04
text: 1 Kings 18:1–40
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151004-TheWordOfGodObeyed.mp3
audioBytes: 63176722
youtube: rArDj7pKVCw
---
