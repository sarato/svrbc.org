---
id: 2009-06-28-am
title: 'Two Women, Two Gospels'
date: 2009-06-28
text: Acts 16:16–18
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-06-28+Two+Women,+Two+Gospels.mp3
audioBytes: 57972228
---
