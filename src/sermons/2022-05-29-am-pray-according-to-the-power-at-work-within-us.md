---
id: 2022-05-29-am
title: Pray According To The Power At Work Within Us
date: 2022-05-29
text: Ephesians 3:20–21
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220529-PrayAccordingToThePowerAtWorkWithinUs.aac
audioBytes: 48197705
youtube: NOkyQ0DUg4k
---
