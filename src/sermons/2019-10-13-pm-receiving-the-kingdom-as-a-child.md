---
id: 2019-10-13-pm
title: Receiving the Kingdom As A Child
date: 2019-10-13
text: Luke 18:15–17
preacher: john-burchett
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191013-PM-ReceivingTheKingdomAsAChild.mp3
audioBytes: 45168183
---
