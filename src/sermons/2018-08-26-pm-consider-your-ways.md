---
id: 2018-08-26-pm
title: Consider Your Ways
date: 2018-08-26
text: Haggai 1:1–11
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180826-PM-ConsiderYourWays.mp3
audioBytes: 20145786
---
