---
id: 2023-04-16-pm
title: Musical Victory
date: 2023-04-16
text: Isaiah 30:29–32
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230416-MusicalVictory.aac
audioBytes: 34791859
youtube: WTW4Ue3gLu8
---
