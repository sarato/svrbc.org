---
id: 2023-01-29-am
title: 'The Gospel of Luke: Christ the Lord'
date: 2023-01-29
text: Luke 2:8–21
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230129-TheGospelOfLuke-ChristTheLord.aac
audioBytes: 49987921
youtube: ITEwB29qH14
---
