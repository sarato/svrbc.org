---
id: 2020-07-05-am
title: Be Anxious For Nothing
date: 2020-07-05
text: Philippians 4:6
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200705-BeAnxiousForNothing.mp3
audioBytes: 65174091
youtube: ikWOVo48wFY
---
