---
id: 2016-07-03-pm
title: Intro to Sermon on the Mount
date: 2016-07-03
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160703+Beattitudes+V1.mp3
audioBytes: 36638814
youtube: 9vNhlTFHm3Q
---
