---
id: 2014-09-07-am
title: 'Be Patient, Brethren'
date: 2014-09-07
text: James 5:7–12
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140907+Be+Patient+-+Brethren.mp3
audioBytes: 43221230
---
