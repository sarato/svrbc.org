---
id: 2012-09-02-am
title: Truly Receiving The Word Of God
date: 2012-09-02
text: James 1:18–25
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-09-02+Truly+Receiving+The+Word+Of+God.mp3
audioBytes: 65906487
---
