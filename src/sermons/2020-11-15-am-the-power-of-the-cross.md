---
id: 2020-11-15-am
title: The Power of the Cross
date: 2020-11-15
text: 1 Corinthians 2:1–5
preacher: conley-owens
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201115-ThePowerOfTheCross.mp3
audioBytes: 58270241
youtube: 0X63Z6aX4qM
---
