---
id: 2014-06-15-am
title: Working Christ’s Works Christ’s Way
date: 2014-06-15
text: John 14:12–14
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-06-15+Working+Christ%27s+Works+Christ%27s+Way.mp3
audioBytes: 43588647
---
