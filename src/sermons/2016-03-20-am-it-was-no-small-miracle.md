---
id: 2016-03-20-am
title: It Was No Small Miracle
date: 2016-03-20
text: 2 Kings 6:1–7
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160320+It+Was+No+Small+Miracle.mp3
audioBytes: 51312952
youtube: zcyDwah3PCY
---
