---
id: 2007-05-13-am
title: Our Standard and Responsibility
date: 2007-05-13
text: Proverbs 31:10–31
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-05-13+Our+Standard+and+Responsibility.mp3
audioBytes: 38263140
---
