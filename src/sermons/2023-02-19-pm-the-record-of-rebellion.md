---
id: 2023-02-19-pm
title: The Record of Rebellion
date: 2023-02-19
text: Isaiah 30:8–11
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230219-TheRecordOfRebellion.aac
audioBytes: 25689976
youtube: 7Df4pZbHk1M
---
