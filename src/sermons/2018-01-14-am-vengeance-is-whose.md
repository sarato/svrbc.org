---
id: 2018-01-14-am
title: Vengeance is … Whose?
date: 2018-01-14
text: Romans 12:17–21
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180114-AM-VengeanceIs-Whose.mp3
audioBytes: 56430918
youtube: N_HiCK7adxo
---
