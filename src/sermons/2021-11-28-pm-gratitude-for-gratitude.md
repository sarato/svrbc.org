---
id: 2021-11-28-pm
title: Gratitude For Gratitude
date: 2021-11-28
text: 1 Chronicles 29:10–19
preacher: conley-owens
unregisteredSeries: Gratitude For Gratitude
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211128-GratitudeForGratitude.aac
audioBytes: 22052391
youtube: sN3ilP8Eshc
---
