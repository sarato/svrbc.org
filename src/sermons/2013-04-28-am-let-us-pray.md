---
id: 2013-04-28-am
title: Let Us Pray
date: 2013-04-28
text: Matthew 6:5–15
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2013/2013-04-28+Let+Us+Pray.mp3
audioBytes: 44624058
---
