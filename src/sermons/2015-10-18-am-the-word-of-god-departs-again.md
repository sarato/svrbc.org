---
id: 2015-10-18-am
title: The Word Of God Departs Again
date: 2015-10-18
text: 1 Kings 19
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151018+The+Word+of+God+Departs+Again.mp3
audioBytes: 55238848
youtube: aai5lMrJUkM
---
