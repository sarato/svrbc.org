---
id: 2020-08-09-am
title: What You Have Learned
date: 2020-08-09
text: Philippians 4:9
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200809-WhatYouHaveLearned.mp3
audioBytes: 68001585
youtube: i6MjcnwpcYA
---
