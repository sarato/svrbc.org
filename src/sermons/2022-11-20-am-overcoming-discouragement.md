---
id: 2022-11-20-am
title: Overcoming Discouragement
date: 2022-11-20
text: 2 Corinthians 1:3–11
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221120-OvercomingDiscouragement.aac
audioBytes: 52593238
youtube: hdGjED4WjbA
---
