---
id: 2021-03-07-am
title: The Power of God’s Word
date: 2021-03-07
text: 1 Thessalonians 2:13
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210307-ThePowerOfGodsWord.mp3
audioBytes: 69076785
youtube: 2mHqaR4syzo
---
