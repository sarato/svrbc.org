---
id: 2021-06-13-pm
title: A Second Exodus
date: 2021-06-13
text: Isaiah 11:11–16
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210613-ASecondExodus.aac
audioBytes: 38580138
youtube: xc9hHg88jgA
---
