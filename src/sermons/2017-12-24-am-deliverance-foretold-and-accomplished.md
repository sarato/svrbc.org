---
id: 2017-12-24-am
title: Deliverance Foretold and Accomplished
date: 2017-12-24
text: Isaiah 7:1–17
preacher: josh-sheldon
series: advent-2017
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171224+Deliverance+Foretold+and+Accomplished.mp3
audioBytes: 47217018
youtube: LSb7L4CqITI
---
