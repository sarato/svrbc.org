---
id: 2021-10-03-pm
title: Do Christians Lament?
date: 2021-10-03
text: Psalm 6
preacher: john-burchett
unregisteredSeries: Do Christians Lament?
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211003-DoChristianLament.aac
audioBytes: 40412934
youtube: E_fk8rGaZBQ
---
