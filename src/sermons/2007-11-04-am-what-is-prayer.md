---
id: 2007-11-04-am
title: What is Prayer
date: 2007-11-04
text: 2 Samuel 24:17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-11-04+What+is+Prayer.mp3
audioBytes: 40778484
---
