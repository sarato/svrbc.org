---
id: 2015-09-06-am
title: What To Do With A Sinning Brother and Why
date: 2015-09-06
text: James 5:19–20
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150906+What+To+Do+With+A+Sinning+Brother+and+Why.mp3
audioBytes: 48635935
youtube: hFPYITbkp9U
---
