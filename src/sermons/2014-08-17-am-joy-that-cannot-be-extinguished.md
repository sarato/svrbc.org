---
id: 2014-08-17-am
title: Joy That Cannot Be Extinguished
date: 2014-08-17
text: John 16:16–24
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-08-17+Joy+That+Cannot+Be+Estinguished.mp3
audioBytes: 45121283
---
