---
id: 2017-03-05-am
title: Rejoicing Through It All
date: 2017-03-05
text: Romans 5:1–11
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170305+Rejoicing+Through+It+All.mp3
audioBytes: 52204099
youtube: 0zbRELw5y2Y
---
