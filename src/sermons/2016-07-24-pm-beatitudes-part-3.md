---
id: 2016-07-24-pm
title: Beatitudes Part 3
date: 2016-07-24
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160724+Beatitudes+Part+3.mp3
audioBytes: 32735096
youtube: i0eZ5_Fji3g
---
