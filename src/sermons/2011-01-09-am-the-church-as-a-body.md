---
id: 2011-01-09-am
title: The Church as a Body
date: 2011-01-09
text: 1 Corinthians 12:12–27
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-01-09+The+Church+as+a+Body.mp3
audioBytes: 48628992
---
