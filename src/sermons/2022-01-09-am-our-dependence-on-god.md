---
id: 2022-01-09-am
title: Our Dependence on God
date: 2022-01-09
text: 1 Corinthians 3:5–9
preacher: josh-sheldon
series: maturity-in-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220109-OurDependenceOnGod.aac
audioBytes: 29758709
youtube: XeN0L1vzCVA
---
