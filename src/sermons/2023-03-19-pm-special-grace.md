---
id: 2023-03-19-pm
title: Special Grace
date: 2023-03-19
text: Isaiah 30:23–26
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230319-SpecialGrace.aac
audioBytes: 36722853
youtube: RjoTGQi13cg
---
