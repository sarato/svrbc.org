---
id: 2018-12-16-pm
title: To Limit God
date: 2018-12-16
text: Zechariah 2:1–5
preacher: josh-sheldon
series: zechariah
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/181216-PM-ToLimitGod.mp3
audioBytes: 38425647
---
