---
id: 2022-06-12-am
title: 'One Lord, Faith and Baptism'
date: 2022-06-12
text: Ephesians 4:1–6
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220612-OneLordFaithAndBaptism.aac
audioBytes: 23331543
youtube: 0QOH9MBRkTw
---
