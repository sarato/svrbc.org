---
id: 2023-01-01-am
title: A Look Back
date: 2023-01-01
text: Haggai 1:1–11
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230101-ALookBack.aac
audioBytes: 41280179
youtube: 7SZoItPn138
---
