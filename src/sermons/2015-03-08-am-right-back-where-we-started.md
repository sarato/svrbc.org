---
id: 2015-03-08-am
title: Right Back Where We Started
date: 2015-03-08
text: John 21:1–14
preacher: josh-sheldon
series: the-passion-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150308+Right+Back+Where+We+Started.mp3
audioBytes: 37763023
---
