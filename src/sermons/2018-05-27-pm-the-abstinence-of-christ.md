---
id: 2018-05-27-pm
title: The Abstinence Of Christ
date: 2018-05-27
text: Luke 22:14–18
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180527-PM-TheAbstinenceOfChrist.mp3
audioBytes: 33461556
youtube: FkEZTfAGHzo
---
