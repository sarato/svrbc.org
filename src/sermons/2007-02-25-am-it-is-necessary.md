---
id: 2007-02-25-am
title: It Is Necessary
date: 2007-02-25
text: Acts 1:15–26
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-02-25+It+Is+Necessary.mp3
audioBytes: 46002660
---
