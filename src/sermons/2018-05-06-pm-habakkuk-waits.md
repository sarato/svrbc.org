---
id: 2018-05-06-pm
title: Habakkuk Waits
date: 2018-05-06
text: Habakkuk 2:1–4
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180506-PM-HabakkukWaits.mp3
audioBytes: 30344406
youtube: SMIgWpbzWrs
---
