---
id: 2020-05-03-am
title: God’s Glory Departs
date: 2020-05-03
text: Ezekiel 10
preacher: josh-sheldon
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200503-GodsGloryDeparts.mp3
audioBytes: 51697559
youtube: BYTSho_3vEY
---
