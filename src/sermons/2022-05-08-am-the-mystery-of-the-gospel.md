---
id: 2022-05-08-am
title: The Mystery of the Gospel
date: 2022-05-08
text: Ephesians 3:1–6
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220508-TheMysteryoftheGospel.aac
audioBytes: 43524595
youtube: qDfsP92Mk2o
---
