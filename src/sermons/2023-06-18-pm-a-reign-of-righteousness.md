---
id: 2023-06-18-pm
title: A Reign Of Righteousness
date: 2023-06-18
text: Isaiah 32:1–2
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230618-AReignOfRighteousness.aac
audioBytes: 34127411
youtube: 99GFdu6LteM
---
