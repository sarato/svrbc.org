---
id: 2019-09-15-am
title: 'Already But Not Yet, A Matter Of Faith'
date: 2019-09-15
text: 1 Samuel 30
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190915-AM-AlreadyButNotYetAMatterOfFaith.mp3
audioBytes: 51695451
---
