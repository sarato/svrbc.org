---
id: 2022-09-25-am
title: 'As Slaves, Serve Christ from the Heart'
date: 2022-09-25
text: Ephesians 6:5–9
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220925-AsSlavesServeChristFromTheHeart.aac
audioBytes: 48951751
youtube: raVDyi9mZhU
---
