---
id: 2022-09-25-pm
title: The Ultimate Gathering
date: 2022-09-25
text: Isaiah 27:12–13
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220925-TheUltimateGathering.aac
audioBytes: 26172172
youtube: ypu0726tj4c
---
