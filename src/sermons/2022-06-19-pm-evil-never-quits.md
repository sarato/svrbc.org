---
id: 2022-06-19-pm
title: Evil Never Quits
date: 2022-06-19
text: 1 Samuel 17:1–11
preacher: josh-sheldon
series: the-life-of-david
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220619-TheBattleisSpiritual.aac
audioBytes: 20333505
youtube: oDGAku5M-1U
---
