---
id: 2022-06-19-am
title: The Grace of Christ’s Gift
date: 2022-06-19
text: Ephesians 4:1–10
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220619-TheGraceofChristsGift.aac
audioBytes: 42961639
youtube: N6ul3zFDNdE
---
