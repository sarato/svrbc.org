---
id: 2019-09-29-am
title: Heroes Need Not Apply
date: 2019-09-29
text: Acts 8:1–8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190929-AM-HeroesNeedNotApply.mp3
audioBytes: 47587729
---
