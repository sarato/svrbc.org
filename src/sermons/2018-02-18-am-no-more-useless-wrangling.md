---
id: 2018-02-18-am
title: No More Useless Wrangling
date: 2018-02-18
text: Romans 14:1–12
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180218+No+More+Useless+Wrangling.mp3
audioBytes: 53582495
youtube: sYzWpMeK2x0
---
