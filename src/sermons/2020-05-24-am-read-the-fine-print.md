---
id: 2020-05-24-am
title: Read The Fine Print
date: 2020-05-24
text: Philippians 3:1–11
preacher: josh-sheldon
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200524-ReadTheFinePrint.mp3
audioBytes: 88342085
youtube: UZ8Clhe3TWI
---
