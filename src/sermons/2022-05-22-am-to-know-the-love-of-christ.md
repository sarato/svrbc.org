---
id: 2022-05-22-am
title: To Know the Love of Christ
date: 2022-05-22
text: Ephesians 3:14–19
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220522-ToKnowtheLoveofChrist.aac
audioBytes: 32913292
youtube: zI5yYARcH1Q
---
