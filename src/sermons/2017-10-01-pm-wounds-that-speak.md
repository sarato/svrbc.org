---
id: 2017-10-01-pm
title: Wounds that Speak
date: 2017-10-01
text: Isaiah 1:1–7
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171001+Wounds+that+speak.mp3
audioBytes: 28070692
youtube: erM4ODRQ3Ng
---
