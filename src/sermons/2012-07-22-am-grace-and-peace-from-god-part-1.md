---
id: 2012-07-22-am
title: 'Grace and Peace from God, Part 1'
date: 2012-07-22
text: Revelation 1:4
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-07-22+Grace+and+Peace+from+God,+Part+1.mp3
audioBytes: 51124671
---
