---
id: 2013-07-14-am
title: Two Sad Followers
date: 2013-07-14
text: Luke 24:13–31
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-07-14+Two+Sad+Followers.mp3
audioBytes: 34024335
---
