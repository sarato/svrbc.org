---
id: 2016-03-13-am
title: The Descent of a Servant of God
date: 2016-03-13
text: 2 Kings 5:20–27
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160313+The+Descent+of+a+Servant+of+God.mp3
audioBytes: 57515504
youtube: 6ECbxlgs8AY
---
