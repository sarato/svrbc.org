---
id: 2019-08-04-am
title: A Special Day Of Worship
date: 2019-08-04
text: Nehemiah 12:27–47
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190804-AM-ASpecialDayOfWorship.mp3
audioBytes: 47972687
---
