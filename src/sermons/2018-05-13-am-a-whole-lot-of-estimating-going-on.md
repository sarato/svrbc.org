---
id: 2018-05-13-am
title: A Whole Lot Of Estimating Going On
date: 2018-05-13
text: Matthew 11:1–24
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180513-AM-AWholeLotOfEstimatingGoingOn.mp3
audioBytes: 53755154
youtube: R29PfwTupGA
---
