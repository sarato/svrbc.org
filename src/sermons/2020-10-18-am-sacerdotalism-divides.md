---
id: 2020-10-18-am
title: Sacerdotalism Divides
date: 2020-10-18
text: 1 Corinthians 1:13–17
preacher: conley-owens
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201018-SacerdotalismDivides.mp3
audioBytes: 52172635
youtube: jezMP6Mjf3c
---
