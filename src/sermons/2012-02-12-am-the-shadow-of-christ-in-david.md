---
id: 2012-02-12-am
title: The Shadow of Christ in David
date: 2012-02-12
text: 2 Samuel 10
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-02-12+The+Shadow+of+Christ+in+David.mp3
audioBytes: 42037407
---
