---
id: 2009-05-24-am
title: Sharing The Truth in the Strength of the Lord
date: 2009-05-24
text: 1 Peter 3:15
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-05-24+Sharing+The+Truth+in+the+Strength+of+the+Lord.mp3
audioBytes: 50657631
---
