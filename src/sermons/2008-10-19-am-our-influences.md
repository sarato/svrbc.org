---
id: 2008-10-19-am
title: Our Influences
date: 2008-10-19
text: 2 Chronicles 23
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-10-19+Our+Influences.mp3
audioBytes: 58748682
---
