---
id: 2019-04-14-am
title: Bring Out The Book!
date: 2019-04-14
text: Nehemiah 8:1–12
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190414-AM-BringOutTheBook.mp3
audioBytes: 47930464
---
