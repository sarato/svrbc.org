---
id: 2021-07-04-pm
title: 'The Restoration of Israel: The Purpose of Enemies'
date: 2021-07-04
text: Isaiah 14:1–2
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210704-TheRestorationOfIsrael-ThePurposeOfEnemies.aac
audioBytes: 33209649
youtube: xoApQ14ZkKg
---
