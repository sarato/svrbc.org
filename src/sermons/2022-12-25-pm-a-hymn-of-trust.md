---
id: 2022-12-25-pm
title: A Hymn of Trust
date: 2022-12-25
text: Psalm 20
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/221225-AHymnOfTrust.aac
audioBytes: 33307755
youtube: B0e59UcA8tI
---
