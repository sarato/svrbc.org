---
id: 2011-02-13-am
title: He Cannot Be Manipulated
date: 2011-02-13
text: Exodus 20:4–7
preacher: josh-sheldon
series: the-ten-commandments
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-02-13+He+Cannot+Be+Manipulated.mp3
audioBytes: 42935040
---
