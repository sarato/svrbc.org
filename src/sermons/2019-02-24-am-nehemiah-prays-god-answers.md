---
id: 2019-02-24-am
title: 'Nehemiah Prays, God Answers'
date: 2019-02-24
text: Nehemiah 4:10–15
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190224-AM-NehemiahPraysGodAnswers.mp3
audioBytes: 51673716
---
