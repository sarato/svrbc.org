---
id: 2021-12-26-pm
title: The Merging of Nations
date: 2021-12-26
text: Isaiah 19:16–25
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211226-TheMergingofNations.aac
audioBytes: 21687448
youtube: 6-G2uhgcrBM
---
