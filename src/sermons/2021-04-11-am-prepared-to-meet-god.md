---
id: 2021-04-11-am
title: Prepared to Meet God
date: 2021-04-11
text: 1 Thessalonians 3:11–13
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210411-PreparedToMeetGod.mp3
audioBytes: 59596217
youtube: KLyLhLL1598
---
