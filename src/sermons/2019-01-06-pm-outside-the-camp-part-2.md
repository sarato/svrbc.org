---
id: 2019-01-06-pm
title: Outside The Camp Part 2
date: 2019-01-06
text: Hebrews 13:11–13
preacher: josh-sheldon
series: outside-the-camp
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190107_PM-OutsideTheCampPt2.mp3
audioBytes: 44298575
---
