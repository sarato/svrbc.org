---
id: 2017-11-12-am
title: God Only Wise
date: 2017-11-12
text: Romans 11:11–36
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/171112+God+Only+Wise.mp3
audioBytes: 55295743
youtube: ogi7qyki6pY
---
