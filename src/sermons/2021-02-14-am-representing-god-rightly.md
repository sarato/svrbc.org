---
id: 2021-02-14-am
title: Representing God Rightly
date: 2021-02-14
text: 1 Thessalonians 2:3–5
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210214-RepresentingGodRightly.mp3
audioBytes: 68404080
youtube: ScvpnI07EvU
---
