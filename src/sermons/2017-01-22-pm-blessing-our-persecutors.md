---
id: 2017-01-22-pm
title: Blessing Our Persecutors
date: 2017-01-22
text: Psalm 4
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170122+Afternoon+Service-Blessing+our+Persecutors.mp3
audioBytes: 26297679
youtube: Ha60qFEgQps
---
