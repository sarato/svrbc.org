---
id: 2021-12-12-pm
title: A Tale Of Two Words
date: 2021-12-12
text: Psalm 12
preacher: josh-sheldon
unregisteredSeries: A Tale Of Two Words
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211212-ATaleOfTwoWords.aac
audioBytes: 21981803
youtube: Gks7oNldgow
---
