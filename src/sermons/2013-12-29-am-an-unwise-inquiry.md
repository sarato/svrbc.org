---
id: 2013-12-29-am
title: An Unwise Inquiry
date: 2013-12-29
text: Ecclesiastes 7:10
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-12-29+An+Unwise+Inquiry.mp3
audioBytes: 39661758
---
