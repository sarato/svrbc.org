---
id: 2022-10-23-pm
title: The Source of Science
date: 2022-10-23
text: Isaiah 28:23–29
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221023-TheSourceOfScience.aac
audioBytes: 36631289
youtube: Fjs-IqpeQTE
---
