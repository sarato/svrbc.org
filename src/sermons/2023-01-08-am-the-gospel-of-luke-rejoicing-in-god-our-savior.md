---
id: 2023-01-08-am
title: 'The Gospel of Luke: Rejoicing in God our Savior'
date: 2023-01-08
text: Luke 1:39–56
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230108-TheGospelOfLuke-RejoicingInGodOurSavior.aac
audioBytes: 54454613
youtube: rvL30HQ9SF8
---
