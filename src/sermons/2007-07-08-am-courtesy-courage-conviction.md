---
id: 2007-07-08-am
title: 'Courtesy, Courage, Conviction'
date: 2007-07-08
text: Acts 4:1–12
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-07-08+Courtesy,+Courage,+Conviction.mp3
audioBytes: 48209007
---
