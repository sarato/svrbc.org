---
id: 2019-12-08-pm
title: Evangelical Endurance
date: 2019-12-08
text: Luke 10:17–20
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191208-PM-EvangelicalEndurance.mp3
audioBytes: 34951575
---
