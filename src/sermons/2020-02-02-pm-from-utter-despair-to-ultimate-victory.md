---
id: 2020-02-02-pm
title: From Utter Despair To Ultimate Victory
date: 2020-02-02
text: Psalm 13
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200202-PM-FromUtterDespairToUltimateVictory.mp3
audioBytes: 40184431
youtube: JV48NDXUHMY
---
