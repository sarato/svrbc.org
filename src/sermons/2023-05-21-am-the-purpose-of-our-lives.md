---
id: 2023-05-21-am
title: The Purpose Of Our Lives
date: 2023-05-21
text: Luke 4:38–44
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230521-ThePurposeOfOurLives.aac
audioBytes: 44476551
youtube: KNnFWNe2_Zo
---
