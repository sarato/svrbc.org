---
id: 2008-02-03-am
title: Faith Trumps Fear
date: 2008-02-03
text: Psalm 11
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-02-03+Faith+Trumps+Fear.mp3
audioBytes: 39459930
---
