---
id: 2016-08-14-pm
title: Beatitudes Part 6
date: 2016-08-14
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160814+Beatitudes+Part+6.mp3
audioBytes: 24760429
youtube: cXrBoLxylpk
---
