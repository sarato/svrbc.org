---
title: Free Intern/New Grad Housing
image: ../../banners/suitcases.jpg
bannerstyle: dark
---

import Testimonial from '~/components/Testimonial'

Openings in Sunnyvale (Santa Clara County), Redwood City, and Fremont.

> Do not neglect to show hospitality to strangers, for thereby some have
> entertained angels unawares.
>
> <p class="citation">Hebrews 13:2</p>

Every summer, tens of thousands of young adults flock to the SF bay area, where
the jobs are plentiful but the housing is not.  SVRBC would like to step in and
help!  Several families are available to host an intern or a new graduate for
anywhere from a few weeks to several months.

This program offers not only free housing, but an immediate base of support in
a local church.  Young singles will have an opportunity to interact closely
with a godly family, and in return, we can be encouraged by their presence and
fellowship!

## The application process {#process}

1.  Applicants may begin by
    [filling out this form](https://forms.gle/Sn4iB89QP6KrC9rj6).
2.  The character reference listed by the applicant will be contacted for a
    recommendation.
3.  A standard background check will be conducted.
4.  If there are no issues with the application and there is available space,
    applicants will be paired with a host family who will contact them.
5.  If both the host family and applicant agree on the fit, then the applicant
    will be able to stay with the host for a specified period of time!

## Rules and guidelines {#rules}

While host families will each have their own rules around the home, we require that the following rules be observed in order to promote the safety and spiritual benefit of this program.

1.  Family activities and costs
    1.  The host family should generally welcome the guest’s involvement in
        family activities, including meals.
    2.  The host family must not request or require any payment from the guest
        for housing, utilities, or food.
    3.  Note: The host family will be provided with a weekly stipend to help
        offset costs incurred while participating in this program.
2.  Daily family worship
    1.  Guests must make a serious effort to be present for daily family
        worship.
    2.  Hosts must attempt to accommodate the schedule of the guest when
        scheduling family worship.
    3.  Recommendation: Schedule family worship for the morning and have a
        backup time in the evening for when that does not work.
    4.  Recommendation: If your family worship is centered around teaching
        children, restructure it to accommodate adults as well.
3.  Church attendance
    1.  Guests must make a serious effort to be regular in their attendance and
        participation in SVRBC meetings and activities.
    2.  Note: SVRBC serves the Lord’s supper weekly, only to those who are
        members of true churches.
4.  Children and safety
    1.  Guests are not to be left alone with children under the age of 16.
    2.  Guests are not to play with children behind closed doors.
5.  At-will housing
    1.  Both the guest and host family have the right to terminate the
        arrangement for any reason.

<testimonial name="Darshan, Summer 2022">
  <template slot="image">
    <g-image src="./darshan-g.testimonial.jpg"
      alt="Portrait of Darshan" />
  </template>
  Coming from India and being used to having many people around, I was worried
  I would be homesick during the summer.  However, my host family and the
  church made me feel very welcomed, and <strong>I felt like I was at
  home.</strong>  I also learned a lot about Jesus Christ during my time there.
</testimonial>

<testimonial name="Sebastian, Summer 2022">
  <template slot="image">
    <g-image src="./sebastian-f.testimonial.jpg"
      alt="Portrait of Sebastian" />
  </template>
  I most appreciated spending time with my host family. I come from a single
  parent home, and having the opportunity to experience how a godly family
  operates has been <strong>very encouraging and inspiring</strong>.
</testimonial>

<testimonial name="April, Fall 2020">
  <template slot="image">
    <g-image src="./april-y.testimonial.jpg"
      alt="Portrait of April" />
  </template>
  Living with the family was a blessing and miracle from God. It is hard to
  move to a new city and find a loving, godly community, especially during
  COVID.  Having the opportunity to stay with the family turned the challenge
  into <strong>a memorable experience I could never imagined</strong>. How
  the family served and worshiped God, loved brothers and sisters, valued
  God's Words and hospitality was inspiring. I am looking forward to
  returning to the community to serve and grow there. 
</testimonial>

<testimonial name="Emmanuel, Spring 2020">
  <template slot="image">
    <g-image src="./emmanuel.o.testimonial.jpg"
      alt="Portrait of Emmanuel" />
  </template>
  A big thank you to [SVRBC] for their hospitality. Moving to the Bay Area, 
  I felt anxious about finding a new church and meeting new people. The church
  was very welcoming and helped me sort out accommodation at a time when things
  were influx. <strong>I also gained a lot spiritually</strong> from my host.
</testimonial>

<testimonial name="Rebekah, Fall 2019">
  <template slot="image">
    <g-image src="./rebekah-h.testimonial.jpg"
      alt="Portrait of Rebekah" />
  </template>
  The time with my host family and the church has been a great blessing. I 
  especially enjoyed joining family devotions. Through my host family’s 
  commitment to growing in their knowledge of God, and through their love for 
  Him and others, <strong>I was encouraged to seek to grow in these areas
  myself.</strong>
</testimonial>

<testimonial name="Daniel, Summer 2019">
  <template slot="image">
    <g-image src="./daniel-m.testimonial.jpg"
      alt="Portrait of Daniel" />
  </template>
  Being part of the Intern Hosing Program with SVRBC was an amazing experience. 
  Everyone at the church was very nice, and <strong>I felt connected and
  welcomed from the moment I got there.</strong> Conley and his family were
  great hosts, and I could not have had a better experience staying with them!
</testimonial>

<testimonial name="Ayo, Summer 2019">
  <template slot="image">
    <g-image src="./ayo-e.testimonial.jpg"
      alt="Portrait of Ayo" />
  </template>
  I used to think I was humble until I met my hosts (the Burchetts)!  The 
  entire family treated me with so much love and respect and that made me 
  feel completely comfortable in their house. <strong>Staying with them is an 
  answer to a prayer</strong> I prayed before getting married.
</testimonial>

<testimonial name="JD, Spring 2019">
  <template slot="image">
    <g-image src="./jd-b.testimonial.jpg" alt="Portrait of JD" />
  </template>
  This church is great, extremely welcoming and tons of fun activities. The
  family that took me in was awesome, <strong>they couldn’t have been more
  hospitable.</strong>
</testimonial>

<testimonial name="Jesús, Fall 2016">
  <template slot="image">
    <g-image src="./jesus-gm.testimonial.jpg"
      alt="Portrait of Jesús" />
  </template>
  I was greatly blessed by the opportunity to have a place to arrive at this new
  area, where I don’t have relatives, because <strong>I found a family in this
  church.</strong>  It was a great learning opportunity, and helped me grow in
  the Lord, both by studying the Bible with them and also by watching their
  sacrificial love for one another.
</testimonial>

<style scoped>
ol ol {
  list-style-type: lower-alpha;
}
</style>

<spell-ignore>
  Ayo
  Barela
  Darshan
  Erinfolami
  Houser
</spell-ignore>
