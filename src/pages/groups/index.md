---
title: Fellowship Groups
image: ../../banners/men-bench.jpg
---

import DataForm from '~/components/DataForm'
import MidwayCta from '~/components/MidwayCta'

We spend Sunday together as a church, but Christians ought to spend time with
each other throughout the week, encouraging and praying for each other.
Joining a fellowship group is a great way to make that happen!

> But exhort one another every day, as long as it is called “today,” that none
> of you may be hardened by the deceitfulness of sin.
>
> [Hebrews 3:13 (ESV)]{.citation}

## Home groups {#homegroups}

Home groups are geographically centered fellowships for men and women of all
ages.  They are not “little churches,” but facilitate Bible study and prayer in
the middle of the week when it is difficult for the whole church to meet.

We encourage everyone at SVRBC to join the nearest home group that fits their
schedule.

-   Redwood City, Tuesdays - 7:00 [pm]{.meridiem} study
-   Casa de Amigos, Tuesdays - 6:00 [pm]{.meridiem} dinner, 7:00 [pm]{.meridiem} study
-   South San Jose, Tuesdays - 6:00 [pm]{.meridiem} dinner, 7:00 [pm]{.meridiem} study
-   Sunnyvale West, Wednesdays - 6:00 [pm]{.meridiem} dinner, 7:00 [pm]{.meridiem} study
-   Lakewood Village, Thursdays - 6:00 [pm]{.meridiem} dinner, 7:00 [pm]{.meridiem} study
-   Fremont, Fridays - 6:00 [pm]{.meridiem} dinner, 7:00 [pm]{.meridiem} study

<midway-cta>
  <data-form id="homegroupform" data-button-text="Sign up for a home group" :template="homegroupSignup" />
</midway-cta>

## Men's group {#mens-group}

Join us every other Friday, as the men get together to eat dinner and study
the Proverbs.  Meeting location varies, contact Albert for more information.

<midway-cta>
  <data-form id="mensgroupform" data-button-text="Get more info" :template="mensgroupSignup" />
</midway-cta>

<script>
import homegroupSignup from '~/data/forms/homegroup-signup.yml'
import mensgroupSignup from '~/data/forms/mensgroup-signup.yml'
//
export default {
  data() {
    return {
      homegroupSignup,
      mensgroupSignup
    }
  }
}
</script>
