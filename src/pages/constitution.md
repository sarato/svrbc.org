---
title: Constitution of Silicon Valley Reformed Baptist Church
image: ../banners/bricks.jpg
---

Lakewood Village Baptist Church (dba Silicon Valley Reformed Baptist Church)

Sunnyvale, California

(revised April 3, 2021)

## Preamble {#preamble}

We, the members of Lakewood Village Baptist Church, in an effort to secure and
promote a unity of mind and spirit, do covenant together to establish the
following articles, to which, as members "one of another" in this local church
body, we willingly submit ourselves.

## Article One: Name {#name}

The name of this organization shall be "Lakewood Village Baptist Church" of
Sunnyvale, California.

## Article Two: Purpose {#purpose}

The purpose of this church shall be to glorify God in:

-   Faithfully gathering corporately for acceptable worship, teaching, and
    prayer in accordance with and in obedience to the word of God.

-   The declaration of the whole counsel of God, including the proclamation of
    the Gospel of free grace to sinners, the edification of believers through
    applicatory preaching, and the defense of the faith once delivered unto the
    saints.

-   The advancement of the kingdom of God through the support of domestic and
    foreign missions and Christian education of like faith and practice.

-   The observance of the ordinances and the building up of the assembly through
    mutual fellowship, concern, and pastoral care.

-   To function under the Lord Jesus Christ our head as a local assembly in
    obedience to every Scriptural directive bearing on the administration of such
    assemblies.

## Article Three: Doctrinal Statement {#doctrine}

### I. Confession of Faith {#confession}

This church, acknowledging the absolute supremacy and authority of the Holy
Scriptures as the very word of God and thus the only creed and rule of both
faith and practice, holds essentially and substantially to the First (1644) and
Second (1689) London Baptist Confessions and to the New Hampshire Baptist
Confession (1833) as the historic expression of our faith. However, we do
deviate from these Confessions at certain points, the following being a partial
listing of these deviations:

1644 Confession

-   **Article 33**:  that part which makes the church and Kingdom identical.

-   **Article 48**: that part dealing with the political, civil, and
    ecclesiastical situation peculiar to that era and kingdom.

1689 Confession

-   **Chapter 10, paragraph 3**: The salvation of all who die in infancy, being a
    matter of which the Scripture is silent, is left to the infinite wisdom of
    God.

-   **Chapter 26, paragraph 1**: The catholic or universal, invisible church.

-   **Chapter 26, paragraph 4**: The Pope as the antichrist.

### II. The Ordinances {#ordinances}

#### General Statement {#general}

There are two ordinances of special significance which our Lord has commanded
us to observe, namely, baptism and the Lord's Supper. Neither of them has
saving merit, nor is receiving them necessary for salvation, nor is any grace
automatically imparted to the recipient through the water of baptism or the
bread and the cup of the Supper (Acts 8:13, 18-24; 1 Cor.  11:20, 27-30; Lk.
23:32-43). Nevertheless they are means of grace and powerful aids to the faith
of believers who properly participate in them.  Accordingly our Lord is
concerned that they be observed unto edification, in a decent and orderly
manner. Therefore, our policy regarding their observance is specified in the
following sections.

#### A. Baptism {#baptism}

Only confessed disciples of our Lord Jesus Christ are proper candidates for
baptism, and all such persons should be baptized and joined to a local church
(Acts 2:38, 41, 47; 5:13, 14). Believing that baptism in water is the
God-ordained sign of one's personal union with Christ in His death, burial, and
resurrection, and the door of entrance into the visible community of the people
of God, we shall receive into the membership of the church only those who have
been baptized as believers "into the name of the Father and of the Son and of
the Holy Spirit" (Matt. 28:19). Immersion in water is the biblical mode of
baptism, is necessary for its due administration, and is the only mode to be
administered by this church.

#### B. The Lord’s Supper {#communion}

Whereas baptism is the initiatory ordinance by which one enters the visible
church and should be observed only once by each believer, the Lord's Supper
should be observed frequently by the assembled church (Acts 2:42; 1 Cor.
11:26). Therefore, in virtue of the unity of all the true churches of Christ,
which collectively are His body (1 Cor. 10:16-17; 12:27-28; Col.  1:18), and in
virtue of our Lord's will that only those who are under the government of His
church should be admitted to the privileges of His church (Acts 2:41-42; 1 Cor.
10:16; London Confession 26:12), the elders will faithfully seek to ensure
that only true believers who are members in good standing of true churches are
admitted to the table. True believers whose church membership involves unusual
circumstances may be admitted at the discretion of the elders. This is a most
holy ordinance and should be observed with solemn joy (Ps. 2:11-12) and
dignity, even though the bread and the cup of the Supper are and remain only
symbols of the body and the blood of our Lord Jesus Christ. The Lord's Supper
shall be held no less than monthly by the church, under normal circumstances.

## Article Four: Covenant {#covenant}

Having been, as we trust, brought by God's sovereign grace to embrace through
faith the Lord Jesus Christ, and baptized in the name of the Father, and of the
Son, and of the Holy Ghost, we most solemnly and joyfully covenant together as
an assembled local body of Christ, to walk together in Him, with brotherly
love, to His glory, as our common Lord (Jonah 2:9; Jn. 1:12-13; Eph. 1:3-14,
19-23; Phil. 4:13; 2 Timothy 1:9; Matt. 28:18-20; Acts 2:36). We do therefore,
in His strength, engage:

-   That we will strive for the advancement of this church in knowledge,
    holiness, and comfort, promoting its prosperity and spirituality, making it
    preeminent in our associations (Jn. 13:14; Rom. 13:8-10; 1 Cor.12:1-12; Phil.
    1:9-11; Eph. 4:13-16; 1 Thess. 4:9; Heb. 12:14; 1 Pet. 1:14-16).

-   That we will not forsake the assembling of ourselves together, but will
    faithfully sustain through both attendance and prayer our public worship of
    God, while upholding the ordinances, doctrines, and discipline of this church
    (Matt 28:19; Heb. 10:23-25; 13:7,17).

-   That we will regularly and cheerfully contribute, according as God has
    prospered us, to the expenses of this church, and the support of its
    ministries (1 Cor. 9:6-14; 16:2; 2 Cor. 9:7; 1 Tim. 5:17-18).

-   That we will cultivate private and family worship at home, striving to attain
    unto the scriptural example for ourselves and our families, bringing up our
    children in the nurture and admonition of the Lord, training them for the
    service of Christ (Deut. 6:4-12; Prov. 22:6; Eph. 5:22-33; 6:1-4; Col.
    3:18-20).

-   That we will exercise a Christian care and watchfulness over each other,
    faithfully warning, exhorting, and admonishing each other in love as occasion
    may require, being slow to take offense and always ready for reconciliation,
    endeavoring to maintain a scriptural unity one with another (Col. 3:8-17;
    Heb. 3:12-13; 10:24).

-   That we will, being jealous of the honor of God's name as reflected through
    this church, walk circumspectly in the world, be equitable in our dealings,
    faithful in our engagements, exemplary in our deportment, guarding our speech
    from all sins of the tongue, abstaining from the misuse of all drugs (Rom.
    12:1-2; Eph. 5:1-13; Jas. 1:26, 27; 3:2-12; Col. 3:8-10).

-   That when we remove from this place we will, as soon as possible, unite with
    another church of like faith where we can carry out the spirit of this
    covenant and the principles of the word of God (Acts 1:15; 2:41-42, 47;
    9:26-28).

-   That we will in all conditions strive to live to the glory of Him who hath
    called us out of darkness into His marvelous light (1 Cor. 10:31; Col.
    1:12-13; 1 Pet. 2:9).

_And may the God of peace, who brought again from the dead our Lord Jesus
Christ, that great Shepherd of the sheep, through the blood of the everlasting
covenant, make us perfect in every good work, to do His will, working in us
that which is well pleasing in His sight through Jesus Christ to whom be glory,
forever and ever. Amen._

## Article Five: Membership {#membership}

### I. Admission of Members {#admission}

A. Any person professing repentance toward God and faith toward our Lord Jesus
   Christ, having been scripturally baptized upon profession of their faith,
   expressing essential agreement with the doctrines, aims, and government of
   this church, manifesting a proper conformity to the word of God, shall be
   eligible for membership consideration.

B. Any person having made known their desire for membership in this church
   shall be interviewed by the elders and deacons with regard to their
   salvation, spiritual life, seriousness of intention to wholeheartedly
   support the total ministry of this church, and a proper conformity to the
   word of God.

C. Should any member object to the applicant the principle of Matt. 18:15-17
   shall be exercised prior to the meeting in which the vote is to be taken.
   Any objection indicated during this meeting may be made only in the form of
   a dissenting vote.

D. A vote of the church is required for the acceptance of the applicant into
   membership.  This proceeding should be announced in a business meeting
   agenda at least two weeks prior to the scheduled business meeting.

### II. Responsibilities of Membership {#responsibilities}

A. It is to be understood that membership is not a trivial undertaking, and is
   unquestionably an obligatory relationship. As such, every member shall
   seriously and prayerfully consider the responsibilities and obligations
   placed upon him by the word of God, and his willing entrance into the
   covenant of this church.

B. Each member shall willingly give of his/her health, strength, time, and
   abilities to assist in the spiritual and physical work and needs of the
   church, recognizing not only that he has a level of responsibility equal to
   that of every other member of this church, but that in addition, to the
   exact degree that he is negligent in this responsibility, he is an
   impediment to the physical and spiritual lives of those members who make up
   that which is lacking.

### III. Termination of Membership {#termination}

Although church membership is a New Testament concept and the obligation of
every Christian, none may force or compel another to join. Membership in a
local church is therefore not compulsory, but voluntary. However, neither is
church membership a unilateral decision on the part of the believer. It may not
be entered into or departed from at the sole discretion and desire of the
individual. Accordingly, the church has both the right and the obligation to
receive and dismiss its members.

In each of the conditions described below (with the exception of the death of
members), the termination of a person's membership may only be granted by the
suffrage of the church. In all cases except formal corrective discipline, a
vote of the church shall be required to terminate anyone's membership in
Lakewood Village Baptist Church.

Conditions of biblical termination are as follows:

#### A. Death {#death}

God, in His wisdom, may choose to remove members from us through death.  Upon
the death of any member, his or her name shall be automatically removed from
the church's membership.

#### B. Transfer of Membership to Another Church {#transfer}

Should a member wish to join another church, he may retain the privileges of
membership in this church while pursuing membership elsewhere by initiating a
process of transfer.

1.  A member not currently subject to the process of formal corrective
    discipline may initiate a process of transfer by announcing his intentions
    to the elders.

2.  A letter of transfer may be provided at the request of either the departing
    member or the leadership of the church to which he desires to unite. Such
    correspondence shall not be generic for any church, but rather shall be
    directed to the leadership of a specific congregation comprising a true
    church.

3.  As appropriate, the officers and members of this church should remain in
    regular communication with the transferring member in order to fulfill the
    mutual obligations outlined in the membership covenant.  Likewise, the
    transferring member should remain in regular communication with the elders
    and members of this church.

4.  A period of one year shall be allowed in which to unite with another
    church, after which time he must resume full participation in this church
    or request dismissal.  Any extension to this time period must be approved
    by a vote of the church.

#### C. Dismissal From Membership {#dismissal}

From time to time, occasions will arise which may warrant the termination of
church membership, but where transfer or discipline would be inappropriate. In
such cases where a member may wish to terminate his fellowship with Lakewood
Village Baptist Church for reasons which do not impugn his Christian profession
or otherwise warrant formal church discipline, he may be dismissed from the
membership of this church. Due to the serious nature of the covenant
considerations included in church membership, a member may not be dismissed
until after attempts have been made by the elders to recover the individual
in question. Furthermore, the church may deem it prudent to publicly state its
opposition to and disagreement with the member's reasons for leaving, and to
communicate the same to the departing member. Individuals thus dismissed from
the membership of Lakewood Village Baptist Church will still be considered by
this church to be fellow believers, though perhaps in error. Termination of
membership does not give former members license to sow discord or otherwise
threaten the peace and unity of this church. Accordingly, if it is established
(according to the discretion of the elders) that a former member is behaving
divisively, appropriate warnings may be issued to and by the church, in order
to preserve the peace and harmony of this and other local churches.

#### D. Formal Corrective Discipline {#discipline}

_Formative_ discipline in the church is to take place through the normal,
ongoing ministry of the word in the life of the church, in biblical pastoral
oversight and in the mutual watch-care of one another. In this sense, every
member of the church is under its discipline.

_Corrective_ discipline is a rare, but biblically necessary action of the
church whereby it excludes from its fellowship those involved in such
scandalous conduct that it brings reproach to the name and cause of Christ.
Accordingly, corrective discipline must not be exercised carelessly or loosely,
nor should it be seen as a substitute for formative discipline, or used as a
threat over the heads of God's people.

The _purpose_ of corrective discipline is to therefore maintain the honor of
Christ's name in the church, to protect and maintain the purity of the church,
and to be the means of restoration, correction and recovery to those under it.

The _grounds_ for corrective church discipline may include, but are not
necessarily limited to:

1.  Irreconcilable differences over personal offenses (Matthew 18:15-17).

2.  Immorality in conduct (1 Corinthians 5:1-13).

3.  Divisive teaching and/or behavior (Romans 16:17; Titus 3:10-11).

4.  Damnable heresy/false doctrine (2 John 10).

5.  Disorderly Behavior:

    1.  Persistent failure in Christian duty as defined in the Church Covenant
        (Thessalonians 3:6, 14-15).

    2.  Forsaking the assembly by willful neglect (Hebrews 10:23-25). Any member
        who develops a pattern of neglecting attendance to the stated worship
        services of the church will be subject to disciplinary action, at the
        discretion of the elders.

When a member is found to err in an area for which God directs corrective
discipline (as outlined above), the elders shall have the particular
responsibility of attempting to effect his restoration. If such attempts fail,
he shall be notified in writing of the charges preferred against him, and be
invited to publicly answer the charges at a church meeting in which
disciplinary action will be considered. If he fails either to attend or
satisfactorily answer the charges, he shall be removed from membership by a
vote of the church.

Our relationship to those disciplined and excluded from us must be considered
in the context of understanding New Testament prescriptives. In some cases,
those disciplined are to be considered unsaved (Mt. 18:17). In other cases,
those cut off from fellowship were to be still viewed as brethren (2 Thess.
3:14-15). In still other cases, the spiritual state of the offender may not be
clear (1 Cor. 5:5). In all cases, the teaching of the Bible is that fellowship
is broken, and the former close relationship has been severed.

Accordingly, we are to avoid all unnecessary contact and relationship to those
excluded "that they may be ashamed" and seek restoration. However, we must
still seek to be cordial with those under discipline, maintaining in some cases
even a limited degree of friendship, so that we may perhaps be used of the Lord
to restore them to Christ and His church.

Any person who has been excluded from membership, under discipline may be
restored to fellowship upon satisfactory evidence of his repentance toward God
and this church. Those who for legitimate reasons may not require restoration
to membership in this church, may upon satisfactory evidence of their
repentance have their discipline removed by a favorable vote of the church. A
vote of the church is required for restoration to church membership and/or
removal of corrective church discipline.

## Article Six: Government {#government}

This church is patterned after the New Testament example and teaching, i.e., we
are an autonomous, congregational-controlled local assembly. Our only head is
the Lord Jesus Christ, whose government is exercised through our elders and
deacons.

### I. Elders/Pastors {#elders}

#### A. Qualifications {#elder-qualifications}

He shall fulfill the requirements of 1 Timothy 3:1-6 and Titus 1:5-9.

#### B. Selection {#elder-selection}

1.  The elders of this church shall be responsible to secure possible candidates
    for the pastorate.  They shall interview and endeavor to hear them speak
    prior to recommendation for candidacy. Any member shall be free to recommend
    possible candidates, but all invitations shall come from the elders alone.
    Further, the elders alone shall have the responsibility to notify the
    candidate of the church's decision regarding him.  In the event of a vacant
    pastorate, the deacons shall bear the aforementioned responsibilities.  In
    the further event of a vacant diaconate, a pastoral search committee elected
    by a vote of the church shall bear these responsibilities.

2.  Each candidate shall preach in at least two public church services.

3.  An elder shall be elected by a vote of the church, with unanimity regarded
    as the aim. Notice of this meeting shall be given in at least two regular
    meetings of the church prior to the election, and be held at least one week
    following the candidate's appearance.

4.  Any man called to the pastorate of this church must be a Baptist who holds
    tenaciously to the doctrines of God's free and sovereign grace as well as to
    both scriptural and historical Baptist distinctives.

5.  If not already a member of this church, the candidate shall enter into
    membership automatically upon his appointment as an elder.  However, where
    applicable, his wife and children shall enter membership in accordance with
    Article Five of this constitution.

#### C. Duties {#elder-duties}

The elders shall earnestly endeavor to:

1.  Give themselves continually to prayer and to the ministry of the word.

2.  Preach the word; be instant in season and out of season; reprove; rebuke,
    and exhort with all long-suffering and doctrine.

3.  Take heed unto the doctrine, himself, and to this flock over which the Holy
    Ghost has made them overseers.

4.  Feed the flock with knowledge and understanding, taking the oversight
    thereof, not by constraint, but willingly; not for filthy lucre, but of a
    ready mind; neither as being lords over God's heritage, but as examples to
    the flock in word, in conversation, in charity, in spirit, in faith, and in
    purity.

(Acts 6:4, 20:28; Jer. 3:15; 1 Peter 5:2,3; 1 Tim. 4:12, 16; 2 Tim. 4:2)

### II. Deacons {#deacons}

#### A. Qualifications {#deacon-qualifications}

He shall fulfill the requirements of Acts 6:3 and 1 Tim. 3:8-13. Too much care
cannot be taken to secure the right kind of men, considering that the permanent
influence of a deacon is scarcely surpassed by that of the pastor.

#### B. Selection {#deacon-selection}

1.  The deacons shall be nominated by the church, then elected by a vote of the
    church.

    1.  Their term of office shall be three years, serving as many consecutive
        terms as this church may desire.

    2.  The number of deacons shall be established according as this church
        deems needful.

#### C. Duties {#deacon-duties}

The deacons shall earnestly endeavor to administer the business, practical
affairs, physical properties, and charitable ministries of this church so as to
relieve the pastor, allowing him to exercise oversight of the flock.

1.  The deacons shall each serve as Trustees and shall possess all such legal
    powers applicable to that designation.

2.  The pastor or a deacon shall sign all legal documents.

3.  The deacons are authorized to make expenditures without a vote of the
    church, up to the limit currently stipulated by the church.

4.  Two deacons shall count and record the offerings of the church, providing
    copies of the record to both the pastor and the church treasurer.

5.  At a maximum of one year intervals the deacons shall review, and make
    recommendation to the church regarding, the pastor's salary.

6.  The deacons shall assist the pastor in the selection of men as instructors
    for Bible classes.

7.  Should the deacons deem it necessary for the responsible transaction and
    stewardship of God's business among us, they may delegate to members any
    duties applicable to a treasurer and/or secretary. In accordance with the
    congregation-controlled nature of this church, these positions are filled
    subject to a vote of the church. In accordance with biblical teaching, the
    performance of the duties therein are under the direct authority and
    supervision of the deacons.

    1.  Qualifications of Treasurer and Secretary

        Must be members of this church, in good standing (i.e., not under
        church discipline), exhibiting holiness in the life as well as the
        appropriate skills usually associated with their position.

    2.  Selection of Treasurer and Secretary

        Upon selection by the deacons, approval shall consist of a vote of the
        church. There is no limit on the length of service in these positions.

        1.  Treasurer

            1.  Maintain an accurate and auditable record of the finances of the
                church.

            2.  Present a written report of receipts and disbursements to each
                quarterly business meeting of the church, as well as a summary
                report to the annual business meeting.

            3.  Receive and deposit all funds, weekly.

            4.  Maintain a detailed record of all individual contributions, and
                provide a written statement of offerings to each contributor in
                January following each calendar year.

        2.  Secretary

            1.  Maintain accurate records of the membership of the church, as
                well as all baptisms.

            2.  Maintain an accurate record of all business proceedings of the
                church, filing copies in two record books established for that
                purpose not more than two weeks following their occurrence.

            3.  Provide a reasonable amount of typing and office work as
                requested by the church, pastor, and deacons.

## Article Seven: Meetings {#meetings}

1.  Recognizing our Lord's promise to be in the midst of the gathering of his
    people, all such meetings should be carried out in a spirit of worship,
    striving for a holy unity

2.  Ordinarily, the church shall meet regularly every Sunday for a worship
    service that centers around the preaching of God's word. Also, the church
    shall facilitate Bible study and prayer through additional meetings that
    are ordinarily to occur on a weekly basis.  The church may schedule any
    other meetings that it deems needful.

3.  Meetings shall be held to facilitate the decent and orderly handling of
    God's business in this church. An annual business meeting shall be held,
    usually in January. Quarterly business meetings shall be held, usually in
    April, July, and October. Other business meetings may be called as required.

4.  No new items of business shall be recognized at any business meeting
    without the prior announcement in an agenda for said business meeting.

5.  Under normal circumstances, the elders may call for business meetings by
    unanimously approving and announcing an agenda.  However:

    1.  In the event of a vacant pastorate, the deacons may call for business
        meetings by unanimously approving and announcing an agenda.

    2.  At any time, 1/4 of the voting members may call for business meetings
        by unanimously approving and announcing an agenda.  Notice of such
        meeting must be given at least one week prior to the day appointed.

6.  The business meetings of the church are confidential. They shall be
    ordinarily attended only by the membership. Members are therefore permitted
    to speak of these matters only to other members.

7.  To the extent that they are an aid in the transaction of business, the
    general principles of Roberts' Rules of Order shall govern the parliamentary
    procedure unless restricted by this Constitution.

8.  A Quorum is defined as 1/3 of total membership.  Unless otherwise stated
    the required vote for all items of business (a vote of the church) shall be
    2/3 of the members present. Each member who is sixteen years of age or
    older shall possess an equal right to vote.

## Article Eight: Amendments {#amendments}

1.  Proposed amendments to this constitution shall be announced in a business
    meeting agenda at least thirty days prior to the business meeting scheduled.

2.  A vote of no less than 3/4 of the members present is required for the
    adoption of proposed amendments.

## Article Nine: Use of Facilities {#facilities}

Any such property owned by the Church shall be for the use of the Church and
its members. Parties, weddings and other uses of the property are limited to
members of the church and known guests of the members. Any other use of the
property must be approved by the elders and deacons. However, at no time may
the elders, deacons, other officers, or church members permit any individual or
group to use church facilities for any assembly or purpose contraindicated by
the Holy Scriptures or in fundamental nonconformity with the church's purpose
as outlined in Article 2 above.

## Article Ten: Dissolution {#dissolution}

In the event of dissolution of the corporation, the property of this church,
real, personal, and other assets, shall be distributed to another church with a
similar purpose to that detailed in Article Two of this constitution, and the
Articles of Incorporation of this church, as amended.

<style lang="scss">
ol ol {
  list-style-type: lower-alpha;
  //
  ol {
    list-style-type: decimal;
    //
    ol {
      list-style-type: lower-alpha;
    }
  }
}
</style>
