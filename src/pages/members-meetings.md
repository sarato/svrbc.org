---
title: Members’ Meetings
image: ../banners/pew-books.jpg
---

import meetings from '~/data/members-meetings.yml'
import Schedule from '~/components/Schedule'

Members’ meetings are for membership-related church announcements and
deliberations (ex. Acts 6:2, 1 Corinthians 5:4–5).  They are typically held on
Monday evening at [6:30pm]{.meridiem}.  Due to the nature of these meetings,
only current members may attend.

<schedule :dates="meetings.schedule" />
