/* eslint-env jest, browser */
/* global page */

import { describe, captureErrs, visit } from '../lib/pageutils'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('/ test', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  it('purpose exists', async () => {
    await expect(page).toMatch('Our purpose')
  })

  it('announcements load', async () => {
    await expect(page).toMatchElement('.placard')
  })

  it('easter egg works', async () => {
    const { errs, pageErrs, res } = await captureErrs(async () => {
      const keys = [
        'ArrowUp',
        'ArrowUp',
        'ArrowDown',
        'ArrowDown',
        'ArrowLeft',
        'ArrowRight',
        'ArrowLeft',
        'ArrowRight',
        'KeyB',
        'KeyA'
      ]
      for (const k of keys) {
        await page.keyboard.press(k)
      }
      return page.waitForSelector('.konami', { timeout: 500 })
    })
    expect(errs).toEqual([])
    expect(pageErrs).toEqual([])
    expect(res).not.toBeNull()
  })
})
