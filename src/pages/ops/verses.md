---
title: Verse formatter
---

<label>
  <input type="checkbox" v-model="style">
  Short format
</label>
<textarea v-model="verses"></textarea>

<script>
import bible from '~/lib/bible'
//
export default {
  data () {
    return {
      verses: '',
      style: false,
    }
  },
  methods: {
    format () {
      const style = this.style ? 'esv-short' : 'esv-long'
      try {
        this.verses = bible.reformat(this.verses, style)
      } catch (e) {}
    }
  },
  watch: {
    verses () {
      this.format()
    },
    style () {
      this.format()
    }
  }
}
</script>
