---
title: Nursery
---

Even the youngest children are welcome to stay in the sanctuary, but we also 
have a nursery during our 11am service for children 2 and younger, staffed with
at least two women.  You can choose to drop off your child, or to sit in the
nursery with them and hear the sermon through the speaker system.
