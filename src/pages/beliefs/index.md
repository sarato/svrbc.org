---
title: What we believe
image: ../../banners/confession.jpg
---

Put simply, we believe that all have sinned (Romans 3:23), and that the wages of
sin is death, but the free gift of God is eternal life in Christ Jesus our Lord
(Romans 6:23).  This is wonderful good news, and all true Christians share this
same gospel.

To describe ourselves more specifically, we are a Reformed Baptist church.
While we live in an era that eschews labels, we find this to be a helpful
designation.

## What is a Reformed Baptist? {#reformed-baptist}

Our church is _Reformed_, meaning that we hold to the principles of the
Protestant Reformation.  This includes the Five Solas:

**_Sola gratia_** - We are saved by God’s grace alone (not by our own merit)  
**_Sola fide_** - We are saved through faith alone (not through works)  
**_Solus Christus_** - We are saved in Christ alone (not with the help of any
  other mediator)  
**_Sola scriptura_** - The infallible guide to knowing and obeying God is
  Scripture alone  
**_Soli deo gloria_** - All things are accomplished to the glory of God alone  

* * *

It also includes the doctrine of salvation taught by the Reformers, often
summarized by the acronym TULIP:

**Total depravity** - Apart from God, our actions and motivations are tainted by
  sin, so that we cannot do anything pleasing to him, including seeking him.
  (Romans 3:10-11)  
**Unconditional Election** - God’s salvation of his people is not based on
  anything good in them or anything they have done. (Ephesians 1:4; 2:8-9)  
**Limited Atonement** - Jesus died and rose again specifically for his
  followers, and this sacrifice saves all for whom it was made.  (John 10:15;
  17:9)  
**Irresistible Grace** - When God effectually calls one to salvation, he
  transforms their will so they do not desire to reject him.  (John 6:37)  
**Preservation of the Saints** - God’s salvation of a person cannot be thwarted,
  and includes preserving believers in holiness. (John 10:28)

* * *

In addition to being Reformed, we are a _Baptist_ church, meaning that we hold
to several Baptist distinctives, including:

**Regenerate church membership** - While only God knows the heart, we believe
membership in a local church should be for those who have made a credible
profession of faith, and who have been baptized upon profession.  
**Congregational government** - Decisions related to the integrity of the gospel
should be made by the congregation, under the guidance of the elders.

## Our standards {#standards}

We believe that the Bible has the ultimate authority to determine what we
believe and we do.

That being said, many that affirm this same Bible arrive at different
conclusions on what it teaches.  In light of this reality, to promote clarity
and transparency, we have adopted several written standards that express our
understanding of the Bible’s vision for the church.

1.  [Our confession of faith](https://1689londonbaptistconfession.com/all-chapters) (the Second London Baptist Confession) defines the official teaching of the
    church.

2.  [Our constitution](/constitution/) governs the administration of the church.

3.  [Our membership covenant](/constitution/#covenant) (contained within the
    constitution) conveys the practical holiness we strive for in the life of
    the church.

Additionally, while it is not an official standard of our church, we use
[the Baptist Catechism](https://baptistcatechism.org/) as a tool to teach the
basics of the faith.

<p v-if="v1">
  If you would like to learn more, please consider attending our weekly
  [SVRBC Orientation](/learning/#orientation).
</p>

<!-- TODO: Contact? -->

<script>
import Debug from '~/mixins/debug'
//
export default {
  mixins: [
    Debug
  ]
}
</script>
