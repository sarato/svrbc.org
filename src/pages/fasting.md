---
title: Corporate Fasting
image: ../banners/pew-books.jpg
---

import V from '~/components/PassageRef'

From time to time, in response to the providence of God, SVRBC holds corporate
fasts.  On such occasions the pastors of the church provide a schedule as well
as a list of things to pray for.

## The purpose of fasting {#purpose}

Fasting is meant to enliven our prayers by humbling ourselves.  One might well
pray without fasting, but fasting without prayer is merely not eating.

Fasting is a denial of the pleasures of life, primarily food; it serves to
sharpen our focus on whatever great matter we seek God’s face, which we set
before Him by prayer.

Fasting is a call to God to have our voice heard on high (<v>Isaiah
58:3-4</v>).

## The practice of fasting {#practice}

One who fasts should typically fast from:

-   Food
-   Flavored drinks or alcohol (water is permitted)
-   Entertainment
-   Sex
-   Any unnecessary pleasures, though they may be acceptable at other times

As appropriate, the one fasting should fill up his time with prayer.

## Health concerns and prior engagements {#health}

Some may not be able to cut out food altogether (e.g., nursing mothers, the
sick, or the elderly) and others may have prior engagements that cannot be
reasonably broken.

If this describes your situation, simply participate as you are able to deny
yourself the pleasures of life without any threat to your health. As one
alternative, you may opt for less pleasant food (e.g. vegetables) rather than
comfort food or delicacies (see <v>Daniel 10:2-3</v>).

Those who fast as they are able are full participants; if you cannot gather
with the rest of the church for prayer or cannot fast fully, then join in as
you are able and we can all be confident that we are all praying together.

## Families and children {#families}

Corporate fasts include the household (see <v>Jonah 3:7-8</v>).  If you are the
head of your household, do encourage all to participate by engaging in prayer
and refraining from pleasures.

For young children, as stated above, it may be reasonable to pursue alternative
means of refraining from pleasures rather than cutting out food altogether.

## Our disposition toward others {#disposition}

Do not use fasting as a reason to place  burdens on others (<v>Isaiah
58:3-4</v>).

Do not appear gloomy (<v>Matthew 6:16-17</v>).

Do not broadcast or advertise our activity outside the church, lest our reward
be lost (<v>Matthew 6:18</v>).
