---
title: Ministries
image: ../../banners/woodworking.jpg
---

import PassageRef from '~/components/PassageRef'

God has blessed SVRBC with much, and we rejoice in the opportunities we have to
share those blessing with others.

## Biblical counseling {#counseling}

We believe the Word of God is sufficient to navigate all of life's trials.
Equipped with [ACBC certification](https://biblicalcounseling.com/about/) our
counselors have helped many people apply God's word to their struggles and life
situations.

[Learn more about our counseling ministry.](/counseling/)

## Community Food Distribution {#food}

Every Saturday, we distribute food to anyone in the community who needs it.

[Learn more, or sign up to be notified of future distributions.](/community-food-distribution/)

## Monthly Singles' Dinners {#singles}

SVRBC organizes and hosts monthly events for Christian singles in the area.
These events include dinner, a brief message, organized discussion time, and
time for fellowship.  [Learn more.](/singles/)

## Rides to church {#rides}

If you have limited transportation and want to attend SVRBC, we’d love to help!
We regularly give rides to students and others without cars.

Contact us at [info@svrbc.org](mailto:info@svrbc.org) to arrange a ride.

## Intern and new grad housing program {#intern-housing}

Every summer, tens of thousands of young adults flock to the SF bay area, where
the jobs are plentiful but the housing is not.  SVRBC would like to step in and
help!  Several families are available to host an intern or a new graduate for
anywhere from a few weeks to several months.

This program offers not only free housing, but an immediate base of support in
a local church.  Young singles will have an opportunity to interact closely
with a godly family, and in return, we can be encouraged by their presence and
fellowship!

[Learn more.](/intern-housing/)

## Silicon Valley Areopagus Forum {#svaf}

SVRBC hosts and organizes the [Silicon Valley Areopagus
Forum](http://svaforum.org), a quarterly discussion panel where pastors or
secular thought-leaders from the area get together to discuss a set topic.

Learn more at [svaforum.org](http://svaforum.org)

## End Abortion Sidewalk Ministry {#life}

God has called us to “Rescue those who are being taken away to death” and “hold
back those who are stumbling to the slaughter”
(<passage-ref>Proverbs 24:11</passage-ref>),
regardless of the what the world thinks.

To this end, SVRBC conducts sidewalk ministry in front of abortion clinics in
order to—by God’s grace and within the limits of the law—call men and women to
repentance.  This includes those who would have their babies in utero killed at
abortion clinics and even those that support abortion by working at or visiting
such places of death.  Our approach is based on [End Abortion
Now](https://endabortionnow.com/).

We provide needed information and help to those going into abortion clinics,
especially during operating hours.  By pointing people to alternative services
and solutions, we aim to turn visitors and workers away from any organization
that would engage in the murder of innocent humans in their mothers’ wombs.
Alternative services include adoption, ultrasounds, pregnancy tests, and other
medical and non-medical services from Christian organizations that do not
perform abortions.

More importantly, we offer the life-giving gospel message at this point of life
and death.  We do not merely desire to save physical lives, but also to be used
of the Lord to save souls from eternal condemnation.  We pray that all under
the sound of God’s good news would turn to Christ for forgiveness of sins and
the eternal life that is only found in him.

Please consider joining us in this vital ministry, and pray with us that all
the abortion mills in the San Francisco Bay Area would cease operations due to
a lack of any “patients” or workers.  Moreover, pray that many would seek true
repentance and would receive God’s gift of saving faith.

Learn more at [rescuetheinnocent.org](https://rescuetheinnocent.org).
