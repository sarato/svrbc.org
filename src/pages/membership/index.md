---
title: Membership
image: ../../banners/keys.jpg
bannerstyle: dark
---

> Now you are the body of Christ and individually members of it. 
>
> [1 Corinthians 12:17 (ESV)]{.citation}

Being a follower of Christ means being united with other followers of Christ.
Practically, this happens through church membership.  Maybe you’ve never
thought about church membership before or maybe you are skeptical.  If that’s
the case, we’ve collected [a list of biblical reasons to affirm local church
membership](/articles/2020-12-21/is-church-membership-biblical/).

On the other hand, perhaps you are warm to the idea or even convinced that
church membership is a biblical imperative.  If that’s the case we’re glad you
are interested!  It’s something we value highly, and we look forward to the
prospect of you joining us in that relationship!

## The Orientation class {#orientation}

The first step in the membership process is to complete the Orientation class,
which runs at 9:30 on Sundays, the same time as the other Sunday school
classes.  However, the Orientation class is not just for those interested in
membership.  We encourage all newcomers to SVRBC to learn about the beliefs and
practices of our church by attending.

In this class, we cover:

1.  The church’s [confession of faith](/confession/) (6-8 lessons, depending
    on discussion)
2.  The church’s [constitution](/constitution/) (1 lesson)
3.  The church’s authority structure and decision-making process (1 lesson)

You may join at any time.  We typically cover the material in order, but we
keep track of the material completed so that we can jump around, depending on
which students are present.

Additionally, any students pursuing membership who are eager to complete the
course are encouraged to schedule additional sessions with a teacher.

> Go therefore and make disciples of all nations, baptizing them in the name of
> the Father and of the Son and of the Holy Spirit, teaching them to observe
> all that I have commanded you.
>
> [Matthew 28:19-20a (ESV)]{.citation}


## Applying for membership {#applying}

Completed the Orientation class and interested in becoming a member?  Great!
Please fill out [the membership application form](apply/) to get started.
Afterward, a pastor will schedule a meeting with you to discuss your
application.

The purpose of this meeting will be to:

-   Ensure you understand and meet the qualifications of membership.
    -   Do you believe the gospel?
    -   Have you been baptized or do you want to be baptized?
    -   Are you pursuing a holy life?
-   Ensure you understand the purpose and responsibilities of membership.
    -   Do you plan to be present for church meetings?
    -   Do you plan to be sacrificial with your time and resources for the sake
        of other church members?
-   Ensure we understand how to best care for you.
    -   Do you have any particular needs we should be looking out for?
    -   Do you have any particular gifts we should help you put to use in
        service of the church?

> Pay careful attention to yourselves and to all the flock, in which the Holy
> Spirit has made you overseers, to care for the church of God, which he
> obtained with his own blood.
>
> [Acts 20:28 (ESV)]{.citation}

Before the meeting, you should take a moment to review
[the membership covenant](/constitution/#covenant).

## Giving a testimony {#testimony}

When someone gives a testimony, they make a public declaration of the great
things God has done in salvation, and, in particular, how God has saved them.
It is called a testimony because, like a courtroom trial with a large audience,
we are called to witness to an unbelieving world of the truth of the gospel.

> And with great power the apostles were giving their testimony to the
> resurrection of the Lord Jesus, and great grace was upon them all.
>
> [Acts 4:33 (ESV)]{.citation}

At SVRBC, prospective members give their testimony to the church before
entering membership.  This typically occurs before the afternoon service on the
Lord’s day.  Since the congregation decides who to receive into membership,
this testimony helps them make an informed affirmation of the work of God in a
person’s life, but also provides them with everything they need to celebrate a
fellow believer’s salvation.

A testimony should be 3–5 minutes long and include the following elements:

-   How you came to know the Lord
    -   What circumstances led you to hear the gospel?
    -   What circumstances led you to believe the gospel?
-   Your understanding of the gospel
    -   What is salvation?
    -   What did you do to be saved?
    -   How do you know you are saved?
-   What the work of the Holy Spirit has looked like in your life
    -   Have you repented of your sin?
    -   Are you growing in your understanding of God’s word?
-   Your baptism
    -   If you have not been baptized:
        -   Do you understand your need for baptism?
        -   Do you desire to be baptized?
    -   If you have been baptized:
        -   Were you a believer at the time?
        -   What church conducted the baptism?
-   Why you would like to join this particular church
    -   Do you find yourself in relative agreement with our doctrine?
    -   How do you believe God can use us to help you grow in your faith?
    -   How do you believe God can use you to help this particular body?

At a regular members’ meeting ([typically held on the first Monday of each
quarter](/members-meetings/)), the congregation will consider the testimony and
vote to formally affirm the prospective member as brother or sister in Christ.

> Truly, I say to you, whatever you bind on earth shall be bound in heaven, and
> whatever you loose on earth shall be loosed in heaven. Again I say to you, if
> two of you agree on earth about anything they ask, it will be done for them
> by my Father in heaven. For where two or three are gathered in my name, there
> am I among them.”
>
> [Matthew 18:18-20 (ESV)]{.citation}

## Becoming a member {#becoming}

After a successful vote, a candidate becomes a member at the end of the next
afternoon service they are present for.  About to enter membership?  This is
what you can expect:

1.  If you have not already been baptized, a pastor will baptize you at the end
    of the afternoon message.
    1.  Please arrive early; the pastor will want to chat with you before the
        service.
    2.  The pastor will ask you a short set of yes or no questions, giving you
        an opportunity to publicly declare your faith.
    3.  You will have a chance to change clothing afterward.

<div class="wrap-list full-width">
  <g-image
    src="./mario-towel.jpg"
    alt="a man being baptized"
  />
  <g-image
    src="./mario-hug.jpg"
    alt="a man being baptized"
  />
  <g-image
    src="./quinn-descending.jpg"
    alt="a woman being baptized"
  />
  <g-image
    src="./quinn-ascending.jpg"
    alt="a woman being baptized"
  />
</div>

2.  You will be asked to sit up front where a pastor will publicly pray for you
    and for God to bless this new addition to our church.
3.  You will receive the Lord’s supper along with the rest of the church.
4.  After the benediction, the members of the church (and anyone else who
    desires to) will come up to greet you with congratulations.  This is often
    referred to as giving the right hand of fellowship.

> and when James and Cephas and John, who seemed to be pillars, perceived the
> grace that was given to me, they gave the right hand of fellowship to
> Barnabas and me,
> [Galatians 2:9a (ESV)]{.citation}

This is one of the most joyous occasions in the life of the church.  If you are
pursuing membership here, we look forward to welcoming you into this local
fellowship!

## Transferring to another church {#transferring}

The time may come when the Lord calls you to a different fellowship!  Because
we believe that every Christian should be a member of a church and privileges
like access to the Lord's supper are contingent on church membership, we
encourage members to transfer rather than simply terminating their membership.
The one who goes through the transfer process retains the privileges of
membership while being excused from the responsibilities of regularly gathering
in order to pursue a new church family.  Under this arrangement, we continue
to pray for the transitioning members and provide pastoral care.

[More details](/constitution/#transfer) about this process may be found in our
constitution.

<style scoped lang="scss">
.full-width {
  position: relative;
  width: var(--fdw);
  left: calc(-0.5 * (var(--fdw) - 100%));
  //
  img {
    height: 15rem;
    width: auto;
    object-fit: cover;
  }
}
</style>
