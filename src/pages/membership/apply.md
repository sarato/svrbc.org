---
title: Apply for membership 
image: ../../banners/keys.jpg
bannerstyle: dark
---

So you are considering [membership at SVRBC](/membership/)… Great!  Assuming
you’ve been through the orientation class, the next step is to complete the
following application form, describing your understanding of and commitment to
the gospel.

After you’ve submitted the application, follow up with a pastor in person about
next steps.  If you have any questions, don’t hesitate to
[send one of us an email](/leadership/#contact) and ask. 

<data-form :template="template">
  <template v-slot:covenant-text>
    Our
    <g-link to="/constitution/#covenant">membership covenant</g-link>
    is our agreement with one another to pursue a holy fellowship.  Because
    those who become members must commit to the following items, those who
    apply should be prepared to do so.  If you have any questions about this
    covenant, please speak with a pastor.
  </template>
  <template v-slot:c1-label>
    Are you willing to strive for the advancement of this church in knowledge,
    holiness, and comfort, promoting its prosperity and spirituality, making it
    preeminent in your associations?<br>
    <passage-ref>John 13:14</passage-ref>;
    <passage-ref>Rom. 13:8–10</passage-ref>;
    <passage-ref>1 Cor. 12:1–12</passage-ref>;
    <passage-ref>Phil. 1:9–11</passage-ref>;
    <passage-ref>Eph. 4:13–16</passage-ref>;
    <passage-ref>1 Thess. 4:9</passage-ref>;
    <passage-ref>Heb. 12:14</passage-ref>;
    <passage-ref>1 Pet. 1:14–16</passage-ref>
  </template>
  <template v-slot:c2-label>
    Are you willing not to forsake the assembling of ourselves, but faithfully
    sustain through both attendance and prayer our public worship of God, while
    upholding the ordinances, doctrines, and discipline of this church?<br>
    <passage-ref>Matt. 28:19</passage-ref>;
    <passage-ref>Heb. 10:23–25; 13:7, 17</passage-ref>
  </template>
  <template v-slot:c3-label>
    Are you willing to regularly and cheerfully contribute, according as God
    has prospered you, to the expenses of this church, and the support of its
    ministries?<br>
    <passage-ref>1 Cor. 9:6–14; 16:2</passage-ref>;
    <passage-ref>2 Cor. 9:7</passage-ref>;
    <passage-ref>1 Tim. 5:17–18</passage-ref>
  </template>
  <template v-slot:c4-label>
    Are you willing to cultivate private and family worship at home, striving
    to attain unto the scriptural example for yourself and your family,
    bringing up your children in the nurture and admonition of the Lord,
    training them for the service of Christ?<br>
    <passage-ref>Deut. 6:4–12</passage-ref>;
    <passage-ref>Prov. 22:6</passage-ref>;
    <passage-ref>Eph. 5:22—6:4</passage-ref>;
    <passage-ref>Col. 3:18–20</passage-ref>
  </template>
  <template v-slot:c5-label>
    Are you willing to exercise a Christian care and watchfulness over other
    members of the church, faithfully warning, exhorting, and admonishing
    others in love as occasion may require, being slow to take offense and
    always ready for reconciliation, endeavoring to maintain a scriptural unity
    others?<br>
    <passage-ref>Col. 3:8–17</passage-ref>;
    <passage-ref>Heb. 3:12–13; 10:24</passage-ref>
  </template>
  <template v-slot:c6-label>
    Are you willing to, being jealous of the honor of God's name as reflected
    through this church, walk circumspectly in the world, be equitable in your
    dealings, faithful in your engagements, exemplary in your deportment,
    guarding your speech from all sins of the tongue, abstaining from the
    misuse of all drugs?<br>
    <passage-ref>Rom. 12:1–2</passage-ref>;
    <passage-ref>Eph. 5:1–13</passage-ref>;
    <passage-ref>James 1:26–27; 3:2–12</passage-ref>;
    <passage-ref>Col. 3:8–10</passage-ref>
  </template>
  <template v-slot:c7-label>
    Are you willing to, upon leaving the membership of this church, as soon as
    possible, unite with another church of like faith where you can carry out
    the spirit of this covenant and the principles of the word of God?<br>
    <passage-ref>Acts 1:15; 2:41–42, 47; 9:26–28</passage-ref>
  </template>
  <template v-slot:c8-label>
    Are you willing to, in all conditions, strive to live to the glory of Him
    who hath called you out of darkness into His marvelous light?<br>
    <passage-ref>1 Cor. 10:31</passage-ref>;
    <passage-ref>Col. 1:12–13</passage-ref>;
    <passage-ref>1 Pet. 2:9</passage-ref>
  </template>
</data-form>

<script>
import DataForm from '~/components/DataForm'
import PassageRef from '~/components/PassageRef'
import template from '~/data/forms/membership-application.yml'
//
export default {
  components: {
    DataForm,
    PassageRef
  },
  data() {
    return {
      template
    }
  }
}
</script>
