---
title: Community Food Distribution
image: ../../banners/mom-kids.jpg
---

![Food Distribution](./food.jpg){.content-wide .content-float-right .content-top}


Every Saturday, SVRBC distributes surplus food from Trader Joe's,
Whole Foods, and other local markets.  This service is provided on a
first-come, first-serve basis, free to anyone!

We'll be open at 11[am]{.meridiem}, every Saturday.
Hope to see you there!

Please fill out the [Visitor Registration Form](https://docs.google.com/forms/d/1r-NJ6uw_9BnvFP62DLBmVV3rodYDEIBSZLy0PYja7VU) before you come.

## Sign up for notifications! {#signup}

Please leave your name and email if you'd like to be informed of future food
deliveries.

<data-form data-button-text="Sign up" :template="fooddistributionSignup" />

<script>
import DataForm from '~/components/DataForm'
//
export default {
  components: {
    DataForm,
  },
  data() {
    return {
      fooddistributionSignup: {
        title: 'Food Distribution signup',
        to: 'sarah',
        fields: [],
        success: 'Thank you for your interest!'
      }
    }
  }
}
</script>

<style lang="scss" scoped>
table {
  border-spacing: 0;
}
//
thead {
  display: none;
}
//
td {
  padding-right: 1rem;
}
</style>
