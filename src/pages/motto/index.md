---
title: A deeper knowledge of Christ
image: ../../banners/map-book.jpg
---

As the ministry of the apostles sunsetted, Peter left the church with these
words:

> Therefore, beloved, since you are waiting for these \[new heavens and
> earth], be diligent to be found by him without spot or blemish, and at
> peace. And count the patience of our Lord as salvation, just as our beloved
> brother Paul also wrote to you according to the wisdom given him, as he does
> in all his letters when he speaks in them of these matters. There are some
> things in them that are hard to understand, which the ignorant and unstable
> twist to their own destruction, as they do the other Scriptures.  You
> therefore, beloved, knowing this beforehand, take care that you are not
> carried away with the error of lawless people and lose your own stability.
> **But grow in the grace and knowledge of our Lord and Savior Jesus Christ.**
> To him be the glory both now and to the day of eternity. Amen.
>
> [2 Peter 3:14–18 (ESV)]{.citation}

Peter speaks of the grace and knowledge of Christ.  And since we experience the
grace of Christ through knowing him, these two are not so distinct.

> May grace and peace be multiplied to you in the knowledge of God and of Jesus
> our Lord.
>
> <p class="citation">2 Peter 1:2 (ESV)</p>

What would guarantee the security of the people of God as they await the return
of Jesus?  Knowledge of Christ.  What must we hold on to?  Knowledge of Christ.
All errors of thought and practice are cured by this one thing.  While the
world is full of false ideologies and many twist the word of God, we have been
called to guard the truth handed down to us and know Christ more deeply.

This is not a mere intellectual pursuit, but a profoundly practical matter that
involves the whole person and shapes daily life.  To know Christ is to be
familiar with him as one is with a friend.  It is to experience his goodness to
its fullest.

At Silicon Valley Reformed Baptist Church, we desire nothing more for ourselves
than a deeper knowledge of Christ.  Likewise, we desire nothing more for _you_
than a deeper knowledge of Christ!

We are by no means perfect people, but if you visit us, we hope that you will
find us discussing, contemplating, and cherishing our Savior.  Furthermore, we
hope that you will find a rich storehouse of that knowledge here, beginning
with [our foundational beliefs and distinctives](/beliefs/).  We invite
you to join us as we prepare for and await his return.
