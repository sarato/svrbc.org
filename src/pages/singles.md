---
title: Monthly Singles Dinner
image: ../banners/puffins.jpg
bannerstyle: light
---

>  He who finds a wife finds a good thing and obtains favor from the LORD.
>
> <p class="citation">Proverbs 18:22 (ESV)</p>

Want to make meaningful connections with other marriage-minded Christian
singles?  SVRBC hosts free monthly singles' dinners!  Events include dinner,
a brief message, organized discussion, and free time.

Next Dinner:<br>
Friday, **August 25**, 6:00 [pm]{.meridiem}

[RSVP here](https://www.eventbrite.com/e/free-christian-singles-dinner-tickets-572042252777){.button}

Events are held at Silicon Valley Reformed Baptist Church:<br>
709 Lakewood Dr.<br>
Sunnyvale, CA 94089

