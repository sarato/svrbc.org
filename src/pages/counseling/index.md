---
title: Free Biblical Counseling
image: ../../banners/small-plant.jpg
bannerstyle: dark
---

Biblical counseling (also known as nouthetic counseling) is counseling based on
the Bible.  The Bible is the Word of God, and contains everything needed for
life and godliness (2 Peter 1:3).  Therefore, it is sufficient to navigate all
of life’s trials.  The goal of biblical counseling is to help struggling people
overcome their difficulties by grasping the wisdom and hope God has given us in
his word.

Equipped with [ACBC certification](https://biblicalcounseling.com/about/) our
counselors have helped many people apply God's word to their struggles and life
situations.

[Sign up for counseling](signup/){.button}

<!-- TODO: testimonials -->

## There is Hope {#hope}

It is God's will for people to live lives of joy and peace, even in the midst
of difficult circumstances.  When a person willingly submits to God's wisdom,
and clings to his promises, there is hope for true and lasting change
(Philippians 1:6). He equips his people to help one another, admonishing and
exhorting one another to grow in holiness.

> I myself am satisfied about you, my brothers, that you yourselves are full
> of goodness, filled with all knowledge and able to instruct one another.
>
> [Romans 15:14 (ESV)]{.citation}

**Free of charge:** The congregation of SVRBC has banded together to provide
this service to the public at no cost to counselees.  We have freely received a
wonderfully good news from Jesus Christ, and it is in that spirit that we wish
to provide it to others (Matthew 10:8).

**Available to all:** Although we welcome those of any religious background,
our counsel will be based on God's word found in the Bible.  We believe that
man's wisdom is insufficient to assist a counselee in genuine change, and that
all genuine heart change depends on the work of God through his Holy Spirit.
(Ezekiel 36:25-27).  Therefore, all our counsel will be based in biblical
principles, and will be accompanied by prayer.

**Addressing all issues:** Our counselors have many years of combined
experience, and have been able to help them with a wide variety of issues.
Some common reasons people seek counseling include:

-   [marital conflict](marriage/)
-   [parenting struggles](family/)
-   [depression](depression/)
-   [anxiety](anxiety/)
-   [anger](anger/)
-   [alcohol addiction](alcohol-addiction/)
-   [drug addiction](drug-addiction/)
-   [pornography addiction](pornography-addiction/)
-   and much, much more.

No matter what you are struggling with, we are confident that God's word is
sufficient to answer any difficulty.

## What to Expect {#expect}

SVRBC is committed to providing counselees the best we have to offer.

**Privacy and Respect:** You will be treated with respect and care.  Everything
said in counseling will be held with strict confidence, except when there is
biblical requirement to involve others (e.g.  criminal activity).  See
[our privacy policy](privacy/) for more information.

**Logistics:** Counseling sessions last between 1 and 1.5 hours, and are
scheduled around the schedule of the counselor and counselee, usually during
weekday business hours.

**Homework:** To encourage growth in Christlikeness, you will be assigned
weekly homework.  The homework will vary case-by-case, but will be chosen with
a view of establishing godly habits and lasting change.

**Results:** Habits are not broken or formed overnight.  True change does not
involve only learning information, but putting it into practice over periods of
time.  The average successful counseling case is able to conclude after 3 or 4
months of weekly sessions.

If you have been blessed by the counseling ministry at SVRBC or you know
someone who is currently counseling with us, please consider
[the various ways you can partner with us](partnership/).

## Meet the counseling team {#team}

<div class="wrap-list portraits">
  <leader
    name="Josh Sheldon"
    :img="$static.joshSheldon.image"
    area="Pastor, ACBC certified counselor"
  />
  <leader
    name="Sarah Owens"
    :img="$static.sarahOwens.image"
    area="ACBC certified counselor"
  />
</div>

## Testimonials {#testimonials}

<testimonial name="Johanna">
  <template slot="image">
    <g-image src="./johanna-y.testimonial.jpg" alt="Portrait of Johanna" />
  </template>
  My situation was intricate -
  <strong>Pastor Josh handled it with grace and patience</strong>.
  With biblical wisdom, he guided me to a happy resolution.  I deeply
  appreciate his help!
</testimonial>

<testimonial name="Lucy">
  <template slot="image">
    <g-image src="./lucy-m.testimonial.jpg" alt="Portrait of Lucy" />
  </template>
  I'm really thankful I can have one-on-one with a woman counselor, it
  has really helped me in my healing process.  I've never felt such caring
  and compassion, and <strong>I don't feel as broken as I was</strong>.
  I am so grateful that they provide these services at SVRBC.
</testimonial>

## Ask a question {#question}

Not quite sure whether you are ready sign up for counseling?<br>
Have some other questions?<br>
Please send us a message; we’d love to hear from you!

<contact-form
  to="info"
  subject="Message about counseling"
  persist-key="/counseling"
/>

<static-query>
query {
  joshSheldon: srcImage(id: "people/josh-sheldon.jpg") { image }
  sarahOwens: srcImage(id: "people/sarah-owens.jpg") { image }
  pamWood: srcImage(id: "people/pam-wood.jpg") { image }
}
</static-query>

<style lang="scss" scoped>
.portraits {
  --flex-spacing: var(--inline-margin);
  //
  margin-top: 1rem;
  //
  > * {
    flex-grow: 0;
  }
}
//
ul {
  padding-left: 0;
  //
  li {
    display: inline-block;
    float: left;
    width: 15rem;
    //
    &::before {
      content: "• ";
    }
  }
  //
  &::after {
    content: "";
    clear: both;
    display: table;
  }
}
</style>

<script>
import ContactForm from '~/components/ContactForm.vue'
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
import Leader from '~/components/Leader.vue'
import Testimonial from '~/components/Testimonial'
//
export default {
  components: {
    ContactForm,
    CovidCounselUpdate,
    Leader,
    Testimonial
  }
}
</script>
