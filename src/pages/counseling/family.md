---
title: Free Biblical Family Counseling in Sunnyvale
image: ../../banners/mom-kids.jpg
---

<covid-counsel-update />

Do you have discipline issues with younger children?  Rebellion or lack of
communication from your teenagers?  Do you as a parent struggle with anger,
frustration, or lack of patience in your parenting?  Our Christian counselors
can walk you through practical steps to improve your relationship with your
family, and give you confidence in your parenting.

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
