---
title: Free Biblical Anxiety Counseling in Sunnyvale
image: ../../banners/bible-wall.jpg
---

<covid-counsel-update />

God's word calls us to be "anxious for nothing", but this is often easier
said than done.  Even those who trust the Lord can find themselves full of
fear, anxiety, and worry.  Do you find yourself anxious sometimes without
cause?  Is worry causing you insomnia and sleepless nights?

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
