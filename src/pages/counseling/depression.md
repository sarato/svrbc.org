---
title: Free Biblical Depression/Suicide Counseling in Sunnyvale
image: ../../banners/man-bridge.jpg
---

<covid-counsel-update />

Depression is something many people struggle with, even those who love the
Lord.  Many Christians know they have so much to be joyful for, they can read
psalms commanding them to be joyful, and still feel trapped by sadness and
even suicidal thoughts.  While some turn to antidepressants to regulate
symptoms and help physically, we believe God's word has the ability to bring
life and joy spiritually.

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
