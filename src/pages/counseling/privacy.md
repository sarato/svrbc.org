---
title: Privacy in Counseling
image: ../../banners/small-plant.jpg
bannerstyle: dark
---

We understand that people sometimes seek counseling for very personal and
sensitive issues. Our counselors at SVRBC are trustworthy and honest, and to
the greatest extent possible will preserve your privacy and confidentiality.

However, there are some situations where the Bible may require your counselor
to share information with others:

-   If the counselor is uncertain how to best address a problem, they may need
    to seek advice from another pastor or counselor (Proverbs 11:14, 24:6).  In
    this situation, as few details as possible will be shared to preserve the
    privacy of the counselee.

-   If there is an immediate possibility of abuse or harm to another, the
    counselor must take reasonable precautions to prevent harm, which may
    include alerting the authorities, or alerting the person in danger (Romans
    13:1-5).

-   When counseling about _ongoing_ sin which has consequences to close family
    relations (e.g. counseling a teenager who repeatedly steals from their
    parents, or a spouse who is continuing to commit adultery), the counselor
    will encourage the counselee to inform the affected family member, and may
    inform them if the counselee refuses (Ephesians 5:22, Ephesians 6:1, 1
    Corinthians 7:4).

-   If the counselee is a church member who becomes hardened in a particular
    sin, and does not repent when called to do so, the counselor will have to
    inform others of the sin to begin the process of church discipline (Matthew
    18:16).  This is done with the goal of encouraging the counselee toward
    repentance.

Even when required to share information, SVRBC counselors will use their
discretion and only share what is required to ensure the best outcome for your
counseling case.

[Learn more about counseling at SVRBC.](../)
