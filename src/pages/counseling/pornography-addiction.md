---
title: Free Biblical Pornography Addiction Counseling in Sunnyvale
image: ../../banners/dark-computer.jpg
---

<covid-counsel-update />

Addiction to pornography is all too common, and its effects are devastating.
It can destroy marriages and ruin true sexual intimacy, leaving nothing but
shame and emptiness.

Are you ready to beat your addiction?  Do you want help and accountability
in your struggle?

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
