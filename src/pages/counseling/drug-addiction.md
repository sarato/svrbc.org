---
title: Free Biblical Drug Addiction Counseling in Sunnyvale
image: ../../banners/hands-face.jpg
---

<covid-counsel-update />

Trapped in the cycle of drug addiction?  Tried quitting, but nothing seems to
work?  Do you want to take control of your life and your choices?

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
