---
title: Rides to church
---

If you have limited transportation and want to attend SVRBC, we’d love to help!
We regularly give rides to students and others without cars.

<!-- TODO: pickup estimate -->

<!-- TODO: injections -->

<!-- TODO: button aesthetics -->

<!-- TODO: rides group -->

<!-- TODO: style final error message -->

<button-area>
  <span class="dark-button" @click="showRequest=true">Request a ride</span>
  <div class="sub-action">
    <p>Want to help provide or coordinate rides?</p>
    <span class="button" @click="showVolunteer=true">Volunteer</span>
  </div>
</button-area>

<hidden-form-container v-model="showRequest">
  <request-a-ride />
</hidden-form-container>

<!--hidden-form-container v-model="showVolunteer">
  <mail-form to="conley@svrbc.org">
    <form-input label="Drive">
      <label>Join the driving rotation</label>
      <input type="checkbox" name="drive" v-model.trim="drive">
    </form-input>
    <form-input>
      <label>Help coordinate rides</label>
      <input type="checkbox" name="coordinate" v-model.trim="coordinate">
    </form-input>
    <form-input>
      <label>Comments</label>
      <textarea name="comments" />
    </form-input>
  </mail-form>
</hidden-form-container-->

<style lang="scss" scoped>
.sub-action {
  display: inline-block;
  width: 15rem;
  text-align: center;
  padding: 1rem;
  margin-top: 1rem;
}
//
.sub-action .button {
  margin: 1rem auto;
}
//
@media only screen and (min-width: 38rem) {
  .input-group-left,
  .input-group-right {
    float: left;
    width: 16rem;
    margin-top: 0;
  }
  //
  .input-group-left {
    padding-right: 0.5rem;
  }
  //
  .input-group-right {
    padding-left: 0.5rem;
  }
}
</style>

<script>
import ButtonArea from '~/components/ButtonArea'
import FormInput from '~/components/FormInput'
import HiddenFormContainer from '~/components/HiddenFormContainer'
import MailForm from '~/components/MailForm'
import RequestARide from '~/forms/RequestARide'
//
export default {
  components: {
    ButtonArea,
    FormInput,
    HiddenFormContainer,
    MailForm,
    RequestARide
  },
  data () {
    return {
      coordinate: true,
      comments: '',
      drive: true,
      showRequest: false,
      showVolunteer: false
    }
  },
  methods: {
    compose (values) {
      return {
        subject: `${values.name} would like to volunteer for the rides program!`,
        body: `Name: ${values.name}
Drive: ${values.drive}
Coordinate: ${values.coordinate}
`
      }
    },
  },
}
</script>
