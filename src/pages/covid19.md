---
title: Virtual Meetings
subtitle: Covid-19 Update
bannerstyle: dark
image: ../banners/stadium.jpg
---

SVRBC has resumed all in-person meetings, with virtual options still available.
[Home groups](/groups/) are meeting at various Bay Area locations, with one
meeting virtually.  The schedule below has details for this, and our other
virtual meetings.

|                        |                                                                                       |
| ---------------------- | ------------------------------------------------------------------------------------- |
| **Sunday @ 11:00am**   | Worship service ([view livestream](/live/)) ([view bulletin](/bulletin/))             |
| **Tuesday @ 7:00pm**   | Virtual home group ([join with Zoom](https://zoom.us/j/3220753228))                   |

If you want to join the zoom meetings, please contact
[jgmancilla@svrbc.org](mailto:jgmancilla@svrbc.org){.no-ext-warn} for the
password.

<style scoped>
td {
  padding-right: 1rem;
}
</style>
