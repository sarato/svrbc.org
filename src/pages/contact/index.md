---
title: Contact
image: ../../banners/rotary-phone.jpg
bannerstyle: dark
---

import ContactForm from '~/components/ContactForm.vue'

<div class="contact-info">
<div class="contact-info-box contact-info-address">
  <div class="icon">
    home
  </div>
  <div>
    709 Lakewood Drive<br>
    Sunnyvale, CA 94089
  </div>
</div>
<div class="contact-info-box">
  <div class="icon">
    phone
  </div>
  <div>408-734-2297</div>
  <div class="icon">
    email
  </div>
  <div><a href="mailto:info@svrbc.org">info@svrbc.org</a></div>
</div>
</div>

Drop us a line!

<contact-form
  class="form-container"
  to="info"
  subject="Message from svrbc.org/contact"
  persist-key="/contact" />

<style lang="scss" scoped>
.contact-info {
  display: flex;
  flex-wrap: wrap;
}
//
.contact-info-box {
  width: 15rem;
}
//
.icon {
  float: left;
  width: 1.5rem;
}
//
.contact-info-address .icon {
  height: calc(2 * var(--normal-line-height-abs));
}
</style>
