---
title: Learning opportunities
---

If anything characterizes our church, it is a love for God’s truth.  We believe
that all believers—regardless of age or education—have much to gain from
studying God’s word.  Members of SVRBC live in pursuit of deepening their
knowledge of Christ, and we hope to share that depth with any who visit.

## SVRBC Orientation {#orientation}

## Adult Sunday School {#adult-sunday-school}

## Children’s Sunday School {#childrens-sunday-school}

## Nursery {#nursery}

## Women’s book study {#womens-book-study}

A diverse, vibrant, and inquisitive group of women gather Thursday evenings to
study the bible. Ranging from interns, young moms, retirees, and all in
between, we gather to discuss a book in the Old or New Testament. Join us, as
we seek to gain a better understanding of Jesus Christ as Lord and uncover
God's redemptive plan of salvation from Genesis to Revelation!

We are currently studying through The Promised One by Nancy Guthrie.

All levels of Biblical familiarity welcome!

Contact Sarah Owens (<a href="mailto:sarah@svrbc.org">sarah@svrbc.org</a>) for
more info.

<div v-if="v1">
  <button-area>
    <span class="dark-button" @click="showContact=true">Contact</span>
  </button-area>
  
  <hidden-form-container v-model="showContact">
    <contact-form
      to="daisy@svrbc.org,sarah@svrbc.org"
      subject="Message regarding the women’s book study" />
  </hidden-form-container>
</div>

<script>
import ButtonArea from '~/components/ButtonArea'
import ContactForm from '~/components/ContactForm.vue'
import HiddenFormContainer from '~/components/HiddenFormContainer'
import Debug from '~/mixins/debug'
//
export default {
  mixins: [
    Debug
  ],
  components: {
    ButtonArea,
    ContactForm,
    HiddenFormContainer
  },
  data () {
    return {
      showContact: false
    }
  }
}
</script>
