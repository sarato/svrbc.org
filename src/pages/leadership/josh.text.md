Josh Sheldon was converted to Christ from a Jewish upbringing in 1992.  He
came to the Doctrines of Grace during his seminary studies
([Western Seminary](https://www.westernseminary.edu/), Master of Divinity). He
joined SVRBC (then, Lakewood Village Baptist Church) in the Fall of 1999, and
was called to be the pastor in October of 2004. He and his wife Sue met when
they were high school sophomores, and have been married since July 1976; they
have one child.

In his younger days, Josh was an avid outdoorsman, enjoying rock and mountain
climbing, long distance backpacking, and hunting. These days, he enjoys
studying history, reading classic literature, and road trips with Sue.

Josh is a member of the [Association of Certified Biblical
Counselors](https://biblicalcounseling.com/). He has a passion to see Christ’s
children grow through biblical teaching, preaching, and counseling.
