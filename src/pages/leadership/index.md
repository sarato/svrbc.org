---
title: Meet our leaders
image: ../../banners/green-path.jpg
---

import Leader from '~/components/Leader.vue'
import LeadershipForm from '~/forms/LeadershipForm.vue'

SVRBC is a congregational church, led by elders (also known as pastors) and
deacons.  While these are the only two offices the Bible describes, we
believe that each member of the church bears the responsibility to use their
talents and resources to assist the church, often in leadership capacities.

## Pastors {#pastors}

<div class="leader-container">
  <leader
    name="Brian Garcia"
    :img="$static.brianGarcia.image"
    email="brian@svrbc.org"
    class="content-float-left clear"
  />
  <div v-html="$static.brianGarciaTxt.content"/>
</div>

<div class="leader-container">
  <!-- FUTURE: Add when others also have social links
       facebook="xcco3x"
       instagram="_cco3_"
       linkedin="conley-owens-17908928"
  -->
  <leader
    name="Conley Owens"
    :img="$static.conleyOwens.image"
    email="conley@svrbc.org"
    class="content-float-left clear"
  />
  <div v-html="$static.conleyOwensTxt.content"/>
</div>

<div class="leader-container">
  <leader
    name="Josh Sheldon"
    :img="$static.joshSheldon.image"
    email="josh@svrbc.org"
    class="content-float-left"
  />
  <div v-html="$static.joshSheldonTxt.content"/>
</div>

## Deacon {#deacons}

<div class="leader-container">
  <leader
    name="John Burchett"
    :img="$static.johnBurchett.image"
    email="john@svrbc.org"
    class="content-float-left"
  />
  <div v-html="$static.johnBurchettTxt.content"/>
</div>


## Lay leaders {#laymen}

<div class="lay-leaders wrap-list">
  <leader
    name="John Davis"
    :img="$static.johnDavis.image"
    area="Music Ministry"
  />
  <leader
    name="Dale Hardin"
    :img="$static.daleHardin.image"
    area="Treasury"
  />
  <leader
    name="Ken Tompkins"
    :img="$static.kenTompkins.image"
    area="Children’s Ministry"
  />
  <leader
    name="James Torrefranca"
    :img="$static.jamesTorrefranca.image"
    area="Audio/Visual"
  />
</div>

## Get in touch with one of our leaders! {#contact}

<leadership-form />

<static-query>
query {
  conleyOwens: srcImage(id: "people/conley-owens.jpg") { image }
  conleyOwensTxt: srcText(id: "pages/leadership/conley") { content }
  brianGarcia: srcImage(id: "people/brian-garcia.jpg") { image }
  brianGarciaTxt: srcText(id: "pages/leadership/brian") { content }
  joshSheldon: srcImage(id: "people/josh-sheldon.jpg") { image }
  joshSheldonTxt: srcText(id: "pages/leadership/josh") { content }
  johnBurchett: srcImage(id: "people/john-burchett.jpg") { image }
  johnBurchettTxt: srcText(id: "pages/leadership/john") { content }
  johnDavis: srcImage(id: "people/john-davis.jpg") { image }
  daleHardin: srcImage(id: "people/dale-hardin.jpg") { image }
  daisyGuardado: srcImage(id: "people/daisy-guardado.jpg") { image }
  kenTompkins: srcImage(id: "people/ken-tompkins.jpg") { image }
  jamesTorrefranca: srcImage(id: "people/james-torrefranca.jpg") { image }
}
</static-query>

<style lang="scss" scoped>
.leader-container {
  + .leader-container {
    margin-top: 3rem;
  }
  //
  &::after {
    content: "";
    clear: both;
    display: table;
  }
}
//
.lay-leaders {
  --flex-spacing: var(--double-inline-margin);
  //
  margin-top: 1rem;
  //
  > * {
    flex-grow: 0;
  }
}
</style>
