John Burchett was raised in a Reformed Baptist church in upstate New York by
Christian parents. In God’s kindness he was saved by grace at the age of 18,
shortly before he left home for graduate studies at Duke University.

John is raising 5 lovely daughters with Gretchen, his wife of 11+ years. He
enjoys hiking, college basketball, computers, and fixing things; ideally,
without having broken them first.

John has been a member of SVRBC since 2016, after God relocated his family from
North Carolina a year earlier. He is employed as a Senior Software Engineer at
a local security company.
