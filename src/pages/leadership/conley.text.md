Conley Owens is a software engineer by day, but hopes one day to do the work of
a pastor full-time.  He is the husband of one wife and the father of eight
children.  He enjoys cooperative games, couch surfing, casual skateboarding,
and other peculiar activities.  He has been a member of SVRBC since 2010, and
a pastor since 2019.

Conley was saved at an early age when he learned that blaming others for his
sins didn’t actually fix anything.  Instead, he trusted in the sacrificial
death of Jesus Christ and found true forgiveness.  He discovered Reformed
theology in college after accidentally getting involved with a cult and
realizing he needed a better grasp on what the Bible teaches to keep such
things from happening again.

Conley holds an MDiv from [The Log College & Seminary](https://logcollege.net).
Additionally, he has recently published
[a book on the ethics of ministry fundraising](https://dorean.church).
