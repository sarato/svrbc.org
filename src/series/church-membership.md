---
id: church-membership
title: Church Membership
text: Matthew 16:13–20; 18:15–20; 28:16–20
preacher: Pastor Josh Sheldon
image: ../banners/church-painting.jpg
---
