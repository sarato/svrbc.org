---
id: the-gospel-and-church-discipline
title: The Gospel and Church Discipline
text: Matthew 18; 1 Corinthians 5; Exodus 34
preacher: Pastor Josh Sheldon
image: ../banners/church-door.jpg
---
