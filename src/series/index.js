module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'series/*.md',
    typeName: 'SermonSeries'
  }
}
