import dataformat from '../../lib/dataformat'
import sermons from '../../lib/sermons'

export default {
  type: 'SermonSeries',
  subdir: 'series',
  query: `node {
  bannerstyle
  content
  fileInfo {
    path
  }
  id
  image
  preacher
  title
  text
}`,
  normalizeFilename: sermons.formatSeriesFilename,
  normalizeContent: (node, opts) =>
    sermons.formatSeriesMd(node, opts.data, opts.content),
  nodeChecks: [
    dataformat.checkSpelling('title'),
    dataformat.checkSpelling('text'),
    dataformat.checkSpelling('content', { html: true }),
    dataformat.checkVerses('text')
  ]
}
