---
id: meditations-upon-the-cross
title: Meditations upon the Cross
text: Matthew 26; Luke 22; John 19
preacher: Pastor Josh Sheldon
image: ../banners/wooden-cross.jpg
---
