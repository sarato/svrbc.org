#!/bin/bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
FILE=$DIR/../src/pages/constitution.md

rm -rf /tmp/svrbc-constitution
git clone https://gitlab.com/svrbc/svrbc-constitution.git /tmp/svrbc-constitution

echo '---
title: Constitution of Silicon Valley Reformed Baptist Church
image: ../banners/bricks.jpg
---

Lakewood Village Baptist Church (dba Silicon Valley Reformed Baptist Church)
' > $FILE

tail -n +5 /tmp/svrbc-constitution/constitution.md |
  # Remark can't handle ordered lists that aren't decimal.
  # https://github.com/remarkjs/ideas/issues/12
  sed 's/    a\./    1./' |
  sed 's/    b\./    2./' |
  sed 's/    (\([1-9]\))/    \1. /' |
  sed 's/    (a)/    1. /' |
  sed 's/    (b)/    2. /' |
  sed 's/    (c)/    3. /' |
  sed 's/    (d)/    4. /' >> $FILE

echo '
<style lang="scss">
ol ol {
  list-style-type: lower-alpha;
  //
  ol {
    list-style-type: decimal;
    //
    ol {
      list-style-type: lower-alpha;
    }
  }
}
</style>' >> $FILE

rm -rf /tmp/svrbc-constitution
echo 'File written to src/pages/constitution.md'
