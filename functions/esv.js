import handler from '../lib/lambda'
import esv from '../lib/esv-server'

exports.handler = handler(esv.getEndpoint, esv.query, {
  recaptchaActions: [
    'verses_click',
    'verses_hover'
  ]
})
