import handler from '../lib/lambda'
import youtube from '../lib/youtube-server'

function consume (data) {
  return {
    origin: data.origin,
    code: data.code
  }
}

async function produce (data) {
  const yt = await youtube.getClient({
    code: data.code
  })
  const ok = await yt.verifyUser()
  if (!ok) throw new Error('Authorized user has unexpected channel ID')

  await yt.storeToken()
}

exports.handler = handler(consume, produce, {
  closeOnSuccess: true
})
