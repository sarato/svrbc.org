import handler from '../lib/lambda'

function consume (data) {
  // TODO: Ensure calling user is authenticated.
  console.log('data received')
}

async function produce () {
  // TODO: Actually add the user.
  throw new Error('Cannot add user at this time')
}

exports.handler = handler(consume, produce)
